package it.unibo.segdash.ssdash;

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Created by Ste on 15/07/2016.
 */

import com.google.android.exoplayer.util.Util;

import java.util.Locale;

/**
 * Holds statically defined sample definitions.
 */

/* package */ class Samples {

    public static class Sample {

        public final String name;
        public final String contentId;
        public final String provider;
        public final String uri;
        public final int type;
        public final int algorithm;

        //.replaceAll("\\s", "")  -----> to remove the spaces (ex. Google Glass (Mp4,H264) --> GoogleGlass(Mp4,H264) )
        public Sample(String name, String uri, int type) {
            this(name, name.toLowerCase(Locale.US).replaceAll("\\s", ""), "", uri, type);
        }

        public Sample(String name, String contentId, String provider, String uri, int type) {
            this.name = name;
            this.contentId = contentId;
            this.provider = provider;
            this.uri = uri;
            this.type = type;
            this.algorithm = -3;
        }

    }

    /**
     * definizione degli mpd di ogni singolo video per 1, 2, 4, 6 ecc.. secondi di segmento
     */

    public static final Sample[] BIG_BUCK_BUNNY = new Sample[] {
            new Sample("Big Buck Bunny (1s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/BigBuckBunny/1sec/BigBuckBunny_1s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Big Buck Bunny (2s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/BigBuckBunny/2sec/BigBuckBunny_2s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Big Buck Bunny (4s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/BigBuckBunny/4sec/BigBuckBunny_4s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Big Buck Bunny (6s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/BigBuckBunny/6sec/BigBuckBunny_6s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Big Buck Bunny (10s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/BigBuckBunny/10sec/BigBuckBunny_10s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Big Buck Bunny (15s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/BigBuckBunny/15sec/BigBuckBunny_15s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
    };

    public static final Sample[] ELEPHANTS_DREAM = new Sample[] {
            new Sample("Elephants Dream (1s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/ElephantsDream/1sec/ElephantsDream_1s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Elephants Dream (2s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/ElephantsDream/2sec/ElephantsDream_2s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Elephants Dream (4s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/ElephantsDream/4sec/ElephantsDream_4s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Elephants Dream (6s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/ElephantsDream/6sec/ElephantsDream_6s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Elephants Dream (10s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/ElephantsDream/10sec/ElephantsDream_10s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Elephants Dream (15s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/ElephantsDream/15sec/ElephantsDream_15s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
    };

    public static final Sample[] OF_FOREST_AND_MEN = new Sample[] {
            new Sample("Of Forest and Men (1s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/OfForestAndMen/1sec/OfForestAndMen_1s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Of Forest and Men (2s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/OfForestAndMen/2sec/OfForestAndMen_2s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Of Forest and Men (4s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/OfForestAndMen/4sec/OfForestAndMen_4s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Of Forest and Men (6s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/OfForestAndMen/6sec/OfForestAndMen_6s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Of Forest and Men (10s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/OfForestAndMen/10sec/OfForestAndMen_10s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Of Forest and Men (15s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/OfForestAndMen/15sec/OfForestAndMen_15s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
    };

    public static final Sample[] RED_BULL_PLAY_STREETS = new Sample[] {
            new Sample("Red Bull Play Streets (1s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/RedBullPlayStreets/1sec/RedBull_1_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Red Bull Play Streets (2s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/RedBullPlayStreets/2sec/RedBull_2s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Red Bull Play Streets (4s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/RedBullPlayStreets/4sec/RedBull_4_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Red Bull Play Streets (6s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/RedBullPlayStreets/6sec/RedBull_6_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Red Bull Play Streets (10s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/RedBullPlayStreets/10sec/RedBull_10_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
            new Sample("Red Bull Play Streets (15s)",
                    "http://www-itec.uni-klu.ac.at/ftp/datasets/DASHDataset2014/RedBullPlayStreets/15sec/RedBull_15s_onDemand_2014_05_09.mpd"
                    , Util.TYPE_DASH),
    };

    private Samples() {}

}