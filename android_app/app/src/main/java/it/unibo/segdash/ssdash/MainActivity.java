package it.unibo.segdash.ssdash;

import it.unibo.segdash.ssdash.Samples.Sample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {

    private static final String FIXED_EVALUATOR = "Fixed Evaluator";
    private static final String RANDOM_EVALUATOR = "Random Evaluator";
    private static final String ADAPTIVE_EVALUATOR = "Adaptive Evaluator";
    private static final String WISER_SMOOTHED_EVALUATOR = "Wiser Smoothed Evaluator";
    private static final String FDASH_EVALUATOR = "FDash Evaluator";
    private static final String DYNAMIC_SS_EVALUATOR = "Dynamic Segment Size Evaluator";

    int algorithm = 2; // Adaptive Evaluator

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final List<SampleGroup> sampleGroups = new ArrayList<>(); //questa e' la lista dei filmati nella main activity e fino qua mi sembra simple, so che me ne pentiro'
        SampleGroup group = new SampleGroup("Big Buck Bunny");
        group.addAll(Samples.BIG_BUCK_BUNNY);
        sampleGroups.add(group);
        group = new SampleGroup("Elephants Dream");
        group.addAll(Samples.ELEPHANTS_DREAM);
        sampleGroups.add(group);
        group = new SampleGroup("Of Forest and Men");
        group.addAll(Samples.OF_FOREST_AND_MEN);
        sampleGroups.add(group);
        group = new SampleGroup("Red Bull Play Streets");
        group.addAll(Samples.RED_BULL_PLAY_STREETS);
        sampleGroups.add(group);
        /*final ExpandableListView sampleList = (ExpandableListView) findViewById(R.id.sample_list);
        sampleList.setAdapter(new SampleAdapter(this, sampleGroups));
        sampleList.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view, int groupPosition,
                                        int childPosition, long id) {
                onSampleSelected(sampleGroups.get(groupPosition).samples.get(childPosition));
                return true;
            }
        });*/
        Spinner spinner = (Spinner) findViewById(R.id.algorithmChoice); //menu a tendina per la scelta degli algoritmi
        ArrayList<String> list = new ArrayList<>();
        list.add(FIXED_EVALUATOR);
        list.add(RANDOM_EVALUATOR);
        list.add(ADAPTIVE_EVALUATOR);
        list.add(WISER_SMOOTHED_EVALUATOR);
        list.add(FDASH_EVALUATOR);
        list.add(DYNAMIC_SS_EVALUATOR);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, list);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                algorithm = arg0.getSelectedItemPosition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        ListView listview=(ListView)findViewById(R.id.videoList); //in base al contenuto della lista definita sopra mette tutto in una listview
        final ArrayList<String> videos = new ArrayList<>(sampleGroups.size()-1);
        for (int i=0;i<sampleGroups.size();i++){
            videos.add(sampleGroups.get(i).title);
        }
        LazyAdapter lazyAdapter = new LazyAdapter(this, videos);
        listview.setAdapter(lazyAdapter);

        // Click event for single list row
        /**
         * se non capisco male dovrebbe gestire il click su ogni singolo elemento
         * della list view prendendo l'uri del video e facendo cose.
         */
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SampleGroup selectedVideo = sampleGroups.get(position);
                // we take the first uri of the video group (so the manifest of 1s segment video)
                Sample selectedManifest = selectedVideo.samples.get(0);
                ArrayList<Sample> manifestsList = new ArrayList<>(selectedVideo.samples.size());
                manifestsList.add(selectedManifest);
                for (int i=1; i<selectedVideo.samples.size(); i++){
                    manifestsList.add(selectedVideo.samples.get(i));
                }
                onSampleSelectedNew(manifestsList);
            }
        });
    }

    /**
     * questo metodo prende il manifest list (che devo capire co e') e poi chiama con intent
     * l'avvio del player con il video che si e' selezionato
     * @param samples
     */
    private void onSampleSelectedNew(ArrayList<Sample> samples) {
        Sample toPlay = samples.get(0);
        ArrayList<String> otherUris = new ArrayList<>(samples.size()-1);
        for (int i=1; i<samples.size(); i++){
            otherUris.add(samples.get(i).uri);
        }
        Intent mpdIntent = new Intent(this, PlayerActivity.class)
                .setData(Uri.parse(toPlay.uri))
                .putExtra(PlayerActivity.CONTENT_ID_EXTRA, toPlay.contentId)
                .putExtra(PlayerActivity.CONTENT_TYPE_EXTRA, toPlay.type)
                .putExtra(PlayerActivity.PROVIDER_EXTRA, toPlay.provider)
                .putExtra(PlayerActivity.ALGORITHM_EXTRA, algorithm)
                .putExtra(PlayerActivity.OTHER_URIS_EXTRA, otherUris);
        startActivity(mpdIntent);
    }

    private void onSampleSelected(Sample sample) {
        Intent mpdIntent = new Intent(this, PlayerActivity.class)
                .setData(Uri.parse(sample.uri))
                .putExtra(PlayerActivity.CONTENT_ID_EXTRA, sample.contentId)
                .putExtra(PlayerActivity.CONTENT_TYPE_EXTRA, sample.type)
                .putExtra(PlayerActivity.PROVIDER_EXTRA, sample.provider)
                .putExtra(PlayerActivity.ALGORITHM_EXTRA, algorithm);
        startActivity(mpdIntent);
    }

    private static final class SampleAdapter extends BaseExpandableListAdapter {

        private final Context context;
        private final List<SampleGroup> sampleGroups;

        public SampleAdapter(Context context, List<SampleGroup> sampleGroups) {
            this.context = context;
            this.sampleGroups = sampleGroups;
        }

        @Override
        public Sample getChild(int groupPosition, int childPosition) {
            return getGroup(groupPosition).samples.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                                 View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent,
                        false);
            }
            ((TextView) view).setText(getChild(groupPosition, childPosition).name);
            return view;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return getGroup(groupPosition).samples.size();
        }

        @Override
        public SampleGroup getGroup(int groupPosition) {
            return sampleGroups.get(groupPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                                 ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.sample_chooser_inline_header, parent,
                        false);
            }
            ((TextView) view).setText(getGroup(groupPosition).title);
            return view;
        }

        @Override
        public int getGroupCount() {
            return sampleGroups.size();
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }

    private static final class SampleGroup {

        public final String title;
        public final List<Sample> samples;

        public SampleGroup(String title) {
            this.title = title;
            this.samples = new ArrayList<>();
        }

        public void addAll(Sample[] samples) {
            Collections.addAll(this.samples, samples);
        }

    }
}
