/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unibo.segdash.ssdash;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecTrackRenderer.DecoderInitializationException;
import com.google.android.exoplayer.TimeRange;
import com.google.android.exoplayer.audio.AudioTrack;
import com.google.android.exoplayer.chunk.Format;

import it.unibo.segdash.ssdash.player.DashRendererBuilder;
import it.unibo.segdash.ssdash.player.DemoPlayer;

import com.google.android.exoplayer.util.VerboseLogUtil;
import com.opencsv.CSVWriter;

import android.content.Context;
import android.media.MediaCodec.CryptoException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Logs player events using {@link Log}.
 */
public class EventLogger implements DemoPlayer.Listener, DemoPlayer.InfoListener,
    DemoPlayer.InternalErrorListener {

  private static final String TAG = "EventLogger";
  private static final NumberFormat TIME_FORMAT;

    DataLog dataLog = new DataLog();

  private String videoName;
  private String chunkLength;
  private String timeStamp;
  private Context context;
  private int semaphore;
  private int downloadTime;

  static {
    TIME_FORMAT = NumberFormat.getInstance(Locale.US);
    TIME_FORMAT.setMinimumFractionDigits(2);
    TIME_FORMAT.setMaximumFractionDigits(2);
  }

  private long sessionStartTimeMs;
  private long[] loadStartTimeMs;
  private long[] availableRangeValuesUs;

  public EventLogger(Context context, Uri manifestUri, String timeStamp) {
    loadStartTimeMs = new long[DemoPlayer.RENDERER_COUNT];
    this.context = context;
    this.semaphore = 0;
    String str = manifestUri.getSchemeSpecificPart();
    videoName = str.replaceFirst(".*/(\\w+).*","$1");
    String[] parts = videoName.split("_");
    this.videoName = parts[0]; // 004
    this.chunkLength = parts[1]; // 034556
    this.downloadTime = 0;
    this.timeStamp = timeStamp;
  }

  public void startSession() {
    sessionStartTimeMs = SystemClock.elapsedRealtime();
    Log.d(TAG, "start [0]");
  }

  public void endSession() {
    Log.d(TAG, "end [" + getSessionTimeString() + "]");
  }

  // DemoPlayer.Listener

  @Override
  public void onStateChanged(boolean playWhenReady, int state) {
    Log.d(TAG, "state [" + getSessionTimeString() + ", " + playWhenReady + ", "
            + getStateString(state) + "]");
  }

  @Override
  public void onError(Exception e) {
    Log.e(TAG, "playerFailed [" + getSessionTimeString() + "]", e);
  }

  @Override
  public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees,
      float pixelWidthHeightRatio) {
    Log.d(TAG, "videoSizeChanged [" + width + ", " + height + ", " + unappliedRotationDegrees
        + ", " + pixelWidthHeightRatio + "]");
  }

  // DemoPlayer.InfoListener

  @Override
  public void onBandwidthSample(int elapsedMs, long bytes, long bitrateEstimate) {
    Log.d(TAG, "bandwidth [" + getSessionTimeString() + ", " + bytes + ", "
        + getTimeString(elapsedMs) + ", " + bitrateEstimate + "]");
      this.downloadTime = elapsedMs;
  }

  @Override
  public void onDroppedFrames(int count, long elapsed) {
    Log.d(TAG, "droppedFrames [" + getSessionTimeString() + ", " + count + "]");
  }

    // the chunk is already downloaded. This function is called when it start the loading of chunks
  @Override
  public void onLoadStarted(int sourceId, long length, String chunkLenght, int type, int trigger, Format format,
      long mediaStartTimeMs, long mediaEndTimeMs, long bufferdurationms, long currentPlayback,
                            long estimatedThroughput,long instantThroughput, boolean isInitialization) {
    loadStartTimeMs[sourceId] = SystemClock.elapsedRealtime();
      String connection = "3g";
      if(WifiIsConnected())
          connection = "Wi-Fi";
      bufferdurationms = bufferdurationms / 1000;
      long buffer = bufferdurationms - currentPlayback;
      if (VerboseLogUtil.isTagEnabled(TAG)) {
          Log.v(TAG, "loadStart [" + getSessionTimeString() + ", " + sourceId + ", " + type
                  + ", " + mediaStartTimeMs + ", " + mediaEndTimeMs + ", "+ currentPlayback + ", "
                  + buffer + ", " + format.id + ", " + format.bitrate + ", " + downloadTime + "]");
      }
      Log.d(TAG, "loadStart [" + mediaStartTimeMs + ", " + mediaEndTimeMs + ", "+ currentPlayback + ", "
              + format.id + ", " + format.bitrate + ", " + downloadTime + "]");
      if (!isInitialization) {
          dataLog.connection = connection;
          dataLog.videoName = videoName;
          dataLog.chunkLenght = chunkLenght;
          dataLog.buffer = buffer;
          dataLog.instantThroughput = instantThroughput;
          dataLog.estimatedThroughput = estimatedThroughput;
          semaphore = 1;
      }
  }

    // the chunk is already downloaded. This function is called when it is completed the loading of chunks
  @Override
  public void onLoadCompleted(int sourceId, long bytesLoaded, int type, int trigger, Format format,
       long mediaStartTimeMs, long mediaEndTimeMs, long elapsedRealtimeMs, long loadDurationMs, int segmentId) {
    if (VerboseLogUtil.isTagEnabled(TAG)) {
      long downloadTime = SystemClock.elapsedRealtime() - loadStartTimeMs[sourceId];
      Log.v(TAG, "loadEnd [" + getSessionTimeString() + ", " + segmentId + ", " + downloadTime
          + "]");
    }
      dataLog.segmentId = segmentId;
      dataLog.formatId = format.id;
      dataLog.bitrate = format.bitrate;
      dataLog.bytesLoaded = bytesLoaded;
      dataLog.downloadTime = downloadTime;
    if(semaphore == 1) {
        SaveToFile(dataLog);
        semaphore = 0;
    }
  }

  @Override
  public void onVideoFormatEnabled(Format format, int trigger, long mediaTimeMs) {
    Log.d(TAG, "videoFormat [" + getSessionTimeString() + ", " + format.id + ", "
            + Integer.toString(trigger) + "]");
  }

  @Override
  public void onAudioFormatEnabled(Format format, int trigger, long mediaTimeMs) {
    Log.d(TAG, "audioFormat [" + getSessionTimeString() + ", " + format.id + ", "
            + Integer.toString(trigger) + "]");
  }

  // DemoPlayer.InternalErrorListener

  @Override
  public void onLoadError(int sourceId, IOException e) {
    printInternalError("loadError", e);
  }

  @Override
  public void onRendererInitializationError(Exception e) {
    printInternalError("rendererInitError", e);
  }

  @Override
  public void onDrmSessionManagerError(Exception e) {
    printInternalError("drmSessionManagerError", e);
  }

  @Override
  public void onDecoderInitializationError(DecoderInitializationException e) {
    printInternalError("decoderInitializationError", e);
  }

  @Override
  public void onAudioTrackInitializationError(AudioTrack.InitializationException e) {
    printInternalError("audioTrackInitializationError", e);
  }

  @Override
  public void onAudioTrackWriteError(AudioTrack.WriteException e) {
    printInternalError("audioTrackWriteError", e);
  }

  @Override
  public void onAudioTrackUnderrun(int bufferSize, long bufferSizeMs, long elapsedSinceLastFeedMs) {
    printInternalError("audioTrackUnderrun [" + bufferSize + ", " + bufferSizeMs + ", "
            + elapsedSinceLastFeedMs + "]", null);
  }

  @Override
  public void onCryptoError(CryptoException e) {
    printInternalError("cryptoError", e);
  }

  @Override
  public void onDecoderInitialized(String decoderName, long elapsedRealtimeMs,
      long initializationDurationMs) {
    Log.d(TAG, "decoderInitialized [" + getSessionTimeString() + ", " + decoderName + "]");
  }

  @Override
  public void onAvailableRangeChanged(int sourceId, TimeRange availableRange) {
    availableRangeValuesUs = availableRange.getCurrentBoundsUs(availableRangeValuesUs);
    Log.d(TAG, "availableRange [" + availableRange.isStatic() + ", " + availableRangeValuesUs[0]
            + ", " + availableRangeValuesUs[1] + "]");
  }

  private void printInternalError(String type, Exception e) {
    Log.e(TAG, "internalError [" + getSessionTimeString() + ", " + type + "]", e);
  }

  private String getStateString(int state) {
    switch (state) {
      case ExoPlayer.STATE_BUFFERING:
        return "B";
      case ExoPlayer.STATE_ENDED:
        return "E";
      case ExoPlayer.STATE_IDLE:
        return "I";
      case ExoPlayer.STATE_PREPARING:
        return "P";
      case ExoPlayer.STATE_READY:
        return "R";
      default:
        return "?";
    }
  }

  private String getSessionTimeString() {
    return getTimeString(SystemClock.elapsedRealtime() - sessionStartTimeMs);
  }

  private String getTimeString(long timeMs) {
    return TIME_FORMAT.format((timeMs) / 1000f);
  }

    public void SaveToFile(DataLog dataLog){
        try {
            String filename = this.timeStamp + "_RUN.csv";
            File basePath = Environment.getExternalStorageDirectory();
            String dirPath = basePath.toString() + "/SSDash_Log/";
            // Making the logging directory
            File dir = new File(dirPath);
            dir.mkdirs();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            // Making the .csv file
            File myFile = new File(dir, filename);
            CSVWriter writer;
            if (!myFile.exists()) {
                myFile.createNewFile();
                writer = new CSVWriter(new FileWriter(myFile));
                String[] intestazione = {"Connection", "Video", "Length", "ID", "Quality", "Bitrate",
                        "I. throughput", "E. throughput","Buffer level", "Size (Byte)", "DWNLD time"};
                writer.writeNext(intestazione);
            }
            else{
                // se il file esiste già andiamo in append mode
                writer = new CSVWriter(new FileWriter(myFile , true));
            }
            String[] toSave = new String[]{dataLog.connection, dataLog.videoName, dataLog.chunkLenght,
                    String.valueOf(dataLog.segmentId), dataLog.formatId, String.valueOf(dataLog.bitrate),
                    String.valueOf(dataLog.instantThroughput), String.valueOf(dataLog.estimatedThroughput),
                    String.valueOf(dataLog.buffer), String.valueOf(dataLog.bytesLoaded), String.valueOf(dataLog.downloadTime)};
            writer.writeNext(toSave);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean WifiIsConnected(){
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            return true;
        }
        else return false;
    }

    private class DataLog{

        String connection;
        String videoName;
        String chunkLenght;
        long buffer;
        //contiene, per la fila i, il throughput relativo alla riga i-1 (ovvero il passaggio dei dati del pacchetto i-1)
        //questo significa che il primo non è da considerare (si riferisce al pacchetto di inizializzazione)
        // =====> da shiftare in alto di uno
        long instantThroughput;
        //contiene, per la fila i, il throughput stimato per il pacchetto della stessa riga i
        long estimatedThroughput;
        int segmentId;
        String formatId;
        long bitrate;
        long bytesLoaded;
        //contiene, per la fila i, il download time relativo alla riga i+1
        //questo significa che il download time del primo pacchetto non viene registrato nel log
        // =====> da shiftare in basso di uno
        int downloadTime;

        DataLog(){
        }

    }

}
