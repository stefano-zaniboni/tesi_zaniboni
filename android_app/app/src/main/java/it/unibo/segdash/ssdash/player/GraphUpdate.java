package it.unibo.segdash.ssdash.player;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import it.unibo.segdash.ssdash.R;

/**
 * Created by Ste on 27/07/16.
 */
public class GraphUpdate extends Fragment {
    private final Handler mHandler = new Handler();
    private Runnable mTimer;
    private LineGraphSeries<DataPoint> qualitySeries;
    private LineGraphSeries<DataPoint> videoBitrateSeries;
    private LineGraphSeries<DataPoint> instantThroughputSeries;
    private LineGraphSeries<DataPoint> estimatedThroughputSeries;
    private LineGraphSeries<DataPoint> bufferLevelSeries;
    private double refreshXValue = 5d;
    private int qualityValue;
    private int videoBitrateValue;
    private long instantThroughputValue;
    private long estimatedThroughputValue;
    private long bufferLevelValue;
    private String TAG = "FRAGMENT";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.graph_fragment, container, false);
        Log.d(TAG, "OnCreateView");
        GraphView graph = (GraphView) rootView.findViewById(R.id.graph);
        qualitySeries = new LineGraphSeries<>();
        //videoBitrateSeries = new LineGraphSeries<>();
        //instantThroughputSeries = new LineGraphSeries<>();
        //estimatedThroughputSeries = new LineGraphSeries<>();
        //bufferLevelSeries = new LineGraphSeries<>();
        graph.addSeries(qualitySeries);
        //graph.addSeries(videoBitrateSeries);
        //graph.addSeries(instantThroughputSeries);
        //graph.addSeries(estimatedThroughputSeries);
        //graph.addSeries(bufferLevelSeries);

        //videoBitrateSeries.setColor(Color.GREEN);
        //instantThroughputSeries.setColor(Color.BLUE);
        //estimatedThroughputSeries.setColor(Color.RED);
        //bufferLevelSeries.setColor(Color.BLACK);

        qualitySeries.setTitle("Quality");
        //videoBitrateSeries.setTitle("Video bitrate");
        //instantThroughputSeries.setTitle("Instant throughput");
        //estimatedThroughputSeries.setTitle("Estimated throughput");
        //bufferLevelSeries.setTitle("Buffer level");
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

        graph.getGridLabelRenderer().setHorizontalAxisTitle("Time (s)");
        graph.getGridLabelRenderer().setVerticalAxisTitle("Bitrate (kbps)");

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(40);

        return rootView;
    }

    public void setNewQualityData(int newData)
    {
        qualityValue = newData;
        //update fragment based on 'new Data'
    }

    public void setNewVideoBitrateData(int newData)
    {
        videoBitrateValue = newData;
        //update fragment based on 'new Data'
    }

    public void setNewInstantThroughputData(long newData)
    {
        instantThroughputValue = newData;
        //update fragment based on 'new Data'
    }

    public void setNewEstimatedThroughputData(long newData)
    {
        estimatedThroughputValue = newData;
        //update fragment based on 'new Data'
    }

    public void setNewBufferLevelData(long newData)
    {
        bufferLevelValue = newData;
        //update fragment based on 'new Data'
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        mTimer = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "onResume");
                refreshXValue += 1d;
                qualitySeries.appendData(new DataPoint(refreshXValue, qualityValue), true, 40);
                //videoBitrateSeries.appendData(new DataPoint(refreshXValue, videoBitrateValue), true, 40);
                //instantThroughputSeries.appendData(new DataPoint(refreshXValue, instantThroughputValue), true, 40);
                //estimatedThroughputSeries.appendData(new DataPoint(refreshXValue, estimatedThroughputValue), true, 40);
                //bufferLevelSeries.appendData(new DataPoint(refreshXValue, bufferLevelValue), true, 40);
                mHandler.postDelayed(this, 1000);
            }

        };
        mHandler.postDelayed(mTimer, 300);

    }

    @Override
    public void onPause() {
        mHandler.removeCallbacks(mTimer);
        super.onPause();
    }
}