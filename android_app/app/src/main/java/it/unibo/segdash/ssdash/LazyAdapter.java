package it.unibo.segdash.ssdash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ste on 07/05/2016.
 */

public class LazyAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<String> data;
    private static LayoutInflater inflater=null;

    public LazyAdapter(Activity a, ArrayList<String> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row, null);

        TextView title = (TextView)vi.findViewById(R.id.name); // title
        ImageView thumbnail=(ImageView)vi.findViewById(R.id.thumbnail); // thumb image

        String videoItem = data.get(position);

        // Setting all values in listview
        title.setText(videoItem);
        if (videoItem.equals("Big Buck Bunny")) thumbnail.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.bunny));
        else if (videoItem.equals("Elephants Dream")) thumbnail.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.elephants));
        else if (videoItem.equals("Of Forest and Men")) thumbnail.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.forest));
        else if (videoItem.equals("Red Bull Play Streets")) thumbnail.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.redbull));
        else thumbnail.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.mipmap.bunny));
        return vi;
    }
}
