/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.exoplayer.chunk;

import android.util.Log;

import com.google.android.exoplayer.upstream.BandwidthMeter;

import java.util.List;
import java.util.Random;

/**
 * Selects from a number of available formats during playback.
 */

/** ========= QUESTO E' IL FILE ADAPTIVE DYNAMIC ==========
 * OVVIAMENTE BISOGNA CAMBIARE IL NOME DELLA CLASSE PER FAR FUNZIONARE DI NUOVO TUTTO
 **/

public interface FormatEvaluator_copiaAdaptive {

    /**
     * Enables the evaluator.
     */
    void enable();

    /**
     * Disables the evaluator.
     */
    void disable();

    /**
     * Pass to the Evaluator the formats matrix of chunk lengths.
     */
    void initializeMatrixFormats(Format[][] chunkLengthFormats);

    /**
     * Update the supplied evaluation.
     * <p>
     * When the method is invoked, {@code evaluation} will contain the currently selected
     * format (null for the first evaluation), the most recent trigger (TRIGGER_INITIAL for the
     * first evaluation) and the current queue size. The implementation should update these
     * fields as necessary.
     * <p>
     * The trigger should be considered "sticky" for as long as a given representation is selected,
     * and so should only be changed if the representation is also changed.
     *  @param queue A read only representation of the currently buffered {@link MediaChunk}s.
     * @param playbackPositionUs The current playback position.
     * @param formats The formats from which to select, ordered by decreasing bandwidth.
     * @param evaluation The evaluation.
     * @param out
     */
    // TODO: Pass more useful information into this method, and finalize the interface.
    void evaluate(List<? extends MediaChunk> queue, long playbackPositionUs, Format[] formats,
                  Evaluation evaluation, ChunkOperationHolder out);

    /**
     * A format evaluation.
     */
    public static final class Evaluation {

        /**
         * The desired size of the queue.
         */
        public int queueSize;

        /**
         * The sticky reason for the format selection.
         */
        public int trigger;

        /**
         * The selected format.
         */
        public Format format;

        /**
         * The selected segment length.
         */

        public int lenght;


        public Evaluation() {
            trigger = Chunk.TRIGGER_INITIAL;
        }

    }

    /**
     * Always selects the first format.
     */
    public static final class FixedEvaluator implements FormatEvaluator_copiaAdaptive {

        @Override
        public void enable() {
            // Do nothing.
        }

        @Override
        public void disable() {
            // Do nothing.
        }

        @Override
        public void initializeMatrixFormats(Format[][] chunkLengthFormats) {
            // Do nothing.
        }

        @Override
        public void evaluate(List<? extends MediaChunk> queue, long playbackPositionUs,
                             Format[] formats, Evaluation evaluation, ChunkOperationHolder out) {
            evaluation.format = formats[formats.length - 1]; //worst quality format
            out.instantThroughput = -1;
            out.estimatedThroughput = -1;
            //evaluation.format = formats[0]; //best quality format
            //l'array formats ha al suo interno tutti i formati disponibili per il video riprodotto: da 0 (indice del formato
            // di qualit‡ pi˘ alta) a formats.length - 1 (indice del formato di qualit‡ pi˘ bassa). Questo algoritmo (se utilizzato
            // in DashRenderer.java, fornisce un algoritmo di rate adaptation di qualit‡ fissata, perchË Ë scelto ad ogni segmento
            // lo stesso formato)
        }

    }

    /**
     * Selects randomly between the available formats.
     */
    public static final class RandomEvaluator implements FormatEvaluator_copiaAdaptive {

        private final Random random;

        public RandomEvaluator() {
            this.random = new Random();
        }

        /**
         * @param seed A seed for the underlying random number generator.
         */
        public RandomEvaluator(int seed) {
            this.random = new Random(seed);
        }

        @Override
        public void enable() {
            // Do nothing.
        }

        @Override
        public void disable() {
            // Do nothing.
        }

        @Override
        public void initializeMatrixFormats(Format[][] chunkLengthFormats) {
            // Do nothing.
        }

        @Override
        public void evaluate(List<? extends MediaChunk> queue, long playbackPositionUs,
                             Format[] formats, Evaluation evaluation, ChunkOperationHolder out) {
            Format newFormat = formats[random.nextInt(formats.length)];
            if (evaluation.format != null && !evaluation.format.equals(newFormat)) {
                evaluation.trigger = Chunk.TRIGGER_ADAPTIVE;
            }
            evaluation.format = newFormat;
            out.instantThroughput = -1;
            out.estimatedThroughput = -1;
        }
        // Questo algoritmo (se utilizzato in DashRenderer.java, fornisce un algoritmo di rate adaptation di
        // qualit‡ casuale, perchË ad ogni segmento da scaricare, la scelta della qualit‡ Ë valutata in modo casuale,
        // ovvero selezionando uno dei formati secondo un numero random
    }


    /**
     * An adaptive evaluator for video formats, which attempts to select the best quality possible
     * given the current network conditions and state of the buffer.
     * <p>
     * This implementation should be used for video only, and should not be used for audio. It is a
     * reference implementation only. It is recommended that application developers implement their
     * own adaptive evaluator to more precisely suit their use case.
     */
    public static final class AdaptiveEvaluator implements FormatEvaluator_copiaAdaptive {

        public static final int DEFAULT_MAX_INITIAL_BITRATE = 800000;

        public static final int DEFAULT_MIN_DURATION_FOR_QUALITY_INCREASE_MS = 10000;
        public static final int DEFAULT_MAX_DURATION_FOR_QUALITY_DECREASE_MS = 25000;
        public static final int DEFAULT_MIN_DURATION_TO_RETAIN_AFTER_DISCARD_MS = 25000;
        public static final float DEFAULT_BANDWIDTH_FRACTION = 0.75f;

        private final BandwidthMeter bandwidthMeter;

        private final int maxInitialBitrate;
        private final long minDurationForQualityIncreaseUs;
        private final long maxDurationForQualityDecreaseUs;
        private final long minDurationToRetainAfterDiscardUs;
        private final float bandwidthFraction;

        /**
         * @param bandwidthMeter Provides an estimate of the currently available bandwidth.
         */
        public AdaptiveEvaluator(BandwidthMeter bandwidthMeter) {
            this (bandwidthMeter, DEFAULT_MAX_INITIAL_BITRATE,
                    DEFAULT_MIN_DURATION_FOR_QUALITY_INCREASE_MS,
                    DEFAULT_MAX_DURATION_FOR_QUALITY_DECREASE_MS,
                    DEFAULT_MIN_DURATION_TO_RETAIN_AFTER_DISCARD_MS, DEFAULT_BANDWIDTH_FRACTION);
        }

        /**
         * @param bandwidthMeter Provides an estimate of the currently available bandwidth.
         * @param maxInitialBitrate The maximum bitrate in bits per second that should be assumed
         *     when bandwidthMeter cannot provide an estimate due to playback having only just started.
         * @param minDurationForQualityIncreaseMs The minimum duration of buffered data required for
         *     the evaluator to consider switching to a higher quality format.
         * @param maxDurationForQualityDecreaseMs The maximum duration of buffered data required for
         *     the evaluator to consider switching to a lower quality format.
         * @param minDurationToRetainAfterDiscardMs When switching to a significantly higher quality
         *     format, the evaluator may discard some of the media that it has already buffered at the
         *     lower quality, so as to switch up to the higher quality faster. This is the minimum
         *     duration of media that must be retained at the lower quality.
         * @param bandwidthFraction The fraction of the available bandwidth that the evaluator should
         *     consider available for use. Setting to a value less than 1 is recommended to account
         *     for inaccuracies in the bandwidth estimator.
         */
        public AdaptiveEvaluator(BandwidthMeter bandwidthMeter,
                                 int maxInitialBitrate,
                                 int minDurationForQualityIncreaseMs,
                                 int maxDurationForQualityDecreaseMs,
                                 int minDurationToRetainAfterDiscardMs,
                                 float bandwidthFraction) {
            this.bandwidthMeter = bandwidthMeter;
            this.maxInitialBitrate = maxInitialBitrate;
            this.minDurationForQualityIncreaseUs = minDurationForQualityIncreaseMs * 1000L;
            this.maxDurationForQualityDecreaseUs = maxDurationForQualityDecreaseMs * 1000L;
            this.minDurationToRetainAfterDiscardUs = minDurationToRetainAfterDiscardMs * 1000L;
            this.bandwidthFraction = bandwidthFraction;
        }

        @Override
        public void enable() {
            // Do nothing.
        }

        @Override
        public void disable() {
            // Do nothing.
        }

        @Override
        public void initializeMatrixFormats(Format[][] chunkLengthFormats) {
            // Do nothing.
        }

        // aumenta la qualit‡ progressivamente basandosi solo sul throughput.
        // Se T rimane costante rispetto al bitrate corrente, allora, l'algoritmo ritorna la representation corrente,
        // se il throughput Ë aumentato líalgoritmo ritorna la representation successiva con il bitrate pi˘ alto,
        // o pi˘ bassa in caso contrario.
        @Override
        public void evaluate(List<? extends MediaChunk> queue, long playbackPositionUs,
                             Format[] formats, Evaluation evaluation, ChunkOperationHolder out) {
            long bufferedDurationUs = queue.isEmpty() ? 0
                    : queue.get(queue.size() - 1).endTimeUs - playbackPositionUs;
            Format current = evaluation.format;
            Format ideal = determineIdealFormat(formats, bandwidthMeter.getBitrateEstimate());
            boolean isHigher = ideal != null && current != null && ideal.bitrate > current.bitrate;
            boolean isLower = ideal != null && current != null && ideal.bitrate < current.bitrate;
            if (isHigher) {
                if (bufferedDurationUs < minDurationForQualityIncreaseUs) {
                    // The ideal format is a higher quality, but we have insufficient buffer to
                    // safely switch up. Defer switching up for now.
                    ideal = current;
                } else if (bufferedDurationUs >= minDurationToRetainAfterDiscardUs) {
                    // We're switching from an SD stream to a stream of higher resolution. Consider
                    // discarding already buffered media chunks. Specifically, discard media chunks starting
                    // from the first one that is of lower bandwidth, lower resolution and that is not HD.
                    for (int i = 1; i < queue.size(); i++) {
                        MediaChunk thisChunk = queue.get(i);
                        long durationBeforeThisSegmentUs = thisChunk.startTimeUs - playbackPositionUs;
                        if (durationBeforeThisSegmentUs >= minDurationToRetainAfterDiscardUs
                                && thisChunk.format.bitrate < ideal.bitrate
                                && thisChunk.format.height < ideal.height
                                && thisChunk.format.height < 720
                                && thisChunk.format.width < 1280) {
                            // Discard chunks from this one onwards.
                            evaluation.queueSize = i;
                            break;
                        }
                    }
                }
            } else if (isLower && current != null
                    && bufferedDurationUs >= maxDurationForQualityDecreaseUs) {
                // The ideal format is a lower quality, but we have sufficient buffer to defer switching
                // down for now.
                ideal = current;
            }
            if (current != null && ideal != current) {
                evaluation.trigger = Chunk.TRIGGER_ADAPTIVE;
            }
            evaluation.format = ideal;
            out.estimatedThroughput = bandwidthMeter.getBitrateEstimate() == BandwidthMeter.NO_ESTIMATE?
                    maxInitialBitrate : (long) (bandwidthMeter.getBitrateEstimate() * bandwidthFraction);
            out.instantThroughput = bandwidthMeter.getBitrateEstimate();
        }

        /**
         * Compute the ideal format ignoring buffer health.
         */
        private Format determineIdealFormat(Format[] formats, long bitrateEstimate) {
            // calculating throughput
            long effectiveBitrate = bitrateEstimate == BandwidthMeter.NO_ESTIMATE
                    ? maxInitialBitrate : (long) (bitrateEstimate * bandwidthFraction);
            for (int i = 0; i < formats.length; i++) {
                Format format = formats[i];
                if (format.bitrate <= effectiveBitrate) {
                    return format;
                }
            }
            // We didn't manage to calculate a suitable format. Return the lowest quality format.
            return formats[formats.length - 1];
        }

    }


    public static final class WiserSmoothedEvaluator implements FormatEvaluator_copiaAdaptive {

        public static final int DEFAULT_MAX_INITIAL_BITRATE = 50000;

        String TAG = "WiserEvaluator";

        private final BandwidthMeter bandwidthMeter;

        private long[] lastEstimatedThroughput;
        private long currentEstimatedThroughput;
        double normalizedThroughputDeviation;
        double smoothedParameter = 0.5;

        private int loadedChunks = 0;

        /**
         * @param bandwidthMeter Provides an estimate of the currently available bandwidth.
         */

        public WiserSmoothedEvaluator(BandwidthMeter bandwidthMeter) {
            this.bandwidthMeter = bandwidthMeter;
            lastEstimatedThroughput = new long[2];
            lastEstimatedThroughput[0] = lastEstimatedThroughput[1] = DEFAULT_MAX_INITIAL_BITRATE;
        }

        @Override
        public void enable() {
            // Do nothing.
        }

        @Override
        public void disable() {
            // Do nothing.
        }

        @Override
        public void initializeMatrixFormats(Format[][] chunkLengthFormats) {
            // Do nothing.
        }

        /**
         *  normalizedThroughputDeviation: ("p", see the article - Adaptive streaming of audiovisual content using MPEG DASH)
         *     Provides an estimate of the changes of throughput: high value of this variable means that
         *     the connection throughput is likely to have a significant change. We initialize it to -1, because
         *     for the first two chunks we can't determine her value
         *  smoothedParameter: ("delta", see the article - Adaptive streaming of audiovisual content using MPEG DASH):
         *     the higher his value is, the more the estimated throughput depends on the last segment throughput.
         *     To have a smoothed value of throughput, a small value of delta should be used. We initialize it to 0.5
         *     in order to have an initial impartial estimate
         *  estimatedThroughput: it represents the estimated throughput of the next chunk to be loaded. We make a sort of
         *     estimate according with the average of throughput in the last chunks downloaded
         *  lastSegmentThroughput: it represent the istantaneous throughput, so the throughput that the last chunk downloaded had
         *  lastEstimatedThroughput: an array that contains the estimated throughput of the last two segments
         */

        @Override
        public void evaluate(List<? extends MediaChunk> queue, long playbackPositionUs,
                             Format[] formats, Evaluation evaluation, ChunkOperationHolder out) {
            Format current = evaluation.format;
            Format ideal;
            if(queue.isEmpty()){
                // we return the lowest quality format for the first chunk, in order to have a faster play of stream
                Format lowest = formats[formats.length - 1];
                ideal = lowest;
                lastEstimatedThroughput[0] = lastEstimatedThroughput[1] = lowest.bitrate;
                currentEstimatedThroughput = 0;
            }
            else{
                ideal = determineIdealSmoothedFormat(formats, currentEstimatedThroughput = getEstimatedThroughput(loadedChunks));
            }
            if (current != null && ideal != current) {
                evaluation.trigger = Chunk.TRIGGER_ADAPTIVE;
            }
            evaluation.format = ideal;
            out.instantThroughput = bandwidthMeter.getBitrateEstimate();
            out.estimatedThroughput = currentEstimatedThroughput;
            loadedChunks++;
        }

        /**
         * Compute the estimated throughput .
         */
        private long getEstimatedThroughput(int chunksNumber) {
            long estimatedThroughput;
            long lastSegmentThroughput = bandwidthMeter.getBitrateEstimate();
            // for the second and the third chunks, the estimated throughput is equal to the last segment throughput
            if (chunksNumber == 1 || chunksNumber == 2) {
                estimatedThroughput = lastSegmentThroughput;
            }
            else {
                estimatedThroughput = (long) (((1-smoothedParameter) * lastEstimatedThroughput[0])
                        + (smoothedParameter * lastSegmentThroughput));
                if (chunksNumber > 3){
                    normalizedThroughputDeviation = ( (double)Math.abs(lastSegmentThroughput - lastEstimatedThroughput[1])
                            / (double)lastEstimatedThroughput[1] );
                    smoothedParameter = 1 / ( 1 + Math.exp(-21 * (normalizedThroughputDeviation-0.2)) );
                }
            }
            lastEstimatedThroughput[0] = lastEstimatedThroughput[1];
            lastEstimatedThroughput[1] = estimatedThroughput;
            return estimatedThroughput;
        }

        /**
         * Compute the ideal format .
         */
        private Format determineIdealSmoothedFormat(Format[] formats, long estimatedThroughput) {
            for (int i = 0; i < formats.length; i++) {
                Format format = formats[i];
                if (format.bitrate <= estimatedThroughput) {
                    return format;
                }
            }
            // We didn't manage to calculate a suitable format. Return the lowest quality format.
            return formats[formats.length - 1];
        }

    }

    public static final class FDashEvaluator implements FormatEvaluator_copiaAdaptive {

        public static final int SHORT_BUFFERING_TIME = 10000;
        public static final int CLOSE_BUFFERING_TIME = 40000;
        public static final int LONG_BUFFERING_TIME = 50000;

        private long lastBufferingTime = 0;

        private final BandwidthMeter bandwidthMeter;

        public FDashEvaluator(BandwidthMeter bandwidthMeter) {
            this.bandwidthMeter = bandwidthMeter;
        }

        @Override
        public void enable() {
            // Do nothing.
        }

        @Override
        public void disable() {
            // Do nothing.
        }

        @Override
        public void initializeMatrixFormats(Format[][] chunkLengthFormats) {
            // Do nothing.
        }

        /** see the article "A fuzzy controller for rate adaptation in MPEG-DASH clients"
         *  bufferingTime: denoting the time that the last received segment waits
         *     at the client until it starts playing
         *  lastBufferingTime: the buffering time at the last chunk evaluation by this rate adaptation algorithm
         */

        @Override
        public void evaluate(List<? extends MediaChunk> queue, long playbackPositionUs,
                             Format[] formats, Evaluation evaluation, ChunkOperationHolder out) {
            long bufferingTimeUs = queue.isEmpty() ? 0 : queue.get(queue.size() - 1).endTimeUs - playbackPositionUs;
            long bufferingTimeMs = bufferingTimeUs / 1000;
            Format current = evaluation.format;
            Format ideal = getIdealFormatOnFuzzyLogic(formats, current, bufferingTimeMs);
            if (current != null && ideal != current) {
                evaluation.trigger = Chunk.TRIGGER_ADAPTIVE;
            }
            lastBufferingTime = bufferingTimeMs;
            evaluation.format = ideal;
            out.instantThroughput = bandwidthMeter.getBitrateEstimate();
            out.estimatedThroughput = -1;
        }

        private Format getIdealFormatOnFuzzyLogic(Format[] formats, Format current, long bufferingTime){
            if(current == null) return formats[formats.length -1];
            final long bufferingTimeDeltaS = (bufferingTime - lastBufferingTime) / 1000;
            final boolean falling = bufferingTimeDeltaS < 0;
            final boolean steady = bufferingTimeDeltaS == 0;
            final boolean rising = bufferingTimeDeltaS > 0;
            if (bufferingTime < SHORT_BUFFERING_TIME && falling){return formats[formats.length - 1];}
            if (bufferingTime < CLOSE_BUFFERING_TIME && falling){return SR(formats, current);}
            if (bufferingTime < LONG_BUFFERING_TIME && falling){return current;}
            if (bufferingTime < SHORT_BUFFERING_TIME && steady){return SR(formats, current);}
            if (bufferingTime < CLOSE_BUFFERING_TIME && steady){return current;}
            if (bufferingTime < LONG_BUFFERING_TIME && steady){return SI(formats, current);}
            if (bufferingTime < SHORT_BUFFERING_TIME && rising){return current;}
            if (bufferingTime < CLOSE_BUFFERING_TIME && rising){return SI(formats, current);}
            if (bufferingTime < LONG_BUFFERING_TIME && rising){return formats[0];}
            return current;
        }

        /**
         * Compute a small reduce in current format quality .
         */

        private Format SR(Format[] formats, Format current){
            Format smallReducedFormat;
            int i;
            for (i = 0; i < formats.length; i++) {
                if (formats[i].bitrate == current.bitrate) break;
            }
            if(i < formats.length - 1) {
                smallReducedFormat = formats[i + 1];
                return smallReducedFormat;
            }
            else return current;
        }

        /**
         * Compute a small increase in current format quality .
         */

        private Format SI(Format[] formats, Format current){
            Format smallIncreasedFormat;
            int i;
            for (i = 0; i < formats.length; i++) {
                if (formats[i].bitrate == current.bitrate) break;
            }
            if(i > 0) {
                smallIncreasedFormat = formats[i - 1];
                return smallIncreasedFormat;
            }
            else return current;
        }

    }

    public static final class DynamicSSEvaluator implements FormatEvaluator_copiaAdaptive {

        private static final int[] possibleChunkLengths = {1, 2, 4, 6, 10, 15};
        // vettore fittizio per considerare le distanze tra lunghezze dei segmenti come costanti
        // (ci sarebberose no problemi nel calcolare toIncrease e toDecrease)
        private static final int[] fittizioArray = {1, 2, 3, 4, 5, 6};
        private static Format[][] matrixFormats;

        public static final float CAREFUL_BANDWIDTH_FRACTION = 0.75f;
        public static final int DEFAULT_MAX_INITIAL_BITRATE = 50000;

        public static final int DEFAULT_MIN_DURATION_FOR_QUALITY_INCREASE_MS = 10000;
        public static final int DEFAULT_MAX_DURATION_FOR_QUALITY_DECREASE_MS = 25000;
        public static final int DEFAULT_MIN_DURATION_TO_RETAIN_AFTER_DISCARD_MS = 25000;
        private final long minDurationForQualityIncreaseUs;
        private final long maxDurationForQualityDecreaseUs;
        private final long minDurationToRetainAfterDiscardUs;
        private long[] lastThroughputsExperienced;
        private static long tmpThroughput;
        private static final int M = 10;
        private long throughputsTot;

        String TAG = "DynamicEvaluator";

        private final BandwidthMeter bandwidthMeter;

        public DynamicSSEvaluator(BandwidthMeter bandwidthMeter) {
            lastThroughputsExperienced = new long[M];
            for (int i=0; i<M; i++){
                lastThroughputsExperienced[i] = DEFAULT_MAX_INITIAL_BITRATE;
                throughputsTot += DEFAULT_MAX_INITIAL_BITRATE;
            }
            this.bandwidthMeter = bandwidthMeter;
            this.minDurationForQualityIncreaseUs = DEFAULT_MIN_DURATION_FOR_QUALITY_INCREASE_MS * 1000L;
            this.maxDurationForQualityDecreaseUs = DEFAULT_MAX_DURATION_FOR_QUALITY_DECREASE_MS * 1000L;
            this.minDurationToRetainAfterDiscardUs = DEFAULT_MIN_DURATION_TO_RETAIN_AFTER_DISCARD_MS * 1000L;
        }

        @Override
        public void enable() {
            // Do nothing.
        }

        @Override
        public void disable() {
            // Do nothing.
        }

        @Override
        public void initializeMatrixFormats(Format[][] chunkLengthFormats) {
            matrixFormats = chunkLengthFormats;
        }

        @Override
        public void evaluate(List<? extends MediaChunk> queue, long playbackPositionUs,
                             Format[] formats, Evaluation evaluation, ChunkOperationHolder out) {
            if((playbackPositionUs/1000)<300){
                evaluation.lenght = 0;
                evaluation.format = formats[formats.length-1];
                out.instantThroughput = out.estimatedThroughput = bandwidthMeter.getBitrateEstimate();
            }
            else{
                Log.wtf(TAG, "============EVALUATE===========");
                int actualLength = evaluation.lenght;
                Log.wtf(TAG, "Actual length: "+actualLength);
                long lastThroughput = bandwidthMeter.getBitrateEstimate();
                Log.wtf(TAG, "I. Throughput: "+lastThroughput);
                throughputsTot -= lastThroughputsExperienced[0];
                // bug fix
                tmpThroughput = lastThroughputsExperienced[0];
                // bug fix ^^^
                Log.wtf(TAG, "TOT: "+throughputsTot);
                for (int j=0; j<M-1; j++){
                    lastThroughputsExperienced[j] = lastThroughputsExperienced[j+1];
                }
                lastThroughputsExperienced[M-1] = lastThroughput;
                for (int j=0; j<M; j++){
                    Log.wtf(TAG, "Array throughput "+j+": "+lastThroughputsExperienced[j]);
                }
                throughputsTot += lastThroughput;
                Log.wtf(TAG, "TOT finale: "+throughputsTot);
                double averageThroughput = throughputsTot / M;
                Log.wtf(TAG, "Average throughput: "+averageThroughput);
                // To better cope with the varying environment, and to react faster after sudden throughput
                // variations, we have to prioritize recent throughputs compared to older ones
                long tmp = 0;
                for (int i=0; i<M; i++){
                    long item = lastThroughputsExperienced[i];
                    Log.wtf(TAG, "item"+(i+1)+": "+item);
                    Log.wtf(TAG, "TMP: "+tmp+" i/M "+(double)(i+1)/(double)M+" |item - averageTh|: "+Math.abs(item-averageThroughput));
                    tmp += ( (double)(i+1) / (double)M ) * Math.abs( item - averageThroughput );
                }
                Log.wtf(TAG, "Tmp finale: " +tmp);
                double weightOnRecentThroughputs = tmp;
                Log.wtf(TAG, "peso: "+weightOnRecentThroughputs);
                double changingWillingness = (averageThroughput / (averageThroughput + weightOnRecentThroughputs));
                Log.wtf(TAG, "Changing willingness: "+changingWillingness);
                // For stable connections we will have changingWillingness ~ 1, while an unstable connection
                // will decrease it; we use this parameter to estimate the possibility to increase or decrease SS
                double toDecrease = (1-changingWillingness) *
                        fittizioArray[ Math.max(0, actualLength - 1) ];
                double toIncrease = changingWillingness *
                        fittizioArray[ Math.min(fittizioArray.length-1, actualLength + 1) ];
                Log.wtf(TAG, "Increase :"+toIncrease + " toDecrease: "+toDecrease);
                int newSegmentLength = actualLength;
                double closestGap = 20;  // an initialization value to have a superior limit of the gap
                Log.wtf(TAG,"toDec + toInc: "+(toDecrease+toIncrease));
                for (int k=0; k<fittizioArray.length; k++){
                    Log.wtf(TAG, "Length: "+ possibleChunkLengths[k]+" (Fittizio: "+fittizioArray[k]+")");
                    double proposalGap = Math.abs(fittizioArray[k] - (toDecrease + toIncrease));
                    Log.wtf(TAG, "Proposal Gap: "+proposalGap);
                    if(proposalGap < closestGap) {
                        closestGap = proposalGap;
                        newSegmentLength = k;
                    }
                }
                evaluation.lenght = newSegmentLength;
                Log.wtf(TAG, "New segment length ---> " + newSegmentLength);
                // avoid a little bug
                if(out.queueSize == queue.size() && out.chunk != null
                        && out.chunk.format.equals(evaluation.format)){
                    Log.wtf(TAG, ">>Already have a chunk!!!<<");
                    throughputsTot -= lastThroughputsExperienced[M-1];
                    for (int j=M-1; j>0; j--){
                        lastThroughputsExperienced[j] = lastThroughputsExperienced[j-1];
                    }
                    lastThroughputsExperienced[0] = tmpThroughput;
                    throughputsTot += tmpThroughput;
                }
                else{
                    Log.wtf(TAG, "============END===========");
                    long bufferedDurationUs = queue.isEmpty() ? 0
                            : queue.get(queue.size() - 1).endTimeUs - playbackPositionUs;
                    Format current = evaluation.format;
                    Format ideal = adaptiveFormatEvaluate(lastThroughput, newSegmentLength, formats.length);
                    boolean isHigher = ideal != null && current != null && ideal.bitrate > current.bitrate;
                    boolean isLower = ideal != null && current != null && ideal.bitrate < current.bitrate;
                    if (isHigher) {
                        if (bufferedDurationUs < minDurationForQualityIncreaseUs) {
                            // The ideal format is a higher quality, but we have insufficient buffer to
                            // safely switch up. Defer switching up for now.
                            Log.wtf(TAG, "===========<<<<<<<1============");
                            ideal = matrixFormats[newSegmentLength][currentID(formats,current)];
                        }
                        else if (bufferedDurationUs >= minDurationToRetainAfterDiscardUs) {
                            // We're switching from an SD stream to a stream of higher resolution. Consider
                            // discarding already buffered media chunks. Specifically, discard media chunks starting
                            // from the first one that is of lower bandwidth, lower resolution and that is not HD.
                            Log.wtf(TAG, "============<<<<<<<2===========");
                            for (int i = 1; i < queue.size(); i++) {
                                MediaChunk thisChunk = queue.get(i);
                                long durationBeforeThisSegmentUs = thisChunk.startTimeUs - playbackPositionUs;
                                if (durationBeforeThisSegmentUs >= minDurationToRetainAfterDiscardUs
                                        && thisChunk.format.bitrate < ideal.bitrate
                                        && thisChunk.format.height < ideal.height
                                        && thisChunk.format.height < 720
                                        && thisChunk.format.width < 1280) {
                                    // Discard chunks from this one onwards.
                                    evaluation.queueSize = i;
                                    break;
                                }
                            }
                        }
                    } else if (isLower
                            && bufferedDurationUs >= maxDurationForQualityDecreaseUs) {
                        // The ideal format is a lower quality, but we have sufficient buffer to defer switching
                        // down for now.
                        ideal = matrixFormats[newSegmentLength][currentID(formats,current)];
                        Log.wtf(TAG, "============<<<<<<<3===========");
                    }
                    if (current != null && ideal != current) {
                        evaluation.trigger = Chunk.TRIGGER_ADAPTIVE;
                    }
                    evaluation.format = ideal;
                    out.estimatedThroughput = (long) (bandwidthMeter.getBitrateEstimate() * CAREFUL_BANDWIDTH_FRACTION);
                    out.instantThroughput = bandwidthMeter.getBitrateEstimate();
                }
            }
        }

        private int currentID(Format[] formats,Format current){
            for (int i = 0; i < formats.length; i++) {
                if (formats[i].bitrate == current.bitrate){
                    return i;
                }
            }
            return formats.length-1;
        }

        private Format adaptiveFormatEvaluate(long lastThroughput, int newSegmentLength, int formatsNumber) {
            long carefulBitrate = (long) (lastThroughput * CAREFUL_BANDWIDTH_FRACTION);
            for (int i = 0; i < formatsNumber; i++) {
                Format format = matrixFormats[newSegmentLength][i];
                if (format.bitrate <= carefulBitrate) {
                    return format;
                }
            }
            // We didn't manage to calculate a suitable format. Return the lowest quality format.
            return matrixFormats[newSegmentLength][formatsNumber - 1];
        }

    }

}
