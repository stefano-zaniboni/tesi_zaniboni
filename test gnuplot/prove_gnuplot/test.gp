set terminal postscript color eps
set out 'test.eps'

f(x) = exp(-x**2 / 2)
plot [t=0:4] f(t), t**2 / 16 w l

set out 'tost.eps'

plot [0:4] '2col.csv' using 1:2 with lines
replot
quit