# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal postscript color eps
set output 'fdash_wifi_2722017.eps'

#set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"
plot[-1:120] "20170226154329_RUN.csv" using 6 with lines title 'bitrate dynamic' axes x1y1, "20170226154329_RUN.csv"  using 7 with linespoints title "throughput1" axes x1y2, \
	 "20170226161355_RUN.csv" using 6 with lines title 'bitrate non dynamic' axes x1y1, "20170226161355_RUN.csv"  using 7 with linespoints title "throughput2" axes x1y2, \
	 "20170227172139_RUN.csv" using 6 with lines title 'bitrate non dynamic' axes x1y1, "20170227172139_RUN.csv"  using 7 with linespoints title "throughput3" axes x1y2
replot
quit