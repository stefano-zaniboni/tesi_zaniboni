# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal postscript color eps
set output 'fdash_mobile_2722017.eps'

#set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"
plot[-1:120] "20170227115627_RUN.csv" using 6 with lines title 'bitrate1 dynamic' axes x1y1, "20170227115627_RUN.csv"  using 7 with linespoints title "throughput1 dynamic" axes x1y2, \
	 "20170227163438_RUN.csv" using 6 with lines title 'bitrate2 non dynamic' axes x1y1, "20170227163438_RUN.csv"  using 7 with linespoints title "throughput2 non dynamic" axes x1y2
replot
quit