# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal postscript color eps
set output 'fdash_mobile_2422017.eps'

#set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"
plot[-1:120] "20170224155254_RUN.csv" using 6 with lines title 'bitrate1 dynamic' axes x1y1, "20170224155254_RUN.csv"  using 7 with linespoints title "throughput1 dynamic" axes x1y2, \
	 "20170224162054_RUN.csv" using 6 with lines title 'bitrate2 dynamic' axes x1y1, "20170224162054_RUN.csv"  using 7 with linespoints title "throughput2 dynamic" axes x1y2, \
	 "20170224162756_RUN.csv" using 6 with lines title 'bitrate3 dynamic' axes x1y1, "20170224162756_RUN.csv"  using 7 with linespoints title "throughput3 dynamic" axes x1y2, \
	 "20170224164405_RUN.csv" using 6 with lines title 'bitrate4' axes x1y1, "20170224164405_RUN.csv"  using 7 with linespoints title "throughput4" axes x1y2
replot
quit