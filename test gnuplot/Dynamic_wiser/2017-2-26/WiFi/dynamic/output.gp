# TEST GNUPLOT AUTOMATICO
 
set datafile separator ","
set terminal postscript color eps
 
# Bitrate
set out 'bitrate_20170227174106_RUN.eps'
set xlabel "time"
set label "bitrate"
plot [-1:120] "20170227174106_RUN.csv" using 6 with lines title "bitrate"
 
# Throughput 
set out 'throughput_20170227174106_RUN.eps'
set xlabel "time"
set label "throughput"
plot [-1:120] "20170227174106_RUN.csv" using 7 with lines title "throughput"
 
#bitrate and throughput
set output 'bt+thr.eps'
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
'20170227174106_RUN.csv' using 6 with lines axes x1y1 title 'bitrate2' linecolor 2, '20170227174106_RUN.csv' using 7 with linespoints title 'throughput2' axes x1y2 linecolor 2
replot

#All bitrate
set output 'all_thr.eps'
set xlabel "time"
set ylabel "throughput"
plot[-1:120] \
'20170227174106_RUN.csv' using 7 with lines axes x1y1 title 'throughput2' linecolor 2
replot

#All throughput
set output 'all_bitrate.eps'
set xlabel "time"
set ylabel "bitrate"
plot[-1:120] \
'20170227174106_RUN.csv' using 6 with lines axes x1y1 title 'bitrate2' linecolor 2
replot

quit
