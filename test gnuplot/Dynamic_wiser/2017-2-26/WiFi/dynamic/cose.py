#!/usr/bin/python
import os
import sys
from pprint import pprint

listFile = []
fw = open('output.gp', 'w')
fw.write('# TEST GNUPLOT AUTOMATICO\n')
fw.write(" \n")

for file in os.listdir(os.getcwd()):
	if file.endswith('.csv'):
		listFile.append(file)

pprint(listFile)

purge_ext = []
for item in listFile:
	purge_ext.append(os.path.splitext(item)[0])

#pprint(purge_ext)


fw.write('set datafile separator ","\n')
fw.write('set terminal postscript color eps\n')
fw.write(" \n")

# TODO: create bitrate files
for item in listFile:
	fw.write("# Bitrate\n")
	fw.write("set out 'bitrate_" + str(os.path.splitext(item)[0]) + ".eps'\n")
	fw.write('set xlabel "time"\n')
	fw.write('set label "bitrate"\n')
	fw.write('plot [-1:120] ' + '"' + str(item) + '"' +' using 6 with lines title ' + '"bitrate"\n')
	fw.write(" \n")

# TODO: create throughput files
for item in listFile:
	fw.write("# Throughput \n")
	fw.write("set out 'throughput_" + str(os.path.splitext(item)[0]) + ".eps'\n")
	fw.write('set xlabel "time"\n')
	fw.write('set label "throughput"\n')
	fw.write('plot [-1:120] ' + '"' + str(item) + '"' +' using 7 with lines title ' + '"throughput"\n')
	fw.write(" \n")

# TODO: create a complete scene with bitrate and throughput
fw.write('#bitrate and throughput\n')
fw.write("set output 'bt+thr.eps'\n")
fw.write('set xlabel "time"\n')
fw.write('set ylabel "bitrate and throughput"\n')
fw.write('plot[-1:120] \\\n')

for item in listFile[:-1]:
	fw.write("'" + str(item) + "' using 6 with lines axes x1y1 title 'bitrate" + str(listFile.index(item)+1) + "' linecolor " + str(listFile.index(item)+1) + ", '" + str(item) + "' using 7 with linespoints title 'throughput" + str(listFile.index(item)+1) + "' axes x1y2 linecolor " + str(listFile.index(item)+1) + ", \\" +"\n")
fw.write("'" + str(listFile[-1]) + "' using 6 with lines axes x1y1 title 'bitrate" + str(listFile.index(item)+2) + "' linecolor " + str(listFile.index(item)+2) + ", '" + str(listFile[-1]) + "' using 7 with linespoints title 'throughput" + str(listFile.index(item)+2) + "' axes x1y2 linecolor " + str(listFile.index(item)+2) +'\n')
fw.write('replot\n')
fw.write("\n")

# TODO: create full throughput file
fw.write('#All bitrate\n')
fw.write("set output 'all_thr.eps'\n")
fw.write('set xlabel "time"\n')
fw.write('set ylabel "throughput"\n')
fw.write('plot[-1:120] \\\n')

for item in listFile[:-1]:
	fw.write("'" + str(item) + "' using 7 with lines axes x1y1 title 'throughput" + str(listFile.index(item)+1) + "' linecolor " + str(listFile.index(item)+1) + ", \\" +"\n")
fw.write("'" + str(listFile[-1]) + "' using 7 with lines axes x1y1 title 'throughput" + str(listFile.index(item)+2) + "' linecolor " + str(listFile.index(item)+2) +'\n')
fw.write('replot\n')
fw.write("\n")

# TODO: create fill bitrate file
fw.write('#All throughput\n')
fw.write("set output 'all_bitrate.eps'\n")
fw.write('set xlabel "time"\n')
fw.write('set ylabel "bitrate"\n')
fw.write('plot[-1:120] \\\n')

for item in listFile[:-1]:
	fw.write("'" + str(item) + "' using 6 with lines axes x1y1 title 'bitrate" + str(listFile.index(item)+1) + "' linecolor " + str(listFile.index(item)+1) + ", \\" +"\n")
fw.write("'" + str(listFile[-1]) + "' using 6 with lines axes x1y1 title 'bitrate" + str(listFile.index(item)+2) + "' linecolor " + str(listFile.index(item)+2) +'\n')
fw.write('replot\n')
fw.write("\n")

# TODO: end finally...
fw.write('quit\n')
