# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal postscript color eps
set output 'wiser_wifi_2722017.eps'

#set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"
plot [-1:120] "20170226153506_RUN.csv" using 6 with lines title 'bitrate1' axes x1y1, "20170226153506_RUN.csv"  using 7 with linespoints title "throughput1" axes x1y2, \
	 "20170226160539_RUN.csv" using 6 with lines title 'bitrate2' axes x1y1, "20170226160539_RUN.csv"  using 7 with linespoints title "throughput2" axes x1y2, \
	 "20170227174106_RUN.csv" using 6 with lines title 'bitrate3' axes x1y1, "20170227174106_RUN.csv"  using 7 with linespoints title "throughput3" axes x1y2
replot
quit	