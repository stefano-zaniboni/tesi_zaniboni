# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal postscript color eps
set output 'wiser_mobile_2722017.eps'

#set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"
plot [-1:120] "20170227115015_RUN.csv" using 6 with lines title 'bitrate1' axes x1y1, "20170227115015_RUN.csv"  using 7 with linespoints title "R1" axes x1y2, \
	 "20170227173445_RUN.csv" using 6 with lines title 'bitrate2' axes x1y1, "20170227173445_RUN.csv"  using 7 with linespoints title "R2" axes x1y2
replot
quit