# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal postscript color eps
set output 'wiser_wifi+mobile_2222017.eps'

#set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"
plot "20170222151532_RUN.csv" using 6 with lines title 'bitrate1' axes x1y1, "20170222151532_RUN.csv"  using 7 with linespoints title "throughput1" axes x1y2, \
	 "20170222152611_RUN.csv" using 6 with lines title 'bitrate2' axes x1y1, "20170222152611_RUN.csv"  using 7 with linespoints title "throughput2" axes x1y2, \
	 "20170222154142_RUN.csv" using 6 with lines title 'bitrate3' axes x1y1, "20170222154142_RUN.csv"  using 7 with linespoints title "throughput3" axes x1y2, \
	 "20170222154946_RUN.csv" using 6 with lines title 'bitrate4' axes x1y1, "20170222154946_RUN.csv"  using 7 with linespoints title "throughput4" axes x1y2, \
	 "20170222161626_RUN.csv" using 6 with lines title 'bitrate5' axes x1y1, "20170222161626_RUN.csv"  using 7 with linespoints title "throughput5" axes x1y2, \
	 "20170222162736_RUN.csv" using 6 with lines title 'bitrate6' axes x1y1, "20170222162736_RUN.csv"  using 7 with linespoints title "throughput6" axes x1y2, \
	 "20170222164614_RUN.csv" using 6 with lines title 'bitrate7' axes x1y1, "20170222164614_RUN.csv"  using 7 with linespoints title "throughput7" axes x1y2
replot
quit