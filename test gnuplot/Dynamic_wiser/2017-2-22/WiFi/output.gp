# TEST GNUPLOT AUTOMATICO
 
set datafile separator ","
set terminal postscript color eps
 
# Bitrate
set out 'bitrate_20170222151532_RUN.eps'
set xlabel "time"
set label "bitrate"
plot [-1:120] "20170222151532_RUN.csv" using 6 with lines title "bitrate"
 
# Bitrate
set out 'bitrate_20170222152611_RUN.eps'
set xlabel "time"
set label "bitrate"
plot [-1:120] "20170222152611_RUN.csv" using 6 with lines title "bitrate"
 
# Bitrate
set out 'bitrate_20170222154142_RUN.eps'
set xlabel "time"
set label "bitrate"
plot [-1:120] "20170222154142_RUN.csv" using 6 with lines title "bitrate"
 
# Bitrate
set out 'bitrate_20170222154946_RUN.eps'
set xlabel "time"
set label "bitrate"
plot [-1:120] "20170222154946_RUN.csv" using 6 with lines title "bitrate"
 
# Throughput 
set out 'throughput_20170222151532_RUN.eps'
set xlabel "time"
set label "throughput"
plot [-1:120] "20170222151532_RUN.csv" using 7 with lines title "throughput"
 
# Throughput 
set out 'throughput_20170222152611_RUN.eps'
set xlabel "time"
set label "throughput"
plot [-1:120] "20170222152611_RUN.csv" using 7 with lines title "throughput"
 
# Throughput 
set out 'throughput_20170222154142_RUN.eps'
set xlabel "time"
set label "throughput"
plot [-1:120] "20170222154142_RUN.csv" using 7 with lines title "throughput"
 
# Throughput 
set out 'throughput_20170222154946_RUN.eps'
set xlabel "time"
set label "throughput"
plot [-1:120] "20170222154946_RUN.csv" using 7 with lines title "throughput"
 
#bitrate and throughput
set output 'bt+thr.eps'
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
'20170222151532_RUN.csv' using 6 with lines axes x1y1 title 'bitrate1' linecolor 1, '20170222151532_RUN.csv' using 7 with linespoints title 'throughput1' axes x1y2 linecolor 1, \
'20170222152611_RUN.csv' using 6 with lines axes x1y1 title 'bitrate2' linecolor 2, '20170222152611_RUN.csv' using 7 with linespoints title 'throughput2' axes x1y2 linecolor 2, \
'20170222154142_RUN.csv' using 6 with lines axes x1y1 title 'bitrate3' linecolor 3, '20170222154142_RUN.csv' using 7 with linespoints title 'throughput3' axes x1y2 linecolor 3, \
'20170222154946_RUN.csv' using 6 with lines axes x1y1 title 'bitrate4' linecolor 4, '20170222154946_RUN.csv' using 7 with linespoints title 'throughput4' axes x1y2 linecolor 4
replot

#All bitrate
set output 'all_thr.eps'
set xlabel "time"
set ylabel "throughput"
plot[-1:120] \
'20170222151532_RUN.csv' using 7 with lines axes x1y1 title 'throughput1' linecolor 1, \
'20170222152611_RUN.csv' using 7 with lines axes x1y1 title 'throughput2' linecolor 2, \
'20170222154142_RUN.csv' using 7 with lines axes x1y1 title 'throughput3' linecolor 3, \
'20170222154946_RUN.csv' using 7 with lines axes x1y1 title 'throughput4' linecolor 4
replot

#All throughput
set output 'all_bitrate.eps'
set xlabel "time"
set ylabel "bitrate"
plot[-1:120] \
'20170222151532_RUN.csv' using 6 with lines axes x1y1 title 'bitrate1' linecolor 1, \
'20170222152611_RUN.csv' using 6 with lines axes x1y1 title 'bitrate2' linecolor 2, \
'20170222154142_RUN.csv' using 6 with lines axes x1y1 title 'bitrate3' linecolor 3, \
'20170222154946_RUN.csv' using 6 with lines axes x1y1 title 'bitrate4' linecolor 4
replot

quit
