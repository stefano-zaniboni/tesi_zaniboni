# Script GNUPLOT

#20170226152448_RUN
#20170226155513_RUN

set datafile separator ","
set terminal postscript color eps

#bitrate and throughput
set output 'Adaptive_ReteMobile_NonDynamic_BT_24032017.eps'
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
	"20170226152448_RUN.csv" using 6 with lines axes x1y1 title 'bitrate1' linecolor 1, "20170226152448_RUN.csv"  using 7 with linespoints title "throughput1" axes x1y2 linecolor 1, \
	"20170226155513_RUN.csv" using 6 with lines axes x1y1 title 'bitrate2' linecolor 2, "20170226155513_RUN.csv"  using 7 with linespoints title "throughput2" axes x1y2 linecolor 2
replot

#All throughput
set output 'Adaptive_Wifi_Dynamic_T_24032017.eps'
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
	"20170226152448_RUN.csv"  using 7 with linespoints title "throughput1" axes x1y2 linecolor 1, \
	"20170226155513_RUN.csv"  using 7 with linespoints title "throughput2" axes x1y2 linecolor 2
replot

#All bitrate
set output 'Adaptive_Wifi_Dynamic_B_23022017.eps'
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
	"20170226152448_RUN.csv" using 6 with lines axes x1y1 title 'bitrate1' linecolor 1, \
	"20170226155513_RUN.csv" using 6 with lines axes x1y1 title 'bitrate2' linecolor 2
replot

# Bitrate
set out 'bitrate_20170226152448_RUN.eps'
set xlabel 'time'
set ylabel 'bitrate'
plot [-1:120] "20170226152448_RUN.csv" using 6 with lines title 'bitrate'

set out 'bitrate_20170226155513_RUN.eps'
set xlabel 'time'
set ylabel 'bitrate'
plot [-1:120] "20170226155513_RUN.csv" using 6 with lines title 'bitrate'

# Throughput
set out 'throughput_20170226152448_RUN.eps'
set xlabel 'time'
set ylabel 'throughput'
plot [-1:120] "20170226152448_RUN.csv" using 7 with lines title 'throughput'

set out 'throughput_20170226155513_RUN.eps'
set xlabel 'time'
set ylabel 'throughput'
plot [-1:120] "20170226155513_RUN.csv" using 7 with lines title 'throughput'

quit 