# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal postscript color eps
set output 'Adaptive_Wifi.eps'

#set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"
plot [-1:120] "20170226152448_RUN.csv" using 6 with lines title 'bitrate1 dynamic' axes x1y1, "20170226152448_RUN.csv"  using 7 with linespoints title "throughput1 dynamic" axes x1y2, \
	 "20170226155513_RUN.csv" using 6 with lines title 'bitrate2 dynamic' axes x1y1, "20170226155513_RUN.csv"  using 7 with linespoints title "throughput2 dynamic" axes x1y2, \
	 "20170227112119_RUN.csv" using 6 with lines title 'bitrate3 non dynamic' axes x1y1, "20170227112119_RUN.csv"  using 7 with linespoints title "throughput3 non dynamic" axes x1y2
replot
quit