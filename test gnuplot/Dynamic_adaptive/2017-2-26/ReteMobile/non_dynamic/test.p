# Script GNUPLOT

set datafile separator ","
#set terminal x11
set terminal png
set output 'out.png'

set autoscale
set grid
set xlabel "time"
set ylabel "bitrate"

# Bitrate e throughput
#plot "20170227114501_RUN.csv" using 6 with linespoints, "20170227114501_RUN.csv" using 7 with linespoints

#bitrate con passo dinamico
#plot "20170227114501_RUN.csv" using(x=x+$7):6 with linespoints

#bitrate solo
plot "20170227114501_RUN.csv" using 6 with linespoints

replot
quit