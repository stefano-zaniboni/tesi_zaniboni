# Script GNUPLOT

set datafile separator ","
set terminal postscript color eps

#bitrate and throughput
set output 'Adaptive_ReteMobile_Dynamic_BT_23022017.eps'
set grid
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
	"20170223164231_RUN.csv" using 6 with lines axes x1y1 title 'bitrate1' linecolor 1, "20170223164231_RUN.csv"  using 7 with linespoints title "throughput1" axes x1y2 linecolor 1, \
	"20170223165128_RUN.csv" using 6 with lines axes x1y1 title 'bitrate2' linecolor 2, "20170223165128_RUN.csv"  using 7 with linespoints title "throughput2" axes x1y2 linecolor 2, \
	"20170324154311_RUN_dyn.csv" using 6 with lines axes x1y1 title 'bitrate3' linecolor 3, "20170324154311_RUN_dyn.csv"  using 7 with linespoints title "throughput3" axes x1y2 linecolor 3, \
	"20170324154802_RUN_ndyn.csv" using 6 with lines axes x1y1 title 'bitrate4' linecolor 4, "20170324154802_RUN_ndyn.csv"  using 7 with linespoints title "throughput4" axes x1y2 linecolor 4
replot

#All throughput
set output 'Adaptive_ReteMobile_Dynamic_T_23022017.eps'
set grid
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
	"20170223164231_RUN.csv"  using 7 with linespoints title "throughput1" axes x1y2 linecolor 1, \
	"20170223165128_RUN.csv"  using 7 with linespoints title "throughput2" axes x1y2 linecolor 2, \
	"20170324154311_RUN_dyn.csv"  using 7 with linespoints title "throughput3" axes x1y2 linecolor 3 ,\
	"20170324154802_RUN_ndyn.csv"  using 7 with linespoints title "throughput4" axes x1y2 linecolor 4
replot

#All bitrate
set output 'Adaptive_ReteMobile_Dynamic_B_23022017.eps'
set grid
set xlabel "time"
set ylabel "bitrate and throughput"
plot[-1:120] \
	"20170223164231_RUN.csv" using 6 with lines axes x1y1 title 'bitrate1' linecolor 1, \
	"20170223165128_RUN.csv" using 6 with lines axes x1y1 title 'bitrate2' linecolor 2, \
	"20170324154311_RUN_dyn.csv" using 6 with lines axes x1y1 title 'bitrate3' linecolor 3, \
	"20170324154802_RUN_ndyn.csv" using 6 with lines axes x1y1 title 'bitrate4' linecolor 4
replot

# Bitrate
set out 'bitrate_20170223164231_RUN.eps'
set grid
set xlabel 'time'
set ylabel 'bitrate'
plot [-1:120] "20170223164231_RUN.csv" using 6 with lines title 'bitrate'

set out 'bitrate_20170223165128_RUN.eps'
set grid
set xlabel 'time'
set ylabel 'bitrate'
plot [-1:120] "20170223165128_RUN.csv" using 6 with lines title 'bitrate'


set out 'bitrate_20170324154311_RUN_dyn.eps'
set grid
set xlabel 'time'
set ylabel 'bitrate'
plot [-1:120] "20170324154311_RUN_dyn.csv" using 6 with lines title 'bitrate'

set out 'bitrate_20170324154802_RUN_ndyn.eps'
set grid
set xlabel 'time'
set ylabel 'bitrate'
plot [-1:120] "20170324154802_RUN_ndyn.csv" using 6 with lines title 'bitrate'

# Throughput
set out 'throughput_20170223164231_RUN.eps'
set grid
set xlabel 'time'
set ylabel 'throughput'
plot [-1:120] "20170223164231_RUN.csv" using 7 with lines title 'throughput'


set out 'throughput_20170223165128_RUN.eps'
set grid
set xlabel 'time'
set ylabel 'throughput'
plot [-1:120] "20170223165128_RUN.csv" using 7 with lines title 'throughput'

set out 'throughput_20170324154311_RUN_dyn.eps'
set grid
set xlabel 'time'
set ylabel 'throughput'
plot [-1:120] "20170324154311_RUN_dyn.csv" using 7 with lines title 'throughput'

set out 'throughput_20170324154802_RUN_ndyn.eps'
set grid
set xlabel 'time'
set ylabel 'throughput'
plot [-1:120] "20170324154802_RUN_ndyn.csv" using 7 with lines title 'throughput'

quit 