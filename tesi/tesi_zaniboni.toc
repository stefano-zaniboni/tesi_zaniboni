\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{7}
\contentsline {section}{\numberline {1.1}Presentazione Panorama attuale}{7}
\contentsline {section}{\numberline {1.2}Terminologia}{9}
\contentsline {subsection}{\numberline {1.2.1}Standard e Protocolli di comunicazione}{9}
\contentsline {subsection}{\numberline {1.2.2}Protocolli usati nell'ambito dello Streaming Multimediale}{10}
\contentsline {section}{\numberline {1.3}DASH: Dynamic Adaptive Streaming Over HTTP}{12}
\contentsline {subsection}{\numberline {1.3.1}MPEG-DASH}{13}
\contentsline {subsection}{\numberline {1.3.2}Tecniche e algoritmi di compressione}{13}
\contentsline {section}{\numberline {1.4}Media Presentation File}{15}
\contentsline {section}{\numberline {1.5}Un esempio di funzionamento di DASH}{17}
\contentsline {chapter}{\numberline {2}Stato dell'Arte}{19}
\contentsline {section}{\numberline {2.1}Storia}{19}
\contentsline {section}{\numberline {2.2}Sistemi Proprietari}{20}
\contentsline {section}{\numberline {2.3}Algoritmi di Rate Adaptation}{21}
\contentsline {subsection}{\numberline {2.3.1}Tipologie di algoritmi di Rate Adaptation}{21}
\contentsline {section}{\numberline {2.4}La lunghezza del Segmento}{21}
\contentsline {subsection}{\numberline {2.4.1}Segmento Fissato}{22}
\contentsline {subsection}{\numberline {2.4.2}Segmento Dinamico}{23}
\contentsline {chapter}{\numberline {3}Proposta}{25}
\contentsline {section}{\numberline {3.1}Caratteristiche Tecniche Simulatore}{25}
\contentsline {subsection}{\numberline {3.1.1}Il linuguaggio Python}{25}
\contentsline {subsection}{\numberline {3.1.2}Struttura delle cartelle e organizzazione dei file}{26}
\contentsline {subsection}{\numberline {3.1.3}Throughput Dataset}{26}
\contentsline {section}{\numberline {3.2}Esecuzione del simulatore e file di output}{27}
\contentsline {subsection}{\numberline {3.2.1}Buffertime}{28}
\contentsline {section}{\numberline {3.3}Descrizione degli algoritmi}{30}
\contentsline {subsection}{\numberline {3.3.1}Instantaneous algorithm}{30}
\contentsline {subsection}{\numberline {3.3.2}Oracolo Greedy}{30}
\contentsline {subsection}{\numberline {3.3.3}Oracolo Smart}{31}
\contentsline {subsection}{\numberline {3.3.4}Dynamic SS}{31}
\contentsline {chapter}{\numberline {4}Valutazioni}{33}
\contentsline {section}{\numberline {4.1}Criteri di valutazione}{33}
\contentsline {section}{\numberline {4.2}Analisi dei dati}{34}
\contentsline {subsection}{\numberline {4.2.1}Custom Dataset}{34}
\contentsline {subsection}{\numberline {4.2.2}Test su segmento fissato}{36}
\contentsline {subsection}{\numberline {4.2.3}Test Dynamic SS + Algoritmi di Rate Adaptation}{39}
\contentsline {section}{\numberline {4.3}Dataset Colorado}{49}
\contentsline {subsection}{\numberline {4.3.1}Segmento Fissato}{49}
\contentsline {subsection}{\numberline {4.3.2}Dynamic SS + Algoritmi di Rate Adaptation}{52}
\contentsline {chapter}{Conclusioni}{64}
\contentsline {chapter}{\numberline {A}Instantaneous Algorithm}{65}
\contentsline {chapter}{\numberline {B}Oracolo Greedy}{67}
\contentsline {chapter}{\numberline {C}Oracolo Smart}{69}
\contentsline {chapter}{Bibliografia}{71}
