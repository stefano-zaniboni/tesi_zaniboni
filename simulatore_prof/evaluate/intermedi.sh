#!/bin/bash
#for item in 0 1 2 4 6 10 15
#exit
#./createFakeCsv.sh

# DATASET="japan"
DATASET="datasetFake"

rm ${DATASET}/*.values
rm values*.data
rm values.SS*.datasetFake.A*.data.*

for item in 0
do
	for p in 0 1 2 3
	do
		# for ss in 1 2 4 6 10 15 
		# do
		# 	for file in `ls datasetFake/*.csv` # per prove normali
		# 	do
		# 		echo $ss
		# 	done
		# done
		for ss in 1 2 4 6 10 15
		do
			rm values.SS${ss}.${DATASET}.A${p}.data
			for file in `find ${DATASET}/*.SS${ss}`
			do 
				echo -n "$file ${DATASET} $ss ${p} " >> values.SS${ss}.${DATASET}.A${p}.data
				#cat $file | grep -v " -1 " | awk 'BEGIN{TIME=C=Q=B=THR=0;}{C+=1;B+=$7;Q+=$6;P+=$9;NUMP+=$10;TIME+=$1;THR+=$7;}END{print THR/C " " Q/C " " TIME-595 " " B/C " " NUMP}' >> values.SS${ss}.A${p}.data
				cat $file | grep -v " -1 " | awk 'BEGIN{TIME=C=Q=B=THR=0;}{C+=1;B+=$7;Q+=$6;P+=$9;NUMP=$11;TIME=$1;THR+=$7;}END{print THR/C " " Q/C " " TIME-595 " " B/C " " NUMP}' | sed 's/#//g' >> values.SS${ss}.${DATASET}.A${p}.data
			done
		done
		rm ${DATASET}/*.values
	done
done
# ./startGenGnuplot.sh # per creare i file values.SS.datasetFake.A.data.csv e generare lo script di gnuplot
