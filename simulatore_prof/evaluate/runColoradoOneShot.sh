#for item in 1 2 4 6 10 15
#do
#	for file in `ls colorado/*.parsed.ok`
#	do
#		./buffertime.py ${file} $item 1 False 1 1 0.5	# argv[2] equal to 0 means we dynamically adjust the segment size
#	done
#done
for item in 0
do
	#for p in 2 3 4 7 12
	for p in 13
	#for p in 10 11 12 1 2 3 4 5 6 7 8 9
	do
		for lambda in 5
		do
			for maxO in 0.9
			do
				for ratioP in 0
				do
					for file in `ls colorado/*.parsed.ok`
					do
						./buffertime.py ${file} $item $p False $lambda $maxO $ratioP	# argv[2] equal to 0 means we dynamically adjust the segment size
					done
					./statisticsColorado.sh > statColorado_p_${p}_False_${lambda}_${maxO}_${ratioP}
					for file in `ls colorado/*.parsed.ok`
					do
						./buffertime.py ${file} $item $p True $lambda $maxO	$ratioP    # argv[2] equal to 0 means we dynamically adjust the segment size
					done
					./statisticsColorado.sh > statColorado_p_${p}_True_${lambda}_${maxO}_${ratioP}
				done
			done
		done
	done
done
