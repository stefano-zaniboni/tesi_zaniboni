set terminal postscript enhanced color eps
set o 'thr1.eps'
plot 'values.SS1.A0.data' u 1:4 w p t "A 0",\
'values.SS1.A1.data' u 1:4 w p t "A 1",\
'values.SS1.A2.data' u 1:4 w p t "A 2",\
'values.SS1.A3.data' u 1:4 w p t "A 3"

set o 'thr2.eps'
plot 'values.SS2.A0.data' u 1:4 w p t "A 0",\
'values.SS2.A1.data' u 1:4 w p t "A 1",\
'values.SS2.A2.data' u 1:4 w p t "A 2",\
'values.SS2.A3.data' u 1:4 w p t "A 3"

set o 'thr4.eps'
plot 'values.SS4.A0.data' u 1:4 w p t "A 0",\
'values.SS4.A1.data' u 1:4 w p t "A 1",\
'values.SS4.A2.data' u 1:4 w p t "A 2",\
'values.SS4.A3.data' u 1:4 w p t "A 3"

set o 'thr6.eps'
plot 'values.SS6.A0.data' u 1:4 w p t "A 0",\
'values.SS6.A1.data' u 1:4 w p t "A 1",\
'values.SS6.A2.data' u 1:4 w p t "A 2",\
'values.SS6.A3.data' u 1:4 w p t "A 3"

set o 'thr10.eps'
plot 'values.SS10.A0.data' u 1:4 w p t "A 0",\
'values.SS10.A1.data' u 1:4 w p t "A 1",\
'values.SS10.A2.data' u 1:4 w p t "A 2",\
'values.SS10.A3.data' u 1:4 w p t "A 3"

set o 'thr15.eps'
plot 'values.SS15.A0.data' u 1:4 w p t "A 0",\
'values.SS15.A1.data' u 1:4 w p t "A 1",\
'values.SS15.A2.data' u 1:4 w p t "A 2",\
'values.SS15.A3.data' u 1:4 w p t "A 3"
