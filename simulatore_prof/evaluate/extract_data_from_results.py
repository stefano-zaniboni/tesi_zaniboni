#!/usr/local/bin/python3
import os
from pprint import pprint
import sys
import collections


if sys.argv[2] == "datasetFake" :
	print('datasetFake')
	file_handle = open(sys.argv[1], "r")
	words = {}
	buff = {}
	bitrate = {}
	time = {}
	for line in file_handle:
	    l = line.split(' ')
	    words[int(l[0])] = l[5]
	    buff[int(l[0])] = l[7]
	    bitrate[int(l[0])] = l[9]
	    time[int(l[0])] = l[6]
	file_handle.close()

	ordered = collections.OrderedDict(sorted(words.items())) # Quality Index
	ordered2 = collections.OrderedDict(sorted(buff.items())) # Buffer
	ordered3 = collections.OrderedDict(sorted(bitrate.items())) # bitrate
	ordered4 = collections.OrderedDict(sorted(time.items())) # tempo di pausa
	fede = open(str(sys.argv[1]) + '.dat', 'w')
	for keys in ordered.keys():
		# print(str(keys))
		fede.write(str(keys) + ' ' + str(ordered[keys]) + ' ' + str(ordered2[keys]).strip() + ' ' + str(ordered3[keys]).strip() + ' ' + str(ordered4[keys]).strip() +'\n')

if sys.argv[2] == "colorado" or sys.argv[2] == "japan": 
	file_handle = open(sys.argv[1], "r")
	# print('colorado' + ' ' + sys.argv[1])
	print(sys.argv[1] + ' ' + sys.argv[2])
	words = {}
	pause = {}
	buff = {}
	bitrate = {}
	time = {}
	for line in file_handle:
	    l = line.split(' ')
	    words[float(l[4])] = l[5]
	    pause[float(l[4])] = l[8]
	    buff[float(l[4])] = l[7]
	    bitrate[float(l[4])] = l[9]
	    time[float(l[4])] = l[6]
	file_handle.close()

	ordered = collections.OrderedDict(sorted(words.items())) # quality index 
	ordered2 = collections.OrderedDict(sorted(pause.items())) # num pause 
	ordered3 = collections.OrderedDict(sorted(buff.items())) # buffer 
	ordered4 = collections.OrderedDict(sorted(bitrate.items())) # bitrate
	ordered5 = collections.OrderedDict(sorted(time.items())) # tempo di pausa
	fede = open(str(sys.argv[1]) + '.dat', 'w')
	for keys in ordered.keys():
		# print(str(keys))
		fede.write(str(keys).strip() + ' ' + str(ordered[keys]).strip() + ' ' + str(ordered2[keys]).strip() + ' ' + str(ordered3[keys]).strip() + ' ' + str(ordered4[keys]).strip() + ' ' + str(ordered5[keys]).strip() + '\n')
