set term postscript color eps
set out 'var.eps'

#set xrange [0:900]
#plot 'myvar.csv' u 1:2 t "VAR" w l, '' u 1:3 t "VAR_RED" w l
plot 'myvar.csv' u 1:2 t "VAR" w l, '' u 1:3 t "VAR_RED" w l, '' u 1:4 t "VAR_WEIGHT" w l
plot 'myvar.csv' u 1:5 t "p" w l
plot 'myvar.csv' u 1:6 t "AVG" w l, '' u 1:7 t "AVG_RED" w l, '' u 1:8 t "AVG_WEIGHT" w l
