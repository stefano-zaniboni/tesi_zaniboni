#
# Boxplot demo
#
reset

print "*** Boxplot demo ***"
set terminal postscript enhanced color eps
set grid ytics lc rgb "#bbbbbb" lw 1 lt 0
set grid xtics lc rgb "#bbbbbb" lw 1 lt 0
set style line 1 lt 2 lw 2 pt 3 ps 0.5
set key outside
set style fill solid 0.5 border -1
set style boxplot outliers pointtype 7
set style data boxplot
set boxwidth  0.5
set pointsize 0.5

unset key
set border 2
set xtics ("Instantaneous" 1, "Greedy" 2, "Smart" 3) scale 0.0
set xtics nomirror
set ytics nomirror

set o 'pause-thr0-barre.eps'
set ylabel "Numero Pause"
set xlabel "Algoritmi"
plot 'values.SS0.colorado.A0.data.dat' using (1):3, 'values.SS0.colorado.A1.data.dat' using (2):3, 'values.SS0.colorado.A2.data.dat' using (3):3


set o 'pause-thr1-barre.eps'
set ylabel "Numero Pause"
set xlabel "Algoritmi"
plot 'values.SS1.colorado.A0.data.dat' using (1):3, 'values.SS1.colorado.A1.data.dat' using (2):3, 'values.SS1.colorado.A2.data.dat' using (3):3


set o 'pause-thr2-barre.eps'
set ylabel "Numero Pause"
set xlabel "Algoritmi"
plot 'values.SS2.colorado.A0.data.dat' using (1):3, 'values.SS2.colorado.A1.data.dat' using (2):3, 'values.SS2.colorado.A2.data.dat' using (3):3


set o 'pause-thr4-barre.eps'
set ylabel "Numero Pause"
set xlabel "Algoritmi"
plot 'values.SS4.colorado.A0.data.dat' using (1):3, 'values.SS4.colorado.A1.data.dat' using (2):3, 'values.SS4.colorado.A2.data.dat' using (3):3


set o 'pause-thr6-barre.eps'
set ylabel "Numero Pause"
set xlabel "Algoritmi"
plot 'values.SS6.colorado.A0.data.dat' using (1):3, 'values.SS6.colorado.A1.data.dat' using (2):3, 'values.SS6.colorado.A2.data.dat' using (3):3


set o 'pause-thr10-barre.eps'
set ylabel "Numero Pause"
set xlabel "Algoritmi"
plot 'values.SS10.colorado.A0.data.dat' using (1):3, 'values.SS10.colorado.A1.data.dat' using (2):3, 'values.SS10.colorado.A2.data.dat' using (3):3


set o 'pause-thr15-barre.eps'
set ylabel "Numero Pause"
set xlabel "Algoritmi"
plot 'values.SS15.colorado.A0.data.dat' using (1):3, 'values.SS15.colorado.A1.data.dat' using (2):3, 'values.SS15.colorado.A2.data.dat' using (3):3