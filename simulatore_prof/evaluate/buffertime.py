import sys
import math
import os
import random
import time
from pprint import pprint
import numpy as np
from scipy import stats

import statistics as s

save = 0
# 0: se si vuole l'originale 
# 1: oracolo greedy
# 2: oracolo 'smart'
oracle_selector = int(sys.argv[3])
vettore_pesi_pacchetto = []
num_pkts = 0


def getBuffer():
    count = 0
#     print("---------------------")
    #print(BUFFER)
    for i in range(actualTime, len(BUFFER)):
        if BUFFER[i] >= 0:
            count += 1
        else:
            return count
    return len(BUFFER) - actualTime


def somethingtoplay():
    if BUFFER[actualTime] >= 0:
        return True
    return False


def esplorazione(SS, num_pkts, qualityIndexInPlay, S_to_download):
    print('#######')
    print('Siamo al pacchetto: ' + str(S_to_download))
    print('Guardiamo al futuro')

    listQual = []
    qip = qualityIndexInPlay # mi salvo l'attuale qualityIndexInPlay

    # Ipotizzo di andare avanti di 10 pacchetti rispetto all'ultimo che ho all'interno del mio buffer
    future = S_to_download + 10
    if future > num_pkts: #un controllo per non cercare pacchetti che non esistono
        return False, 0, qip

    print(str(num_pkts) + " buf: " + str(getBuffer()))
    print('Guardo in ' +str(BASEDIR + str(SS) + str('sec')))

    #per comodita' prendo sempre la qualita' piu' bassa
    #TODO Need to get the highest possible quality
    #qip = 19
    fq = BASEDIR + str(SS) + "sec/" + ACRONYM + "_" + str(int(listaQualita[qip])) + "bps" # p.e in 1sec/bunny_270316bps 
    print(str(fq))
        
    fff = open(fq, 'r')
    for line2 in fff.readlines():
        ll2 = line2.split()
        if int(future) == int(ll2[0]):
            bits = int(ll2[1])
    #pktSizeCurrentlyDownloading = bits
    #originalPktSize = float(bits)
    #time_data = (float(bits)/THRFromDataset) + 3 * RTT
    #print("Il futuro pacchetto " + str(future) + " pktSize = " + str(pktSizeCurrentlyDownloading) + ", time = " + str(time_data))
    aux_bytes[future] = (originalPktSize / SS)
    aux_buf[future] = qip
    return True, bits, qip
    


BUF = 0
first = True
counter = 0
totaltime = 0

SS_A = [1,2,4,6,10,15]
LAMBDA = int(sys.argv[5])

maximumOverlap = float(sys.argv[6])

alpha = 2

ratioForP = float(sys.argv[7])

pType = 4
fastBackoff = sys.argv[4]
if fastBackoff == True:
    fastBackoff = True
else:
    fastBackoff = False

#print("fast = " + str(fastBackoff))

SS_FIXED = int(sys.argv[2])  # this is 1 2 4 6 10 15
#if SS_FIXED == 0:
#    open('myvar.csv','w+').close()

ff = open(sys.argv[1],'r')

#RTT = float(sys.argv[8]) # latenza per connessione
RTT = 0
#sigma = float(sys.argv[9]) #ma se buffertime e' chiamato da runFake e gli vengono passati 9 argomenti (0-8) come fa a prendere il 9?
sigma = 0 # a magic number  
#BASEDIR = "../data/"
BASEDIR = sys.argv[8]
ACRONYM = sys.argv[9]
# Let's start with a segment size equal to 1
# TODO Make it parametric
if SS_FIXED > 0:
    SS = SS_FIXED
else:
    SS = 1

#============================= Prendo i pesi dato il SS fissato ====================================
pkts_weight = [] 

path = BASEDIR + '/' + str(SS_FIXED) + 'sec/'
num_files = sum(os.path.isfile(os.path.join(path, f)) for f in os.listdir(path))

listaQualita = []
for file in os.listdir(path):
    thisqual = int(file.split('_')[1].split('bps')[0])
    listaQualita.append(thisqual)
listaQualita = sorted(listaQualita)

c = -1
#for file in os.listdir(path):
for item in listaQualita:
    pkts_weight.append([])
    thisqual = ACRONYM + '_' + str(item) + 'bps'
    fw = open(path + thisqual, 'r')
    c += 1
    num_pkts = 0
    for line in fw:
        num_pkts += 1
        p = line.split()
        pkts_weight[c].append((p[1])) #optional float
#============================= END ==================================================================
aux_buf = []
aux_bytes = []

for timeSinceStart in range(num_pkts):
    aux_bytes.append(-1)
    aux_buf.append(-1)


#print("SIGMA = " + str(sigma))

lastPkts = []
actualTime = 1

# TIME of the video
needToDownload = True
timeToFinish = 0

timeFromDataset = -1
THRFromDataset = -1
actualTime = 1

numPauses = 0
wasPaused = False

open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'w+').close()

wasDownloading = False

currentQual = 0
timeDownloadStarted = 0
pktSizeCurrentlyDownloading = 0
originalPktSize = 0
weHaveToBreak = False
alreadyDownloadedTime = 0

VAR = 0
VAR_RED = 0
VAR_WEIGHT = 0
AVG_WEIGHT = 0
AVG_VAR = 0
AVG = 0

BUFFER = []
BYTES = []
for timeSinceStart in range(6500000):
    BUFFER.append(-1)
    BYTES.append(-1)

firstTime = True ###################### a che cosa serve?

S_to_download = -1

riskOfBufferOutage = False

qualityIndexInPlay = -1
canChange = 0
switchMinimumQuality = False
totalBits = 0
p = 0

expectedOriginalTime = -1

lastTHR = -1

#print(str(SS) + " " + str(sys.argv[1]))

print(sys.argv[1])
bufferFull = False
timeSinceStart = 0

MAXBUFFER = 60 * 1000

for timeSinceStartsss in range(1, 10000000):
    timeSinceStart += 1
#    print(",".join([str(timeSinceStart),str(actualTime),str(getBuffer()),str(SS),str(THRFromDataset),str(qualityIndexInPlay),str(lastTHR),str(sigma),str(wasDownloading),str(timeToFinish)]))
#    print(timeSinceStart)
    if bufferFull:
        bufferFull = False
        deltaTime = getBuffer() - MAXBUFFER
        print("Buffer is full, let's skip of " + str(deltaTime))

        actualTime += deltaTime
        timeSinceStart += deltaTime
        continue
    if timeSinceStart < timeToFinish and (timeSinceStart % 1000 != 0) and wasDownloading:
        if somethingtoplay():
            actualTime += 1
        continue
    #print(THRFromDataset)
    if lastTHR > 0:
        variation = lastTHR - random.gauss(float(lastTHR), float(sigma)) 
        THRFromDataset = min(lastTHR,max(1,lastTHR - variation))
    if weHaveToBreak:
        # Let's empty the buffer
        fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
        cc = timeSinceStart
        while getBuffer() > 0:
            print("Finishing buffer... actualTime = " + str(actualTime))
            fw.write(str(cc) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + "\n")
            actualTime += 1000
            cc += 1000
        fw.close()    
        sys.exit()
    # At each second (or when we have finished downloading a segment)
    # we have to choose the segment size.
    line = ""
    if timeSinceStart > timeFromDataset:
        #print("New time from dataset")
        # Read a new line, which should represent the data rate and the timestamp
        line = ff.readline()
        lastTHR = THRFromDataset
        if line != "":
            timeFromDataset = float(line.split(" ")[0])*1000
            THRFromDataset = max(1,float(line.split(" ")[1]))
            #print("New time: TIME = " + str(timeFromDataset) + ", THR = " + str(THRFromDataset))
        else:
            pass    # We finished values from the dataset
        # TODO Here we have to update the timetofinish according to the new datarate and to the content already downloaded
    if wasDownloading:
        #print("I was downloading")
#        print("** - timetofinish = " + str(timeToFinish))
        timePassed = timeSinceStart - timeDownloadStarted
        
        contentDownloaded = lastTHR * (timePassed/1000.0)
        #print("Original - " + str(originalPktSize) + ", Downloaded - " + str(contentDownloaded))
        pktSizeCurrentlyDownloading = originalPktSize - contentDownloaded
        if pktSizeCurrentlyDownloading > 0:
            #print("Still something to download")
        # Now compute new timetofinish
            time_data = ((pktSizeCurrentlyDownloading/THRFromDataset) + 3 * RTT) * 1000
#             print("pktSize = " + str(pktSizeCurrentlyDownloading) + ", time = " + str(time_data))
            timeToFinish = timeSinceStart + time_data
            time_tmp = ((originalPktSize * 1.0 / THRFromDataset) + 3 * RTT) * 1000
            ratio = expectedOriginalTime / (time_tmp * 1.0)
            if getBuffer() > 0:
                actualTime += 1
            if actualTime % 1000 == 0:
                # Write stats only every second
                fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
                fw.write(str(timeSinceStart) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + " PLAY\n")
                fw.close()
            continue
#        if ratio <= 0.5 and SS_FIXED == 0 and SS > 1 and timeToFinish > i + 1 and getBuffer() < 15:
#        if getBuffer() < timeToFinish:
#            wasDownloading = False
#            SS = 1
#            riskOfBufferOutage = True
#            timeToFinish = i

    needToDownload = True
    while timeSinceStart >= timeToFinish and needToDownload:
        #print("canChange = " + str(canChange) + ", qualityIndexInPlay = " + str(qualityIndexInPlay))
        if THRFromDataset > -1:
            if len(lastPkts) >= LAMBDA:
                lastPkts.remove(lastPkts[0])
            lastPkts.append(THRFromDataset)
        
        # We finished to download the previous packet
        if wasDownloading:
            canChange -= 1
            # Let's fill correctly the buffer
            print("Finished Download segment number " + str(S_to_download) + ", filling the buffer from " + str(((SS*1000) * (S_to_download - 1) + 1)) + " to " + str(((SS*1000) * (S_to_download) + 1)) + ", expected in " + str(expectedOriginalTime))
            for s_down in range(((SS*1000) * (S_to_download - 1) + 1), ((SS*1000) * (S_to_download) + 1)):
                #sys.stdout.write(str(s_down) + " ")
                #BUFFER.insert((SS*S_to_download)+s_down,1)
                BUFFER[s_down] = qualityIndexInPlay
                BYTES[s_down] = (originalPktSize / (SS*1000))
                totalBits += originalPktSize
            #sys.exit()
            #print("Buffer = " + str(getBuffer()))
            fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
            file_media = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]) + '.values', 'a+')
            fw.write(str(timeToFinish) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + "# down packet " + str(S_to_download) + " " + str(numPauses) + "\n")
            file_media.write(str(THRFromDataset) + ' ' + str(BUFFER[actualTime]) + '\n')
            fw.close()

            #if oracle_selector == 3:
                #esplorazione(SS, num_pkts, qualityIndexInPlay, math.floor((actualTime + getBuffer()) / SS))
                       
            # TODO: check that we do not surpass LAMBDA
            # We have to update lastPkts
            #timeItTook = timeToFinish - timeDownloadStarted
            #lastPkts.append(pktSizeCurrentlyDownloading / timeItTook)
        if getBuffer() > MAXBUFFER:
            needToDownload = False
            wasDownloading = False
            timeDownloadStarted = timeSinceStart
            timeToFinish = timeSinceStart
            #print("STOPPING")
            #print(actualTime)
            bufferFull = True
            #actualTime += 1000
            #timeSinceStart += 1000

        if needToDownload:
            # Let's first decide the quality
            listQual = [] 
            for file in os.listdir(BASEDIR + str(SS) + str("sec")):
                thisqual = int(file.split("_")[1].split("bps")[0])
                listQual.append(thisqual)
            listQual = sorted(listQual) ######################################## le ordino p.e sorted([5, 2, 3, 1, 4]) --> [1, 2, 3, 4, 5]
            if SS_FIXED >= 0 and canChange <= 0:
                #print("*****")
                #print("Let's decide whether we have to change the quality")
                #print("We have a THR of " + str(THRFromDataset))
                #print("We are in the #down Packet" + str(S_to_download))
                #print("We try to change quality with a THR of " + str(THRFromDataset))

                #canChange = 3 ############################################# cosa e perche' 3
                expectedTime = 0
                actualQual = listQual[0] #################### la mia qualita' attuale e' la prima alla posizione 0
                for item in listQual: ################### ciclando sulla lista delle qualita' se THR(presumo il throughput dal dataset) e' maggiore dell'elemento nella lista delle qualita' allora 211
                    if THRFromDataset > item: ########## in pratica mi fermo quando supero il throughput disponibile mi sono salvato l'ultimo compatibile
                        actualQual = item
                if qualityIndexInPlay != listQual.index(actualQual):
                    canChange = 1
                qualityIndexInPlay = listQual.index(actualQual)
                    #print("new quality index is " + str(qualityIndexInPlay) + " SS: " + str(SS))

            if SS_FIXED > 0 and firstTime:
                qualityIndexInPlay = 0
                firstTime = False

            ######################## TODO: Decide which segment size we have to choose
            p = 0
            if len(lastPkts) > 1 and SS_FIXED == 0: #################### dal file runFake visto che SS_FIXED prende il secondo argomento $item dal ciclo 1,2,4,6,10,15 come fa ad essere 0?
                AVG = int(sum(lastPkts)/len(lastPkts))
                AVG_RED = 0 ################## che cosa fa?
                count = 1
                total = len(lastPkts) ################### numero totale di elementi che stanno dentro l'array
                summaCount = 0
                for item in lastPkts:
                    AVG_RED += abs(min(0,(listQual[qualityIndexInPlay] / item) - SS))
                    AVG_WEIGHT += (count*1.0) * item ################# cosa fa?
                    count += 1
                    summaCount += count
                AVG_WEIGHT /= summaCount
                AVG_WEIGHT = THRFromDataset
                AVG_RED = AVG_RED / len(lastPkts)

                count = 1
                for j in range(total): 
                    VAR += abs(int(lastPkts[j]) - AVG)
                    VAR_RED += abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED)) ################## cosa fa?
                    VAR_WEIGHT += pow(((count * 1.0)/total)*(abs(int(lastPkts[j]) - AVG_WEIGHT)), alpha) ################## cosa fa?
                    #VAR_WEIGHT += pow(((count * 1.0)/total)*abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED)),5)
                    #VAR_WEIGHT += pow((count * 1.0)/total,2)*abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED))
                    count += 1
                VAR_WEIGHT = math.sqrt(VAR_WEIGHT)
                VAR_RED = math.sqrt(VAR_RED)
                VAR = math.sqrt(VAR)
                isVarOk = False
                if (pType == 1 or pType == 5) and VAR_RED > 0:
                    isVarOk = True
                if (pType == 2 or pType == 3 or pType == 4 or pType == 7 or pType == 12) and VAR_WEIGHT > 0:
                    isVarOk = True
                if (pType == 6) and VAR > 0:
                    isVarOk = True
                if isVarOk > 0:
                    #VAR = math.sqrt(VAR)                         #       BUF      QUAL       SS     QUAL_I  PAUSE
                    beta = 1
                    if pType == 1:
                        p = beta * (AVG/(VAR_RED + AVG))           #SS0  17.9464 2.44187e+06 5.99726 13.3602 73.8056
                    elif pType == 2:
                        p = beta * (AVG/(VAR_WEIGHT + AVG))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 3:
                        p = beta * (AVG_RED/(VAR_WEIGHT + AVG_RED))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 4:
                        p = beta * (AVG_WEIGHT/(VAR_WEIGHT + AVG_WEIGHT))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 5:
                        p = beta * (AVG_RED/(VAR_RED + AVG_RED))   #SS0  20.9993 2.12835e+06 1.78363 11.173 17.1389
                    elif pType == 6:
                        p = beta * (AVG_RED / (VAR + AVG_RED))
                    elif pType == 7:
                        p = beta * (AVG_RED / (VAR_WEIGHT + AVG_RED))
                    elif pType == 12:
                        p = beta * (AVG / (VAR_WEIGHT + AVG))
                        if getBuffer() < 10:
                            p = 0
                            SS = 1
                        elif getBuffer() < 15:
                            SS = 2
                        elif getBuffer() < 20:
                            SS = 4


                else:
                    p = 1
                SSold = SS

                if not firstTime:
                    SS_before = SS_A[max(0,SS_A.index(SS) - 1)]
                    SS_after = SS_A[min(len(SS_A) - 1,SS_A.index(SS) + 1)]
                    SSprime = (1-p) * SS_before + p * SS_after
                    delta = max(SS_A)
                    choosen = -1
                    for item in SS_A:
                        tmp_delta = abs(item - SSprime)
                        if tmp_delta < delta:
                            choosen = item
                            delta = tmp_delta
                        if SS_FIXED == 0:
                            if choosen == -1:
                                SS = 1
                            else:
                                SS = choosen
                else:
                    qualityIndexInPlay = 0
                    if SS_FIXED == 0:
                        SS = 1
                    elif SS_FIXED > 0:
                        SS = SS_FIXED

                    firstTime = False
            else:  ####################### questo e' l'else di if len(lastPkts) > 1 and SS_FIXED == 0:
                if SS_FIXED > 0:
                    SS = SS_FIXED
                else:
                    SS = 1  # We start with a segment size of 1
                    
            # TODO Identify segment to be downloaded
            # TODO, for now, we math.floor(), so we pick an earlier segment and we might download again some data
            if SS_FIXED == 0 and switchMinimumQuality and fastBackoff:
#                 print("SSprime, here")
                SS = 1
            
            if SS_FIXED > 0:
                SS = SS_FIXED
            S_to_download = math.ceil(((actualTime + getBuffer())/1000.0) / SS) ############################## parte intera superiore 
            print("Time passed " + str(timeSinceStart) + ", played time = " + str(actualTime) + ", S_to_download = " + str(S_to_download) + ", Buffer = " + str(getBuffer()))

            # Ok, now we got in SSold the old SS, and in SS the new SS. Let's see how many seconds 
            # already downloaded we have to lose
            amount = 0
            for BB in range(S_to_download * SS * 1000, (S_to_download + 1) * SS * 1000):
                if BUFFER[BB] >= 0:
                    amount += 1
            mmm = (amount * 1.0) / SS ##### questo e' l'overlap non mi interessa (e' una cosa di dynamic dove ho pacchetti di ss=1 e scarico i primi 9 sec. Poi voglio passare ad un ss=10, overlap e' spreco di tempo )
            if mmm >= maximumOverlap and SS_FIXED == 0:
                SS = SSold
                S_to_download = math.floor(((actualTime + getBuffer())/1000.0) / SS)
            
            # TODO Let's choose the quality depending on the buffer and on the
            ## Qua viene preso il file della qualita' della qualita' che ho scelto
            if SS_FIXED == 0 and riskOfBufferOutage:
                SS = 1
                S_to_download = math.floor(((actualTime + getBuffer())/1000.0) / SS)
            listQual = []
            for fileQ in os.listdir(BASEDIR + str(SS) + str("sec")):
                thisqual = int(fileQ.split("_")[1].split("bps")[0])
                listQual.append(thisqual)
            listQual.sort()
            fileQuality = BASEDIR + str(SS) + "sec/" + ACRONYM + "_" + str(int(listQual[qualityIndexInPlay])) + "bps"
           
            # Download pkt QUA!
            #TODO: mi prendo dal vettore 2dimensioni di tutti i pesi di tutte le qualita' i pesi specifici del pacchetto con l'id che sto osservando
            vettore_pesi_pacchetto = []
            for item in range(len(pkts_weight)):
                #print(item)
                for indice_pacchetto in range(len(pkts_weight[item])):
                    #print(pkts_weight[item][peso])
                    if int(S_to_download) == int(indice_pacchetto):
                        really = str((int(pkts_weight[item][indice_pacchetto]) * 8))
                        vettore_pesi_pacchetto.append(float(really))

            ### Esperimento per switch rapido degli algoritmi: 
            ### 0 originale
            ### 1 greedy
            ### 2 smart
            bits = 0
            if oracle_selector == 0:
                # buffertime originale
                ff2 = open(fileQuality, 'r')
                for line2 in ff2.readlines():
                    ll2 = line2.split()
                    if int(S_to_download) == int(ll2[0]):
                        bits = int(ll2[1])
                bits *= 8
            else:
                if oracle_selector == 1:
                    # modifica oracolo greedy
                    if int(S_to_download) < 0 or not vettore_pesi_pacchetto:
                        qualityIndexInPlay = save
                    else:
                        qualityIndexInPlay = vettore_pesi_pacchetto[0]
                        for item in vettore_pesi_pacchetto:
                            time_data = ((float(item) / THRFromDataset) + 3 * RTT) * 1000
                            if int(THRFromDataset) > (float(item) / int(SS)) and time_data <= (int(SS)*1000):
                                bits = float(item)
                        if bits == 0:
                            qualityIndexInPlay = 0
                            bits = vettore_pesi_pacchetto[0]
                        else:
                            qualityIndexInPlay = vettore_pesi_pacchetto.index(bits)
                        save = qualityIndexInPlay
                else:
                    # vecchio oracolo smart sostituito dal quello del prof.
                    # caso oracolo smart
                    # We changed this to 3 just to analyze the 20171004 edit
                    if oracle_selector == 3:
                        #print("AAA")
                        if int(S_to_download) < 0 or not vettore_pesi_pacchetto:
                            qualityIndexInPlay = save
                        else:
                            qualityIndexInPlay = vettore_pesi_pacchetto[0]
                            myTmpTHR = stats.hmean(lastPkts)
                            fwThr = open('thrEval.csv','a')
                            if LAMBDA == 50: #Testing the thr prediction
                                s = [5,10,15,20,25,30,35,40,45,50]
                                for itemSS in s:
                                    subSet = lastPkts
                                    if (len(lastPkts) >= itemSS):
                                        subSet = lastPkts[-itemSS:]
                                    fwThr.write(str(sys.argv[1]) + ',' + str(SS_FIXED) + ',' + str(itemSS) + ',' + str(stats.hmean(subSet)) + ',' + str(np.mean(subSet)) + ',' + str(stats.gmean(subSet)) + ',' + str(np.average(subSet,weights=list(range(min(LAMBDA,len(subSet)),0,-1)))) + ',' + str(np.median(subSet)) + ',' + str(THRFromDataset) + '\n')

                            fwThr.close()
                            bits = 0
                            for item in vettore_pesi_pacchetto:
                                time_data = ((float(item) / THRFromDataset) + 3 * RTT) * 1000
                                print("*************** THR = " + str(myTmpTHR) + "***************")
                                #print(str(time_data) + " - " + str(item) + " - " + str(getBuffer()))
                                newTimeData = ((float(item) / myTmpTHR) + 3 * RTT) * 1000
                                # Edit 20171004
                                currentBuffer = max(getBuffer(),1)
                                if (newTimeData > currentBuffer*0.5):
                                    continue # If the time needed to download the packet is greater than the buffer, skip this quality
                                myRatio = newTimeData / currentBuffer
                                #newLambda = (newTimeData / max(1,getBuffer()))
                                #newLambda = min(newLambda,1)
                                standardDeviation = np.std(lastPkts)
                                newLambda = myTmpTHR/(standardDeviation + myTmpTHR)
#                                newLambda = THRFromDataset/(np.std(lastPkts) + THRFromDataset)
                                #newBuffer = (getBuffer() - newTimeData) - newTimeData**(1-newLambda)
                                myLikelihoodTmp = newLambda - (myRatio * 0.2) # Ugly 0.8
                                fwTmp = open('functionsCsv.csv','a')
                                # TODO Here the problem might be that in case we have full buffer, the second term goes to zero, and you always download the highest possible quality since new
                                # newLambda is always positive. This boosts the quality, but we have to find a way to limit this behavior in case of extremely unstable connectionsm and/or
                                # when myRatio is greater than 1, which means we have a time of download higher than the buffer.
                                # TODO This kinda works 
                                myLikelihood = newLambda - (1-(currentBuffer/MAXBUFFER))*(myRatio * (standardDeviation/(standardDeviation + myTmpTHR))) # Ugly 0.8
                                #myLikelihood = newLambda - (1-(currentBuffer/MAXBUFFER))*(myRatio)
                                #myLikelihood = -1
                                #if (currentBuffer >= MAXBUFFER):
                                    # Buffer full, rely on time and ratio
                                #    myLikelihood = newLambda - (myRatio * (standardDeviation/(standardDeviation + myTmpTHR))) # Ugly 0.8
                                #else:
                                #    if standardDeviation != 0:
                                #    myLikelihood = newLambda - (1-(currentBuffer/MAXBUFFER))*(myRatio * (standardDeviation/(standardDeviation + myTmpTHR))) # Ugly 0.8
                                #    else:
                                #        myLikelihood = newLambda - myRatio
                                fwTmp.write(str(myTmpTHR) + ',' + str(standardDeviation) + ',' + str(newLambda) + ',' + str(myRatio) + ',' + str(myLikelihoodTmp) + ',' + str(myLikelihood) + ',' + str(currentBuffer) + '\n')
                                fwTmp.close()
                                print(str(newTimeData) + " - " + str(newLambda) + " - " + str(myRatio) + " - " + str(myLikelihood) + " - " + str(currentBuffer) + " - " + str(myTmpTHR) + " - " + str(standardDeviation))
                                if myLikelihood > 0:
                                    bits = float(item)
#                                if currentBuffer <= SS_FIXED * 1000:
                                    # This means we are probably experiencing pauses
 #                                   bits = 0
                                if False: # Old version
                                    if getBuffer() <= 5000:
                                        print("Low buffer, switching to quality 0")
                                        qualityIndexInPlay = 0
                                        bits = vettore_pesi_pacchetto[0]
                                    else:
                                    #myVar = getVariance(lastPkts,myTmpTHR)
                                    #indexOfDispersion = (myVar)/myTmpTHR
                                    #indexOfDispersion = (myVar)/(myTmpTHR+myVar)
                                    #grad = np.mean(np.gradient(lastPkts))*-1
#                                    time_data = (float(item) / myTmpTHR) + 3 * RTT
#                                    time_data += (time_data*grad)
                                        time_data = ((float(item) / myTmpTHR) + 3 * RTT) * 1000
                                    #time_data = (float(item) / THRFromDataset) + 3 * RTT
#                                    if (indexOfDispersion > 1):
#                                        print(str(time_data) + " " + str(indexOfDispersion) + " " + str(time_data**(1+indexOfDispersion)))
#                                    if (time_data**(1+indexOfDispersion) ) < getBuffer():
                                        if time_data < getBuffer():
                                      # print(str(vettore_pesi_pacchetto.index(item)) + ": " + str(time_data) + " " + str(getBuffer()) + " " + str(bits))
                                            bits = float(item)
                          # print(vettore_pesi_pacchetto)
                            total = vettore_pesi_pacchetto.count(bits)
                            if bits == 0:
                                qualityIndexInPlay = 0
                                bits = vettore_pesi_pacchetto[0]
                            else:
                                qualityIndexInPlay = vettore_pesi_pacchetto.index(bits) + total - 1
                            save = qualityIndexInPlay
                            #print("Selected " + str(bits))
                        '''qq = -1
                        if int(S_to_download) < 0 or not vettore_pesi_pacchetto:
                            qq = save
                        else:
                            qq = vettore_pesi_pacchetto[0]
#                            uncertainty = numpy.var(lastPkts)
                            
                            avgTHR = sum(lastPkts)/(len(lastPkts))
#                            if len(lastPkts) > 2:
#                                avgTHR = sum(lastPkts)/(len(lastPkts)-2)
                            varTHR = 0
                            for i in range(len(lastPkts)):
                                varTHR += ((lastPkts[i] - avgTHR)**2)
                            varTHR = math.sqrt(varTHR)
#                            if varTHR != 0:
#                                print('***')
#                                print(varTHR)
#                                print(avgTHR)
#                                avgTHR = max(avgTHR - avgTHR**varTHR,0)
                            #avgTHR = min(lastPkts)
                            #avgTHR = 1
                            for item in vettore_pesi_pacchetto:
                                time_data = (float(item) / avgTHR) + 3 * RTT
                                if getBuffer() == 0:
                                    qq = 0
                                    bits = vettore_pesi_pacchetto[0]
                                else:
                                    # Here we compute how much we trust the throughput
                                    # High variance, low trust, and the inverse
                                    #trust = avgTHR / (avgTHR + varTHR)
                                    #ratio = (time_data + time_data * (1-trust))/getBuffer()
                                    if time_data < getBuffer():
                                    #if ratio <= 0.3:
#                                        print(str(vettore_pesi_pacchetto.index(item)) + ": " + str(time_data) + " " + str(getBuffer()) + " " + str(bits))
                                        bits = float(item)
#                            print(vettore_pesi_pacchetto)
                            total = vettore_pesi_pacchetto.count(bits)
                            if bits == 0:
                                qq = 0
                                bits = vettore_pesi_pacchetto[0]
                            else:
                                qq = vettore_pesi_pacchetto.index(bits) + total - 1
                            #save = qq'''
                    else:
                        #caso esplorazione
                        # mi chiedo se devo fare esplorazione:
                        # se si scarico il pacchetto in avanti e me lo faccio ritornare come nuovo bits con il suo qualityIndexInPlay
                        # e vado a metterlo in BYTES e BUFFER
                        ff2 = open(fileQuality, 'r')
                        #print(str(fileQuality) + " " + str(S_to_download))
                        for line2 in ff2.readlines():
                            ll2 = line2.split()
                            if int(S_to_download) == int(ll2[0]):
                                item = int(ll2[1])
                        bits = item
                        if S_to_download == 41:
                            bits = 0                        
                        #print(str(bits) + " " + str(item))
                        # in questo if assumo di fare esplorazione solo a determinati valori per provare 20, 31, 47 ecc..
                        #if getBuffer() > 28 and (S_to_download == 15 or S_to_download == 29 or S_to_download == 47 or S_to_download == 60 or S_to_download == 83 or S_to_download == 160 or S_to_download == 247 or S_to_download == 333 or S_to_download == 418 or S_to_download == 551):
                        if getBuffer() > 28:
                            [exploring, bits_f, quality_f] = esplorazione(SS, num_pkts, qualityIndexInPlay, S_to_download)
                            if exploring == False: #significa che l'esplorazione non e' andata a buon fine
                                #print('No esplorazione')
                                bits = item
                                #print(str(bits) + " " + str(item))
                            else:
                                #print('Si esplorato')
                                exploring == False
                                bits = bits_f
                                qualityIndexInPlay = quality_f
                                #print(str(bits) + " " + str(item))
                        else:
                            #print("No condizioni")
                            bits = item
                            # print(str(bits) + " " + str(item))
                            # time.sleep(1)
                            # se non faccio esplorazione, guardo se nel mio buffer ho informazioni future
                            # se si cerco di capire come posso/se devo cambiare la qualita'
                            # altrimenti procedo normalmente
                            # print(str(S_to_download))
                            # if S_to_download == 41:
                            #     bits = 0
                            # else:
                            if aux_buf[S_to_download] > -1: 
                                print("SI")
                                # if aux_bytes[S_to_download] < item:
                                # time_data = (float(item) / THRFromDataset) + 3 * RTT
                                # if int(THRFromDataset) > (float(item) / int(SS)) and time_data <= int(SS):
                                time_data = ((float(aux_bytes[S_to_download]) / THRFromDataset) + 3 * RTT) * 1000
                                # if aux_bytes[S_to_download] < THRFromDataset:
                                if int(THRFromDataset) > (float(aux_bytes[S_to_download]) / int(SS)) and time_data <= (int(SS) * 1000):
                                    bits = aux_bytes[S_to_download]
                                    qualityIndexInPlay = aux_buf[S_to_download]
                                else:
                                    #print("NO")
                                    bits = item
            
            if bits == 0:
                # We do not have further segments to download
                weHaveToBreak = True ############################### siamo alla fine del video quindi non devo scaricare altro
                break
            else:
                switchMinimumQuality = False
                weHaveToBreak = False

            pktSizeCurrentlyDownloading = bits
            originalPktSize = float(bits)
            time_data = ((float(originalPktSize)/THRFromDataset) + 3 * RTT) * 1000
            #print("bits = " + str(bits) + ", pktSize = " + str(pktSizeCurrentlyDownloading) + ", time = " + str(time_data))
            timeDownloadStarted = timeToFinish
            timeToFinish = timeSinceStart + time_data
            print("** DOWNLOAD ** bits=" + str(originalPktSize) + ", thr=" + str(THRFromDataset) + ", time=" + str(time_data) + ", qi=" + str(qualityIndexInPlay))
            #print("timeToFinish = " + str(timeToFinish))
            expectedOriginalTime = time_data
            wasDownloading = True
    
    fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
    if getBuffer() > 0:
        wasPaused = False
        BUF = max(0,BUF - 1)
        actualTime += 1
        fw.write(str(timeSinceStart) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + " PLAY\n")
        
    else:
        # We have a pause. Let's switch back to the lowest possible quality
        if not wasPaused:
            numPauses += 1
        wasPaused = True
        fw.write(str(timeSinceStart) + " " + str(actualTime) + " " + str(getBuffer()) + " 0 " + str(SS) + " 0 " + str(0) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + " PAUSE\n")
    if getBuffer() == 0:
        switchMinimumQuality = True

    fw.close()
