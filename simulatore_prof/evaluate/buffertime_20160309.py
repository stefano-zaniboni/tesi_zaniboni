#!/usr/local/bin/python3
import sys
import math

BUF = 0
first = True
counter = 0
totaltime = 0

SS_A = [1,2,4,6,10,15]
LAMBDA = int(sys.argv[5])

maximumOverlap = float(sys.argv[6])

alpha = 2

ratioForP = float(sys.argv[7])

pType = int(sys.argv[3])
fastBackoff = bool(sys.argv[4])

SS_FIXED = int(sys.argv[2])
#if SS_FIXED == 0:
#    open('myvar.csv','w+').close()

print (sys.argv[1])

ff = open(sys.argv[1],'r')

# Let's start with a segment size equal to 1
# TODO Make it parametric
if SS_FIXED > 0:
    SS = SS_FIXED
else:
    SS = 1

lastPkts = []
actualTime = 1
BASEDIR = "../data/"

# TIME of the video
needToDownload = True
timeToFinish = 0

timeFromDataset = -1
THRFromDataset = -1
actualTime = 1

open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'w+').close()

wasDownloading = False

currentQual = 0
timeDownloadStarted = 0
pktSizeCurrentlyDownloading = 0
originalPktSize = 0
weHaveToBreak = False
alreadyDownloadedTime = 0

VAR = 0
VAR_RED = 0
VAR_WEIGHT = 0
AVG_WEIGHT = 0
AVG_VAR = 0
AVG = 0

BUFFER = []
BYTES = []
for i in range(650):
    BUFFER.append(-1)
    BYTES.append(-1)

firstTime = True

S_to_download = -1

def getBuffer():
    count = 0
#     print("---------------------")
    #print(BUFFER)
    for i in range(actualTime, len(BUFFER)):
        if BUFFER[i] >= 0:
            count += 1
        else:
            return count
    return len(BUFFER) - actualTime

riskOfBufferOutage = False

qualityIndexInPlay = -1
canChange = 0
switchMinimumQuality = False
totalBits = 0
p = 0

expectedOriginalTime = -1

for i in range(1,10000):
    print("**********************************************************************************")
    #print(BUFFER)
    if weHaveToBreak:
        # Let's empty the buffer
        fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
        cc = i
        while getBuffer() > 0:
            fw.write(str(cc) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + "\n")
            actualTime += 1
            cc += 1
        fw.close()    
        print("BROKE")
        sys.exit()
    # At each second (or when we have finished downloading a segment)
    # we have to choose the segment size.
    print("THR = " + str(THRFromDataset))
    print("Started = " + str(timeDownloadStarted))
    line = ""
    if i > timeFromDataset:
        # Read a new line, which should represent the data rate and the timestamp
        line = ff.readline()
        lastTHR = THRFromDataset
        if line != "":
            timeFromDataset = float(line.split(" ")[0])
            THRFromDataset = max(1,float(line.split(" ")[1]))
        else:
            pass    # We finished values from the dataset
        # TODO Here we have to update the timetofinish according to the new datarate and to the content already downloaded
        if wasDownloading:
            timePassed = i - timeDownloadStarted
        
            contentDownloaded = lastTHR * timePassed
            pktSizeCurrentlyDownloading = originalPktSize - contentDownloaded
            print("Still have to download: " + str(pktSizeCurrentlyDownloading))
            # Now compute new timetofinish
            time_data = pktSizeCurrentlyDownloading/THRFromDataset
            print("Time data = " + str(time_data))
            timeToFinish = i + time_data
            time_tmp = originalPktSize * 1.0 / THRFromDataset
            ratio = expectedOriginalTime / (time_tmp * 1.0)
            print("JJJ - ratio is " + str(ratio))

            if ratio <= 0.5 and SS_FIXED == 0 and SS > 1 and timeToFinish > i + 1 and getBuffer() < 15:
            #if getBuffer() < timeToFinish:
                wasDownloading = False
                SS = 1
                riskOfBufferOutage = True
                timeToFinish = i



    needToDownload = True
    print("i = " + str(i) + ", timetofinish = " + str(timeToFinish) + ", needToDownload = " + str(needToDownload))    
    while i >= timeToFinish and needToDownload:
        print("AHAH - " + str(THRFromDataset))
        
        if THRFromDataset > -1:
            print("AHAH - inside")
            if len(lastPkts) >= LAMBDA:
                print("|||||||||||")
                print(lastPkts)
                lastPkts.remove(lastPkts[0])
            print("AHAH BEFORE = " + str(lastPkts))
            lastPkts.append(THRFromDataset)
            print("AHAH AFTER  = " + str(lastPkts))
#            print(lastPkts)
        
        # We finished to download the previous packet
        if wasDownloading:
            canChange -= 1
            print("*_*_*_*_*_*_* PACKET DOWNLOADED *_*_*_*_*_*_*")
            print("CHECK i is " + str(i))
            print("CHECK S_to_download = " + str(S_to_download))
            print("CHECK SS = " + str(SS))
            print("CHECK actualTime = " + str(actualTime))
            # Let's fill correctly the buffer
            for s_down in range(SS * (S_to_download - 1) + 1, SS * (S_to_download) + 1):
                print("CHECK s_down = " + str(s_down))
                #BUFFER.insert((SS*S_to_download)+s_down,1)
                BUFFER[s_down] = qualityIndexInPlay
                BYTES[s_down] = (originalPktSize / SS)
                totalBits += originalPktSize
            fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
            fw.write(str(timeToFinish) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " # down packet " + str(S_to_download) + "\n")
            fw.close()
            #print(BUFFER)
            # TODO: check that we do not surpass LAMBDA
            # We have to update lastPkts
            #timeItTook = timeToFinish - timeDownloadStarted
            #lastPkts.append(pktSizeCurrentlyDownloading / timeItTook)
        if getBuffer() >= 30:
            print("*_*_*_*_*_*_* BUFFER FULL *_*_*_*_*_*_*")
            needToDownload = False
            wasDownloading = False
            timeDownloadStarted = i
            timeToFinish = i
    
        if needToDownload:
            # Let's first decide the quality
            print("AAA - here canChange is " + str(canChange))
            if canChange <= 0:
                print("AAA - SWITCH QUALITY, time = " + str(actualTime))
                print("AAA - canChange = " + str(canChange))
                print("AAA - S_to_download = " + str(S_to_download))
                canChange = 3
                import os
                # Here we define the rate adaptation algorithm
                qual = "min"
                if getBuffer() >= 20 and THRFromDataset > 3000000: # No Problems, maximum quality
                    qual = "max"
                elif getBuffer() >= 20: # We can increase a bit
                    qual = "stepup"
                elif getBuffer() > 15: # We stay as we were
                    qual = "same"
                elif getBuffer() > 9: # We might have some problems, step down
                    qual = "stepdown"
                else:   # Minimum quality, buffer extremely low
                    qual = "min"
                if switchMinimumQuality:
                    qualityIndexInPlay = 0
                    currentQual = listQual[qualityIndexInPlay]  

                print("AAA - NEXT QUAL = " + str(qual))
                actualQual = -1 
                listQual = []
    #             print(SS)
                for file in os.listdir(BASEDIR + str(SS) + str("sec")):
                    thisqual = int(file.split("_")[1].split("bps")[0])
                    listQual.append(thisqual)
                listQual = sorted(listQual)
    #             print(listQual)
                if qual == "min":
                    qualityIndexInPlay = 0
#                    actualQual = listQual[0]
                if qual == "max":
                    # Determine the maximum quality that we can support with this data rate
                    # We allow a BUF of maximum 20 after this
                    expectedTime = 0
                    actualQual = listQual[0]
                    for item in listQual:
                        expectedTime = (item/THRFromDataset) - (getBuffer() - 10)
                        #expectedTime = (item/THRFromDataset)
    #                     print("THR = " + str(THRFromDataset))
    #                     print(str(expectedTime) + "," + str(item))
                        if expectedTime < SS:
                            actualQual = item
                        else:
                            break
                    print("Choosen: " + str(actualQual))
                    qualityIndexInPlay = len(listQual) - 1
#                    actualQual = listQual[len(listQual) - 1]
                if qual == "stepup":
                    print("STEPUP: actualqual = " + str(qualityIndexInPlay))
                    expectedTime = 0
                    actualQual = listQual[qualityIndexInPlay]
                    for item in listQual:
                        expectedTime = (item/THRFromDataset) - (getBuffer() - 10)
                        #expectedTime = (item/THRFromDataset)
    #                     print("THR = " + str(THRFromDataset))
    #                     print(str(expectedTime) + "," + str(item))
                        if expectedTime < SS:
                            actualQual = item
                        else:
                            break
                    qualityIndexInPlay = listQual.index(actualQual)
                    #istQual[min(qualityIndexInPLay + 1, len(listQual) - 1)]
                if qual == "stepdown":
                    qualityIndexInPlay = max(qualityIndexInPlay - 1, 0)
#                    print(qualityIndexInPLay)
#                    actualQual = listQual[max(qualityIndexInPLay - 1, 0)]
                if qual == "same":
                    pass
#                    actualQual = listQual[qualityIndexInPLay]
                #currentQual = actualQual
                #qualityIndexInPLay = listQual.index(currentQual)
                #print("WE HAVE CHANGED!!! To " + str(currentQual))
                
                # Let's go with the smoothed throughput
                # Actually, this one is the istantaneous
                listQual = []
    #             print(SS)
                for fileQ in os.listdir(BASEDIR + str(SS) + str("sec")):
                    thisqual = int(fileQ.split("_")[1].split("bps")[0])
                    listQual.append(thisqual)
                listQual.sort() 
                choice = listQual[0]
                for qq in listQual:
                    expect = qq / THRFromDataset
                    if expect <= SS:
                        choice = qq
                qualityIndexInPlay = listQual.index(choice)
                actualQual = choice

            if actualTime == 1: # FIrst time, quality has to be 10
                qualityIndexInPlay = math.ceil(len(listQual) / 2)
                actualQual = listQual[qualityIndexInPlay]


                    
                
            listQual = []
    #             print(SS)
            for fileQ in os.listdir(BASEDIR + str(SS) + str("sec")):
                thisqual = int(fileQ.split("_")[1].split("bps")[0])
                listQual.append(thisqual)
            listQual.sort()
                
            #print(listQual)
            currentQual = listQual[qualityIndexInPlay]
            if SS_FIXED == -1:
                # We go step by step to decide the best segment size
                print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
                print("THR: " + str(THRFromDataset))
                print("BUF: " + str(getBuffer()))
                #print(listQual)
                qualityIndexInPlay = ""
                while qualityIndexInPlay == "":
                    qualityIndexInPlay = input("Insert new quality index: ")
                
                qualityIndexInPlay = max(0,min(int(qualityIndexInPlay),len(listQual) - 1))

                qualityIndexInPlay = int(qualityIndexInPlay)
                currentQual = listQual[qualityIndexInPlay]
            
            if SS_FIXED >= 0 and canChange <= 0:
                # We have choosen the SS, now we choose the quality
                # This method improved the quality for JAPAN
                if getBuffer() >= 20: # No Problems, maximum quality
                    expectedTime = 0
                    actualQual = listQual[qualityIndexInPlay]
                    for item in listQual:
                        expectedTime = (item/THRFromDataset) - (getBuffer() - 10)
                        #expectedTime = (item/THRFromDataset)
    #                     print("THR = " + str(THRFromDataset))
    #                     print(str(expectedTime) + "," + str(item))
                        if expectedTime < SS:
                            actualQual = item
                        else:
                            break
                    qualityIndexInPlay = listQual.index(actualQual)
                elif getBuffer() >= 16: # We can increase a bit
                    print("STEPUP: actualqual = " + str(qualityIndexInPlay))
                    expectedTime = 0
                    actualQual = listQual[qualityIndexInPlay]
                    for item in listQual:
                        expectedTime = (item/THRFromDataset) - (getBuffer() - 10)
                        #expectedTime = (item/THRFromDataset)
    #                     print("THR = " + str(THRFromDataset))
    #                     print(str(expectedTime) + "," + str(item))
                        if expectedTime < SS:
                            actualQual = item
                        else:
                            break
                    qualityIndexInPlay = listQual.index(actualQual)
                elif getBuffer() > 12: # We stay as we were
                    pass
                elif getBuffer() > 8: # We might have some problems, step down
                    qualityIndexInPlay = max(qualityIndexInPlay - 1, 0)
                else:   # Minimum quality, buffer extremely low
                    qualityIndexInPlay = 0
                
                # Second try
                expectedTime = 0
                actualQual = listQual[qualityIndexInPlay]
                for item in listQual:
                    expectedTime = (item/THRFromDataset) - (getBuffer() - 10)
                        #expectedTime = (item/THRFromDataset)
    #                     print("THR = " + str(THRFromDataset))
    #                     print(str(expectedTime) + "," + str(item))
                    if expectedTime < SS:
                        actualQual = item
                    else:
                        break
                qualityIndexInPlay = listQual.index(actualQual)
            
            print("DEBUG - So we decided that the next qualityIndexInPlay is " + str(qualityIndexInPlay))
            # Decide which segment size we have to choose
            print("*_*_*_*_*_*_* WE NEED TO DOWNLOAD *_*_*_*_*_*_*")
            print(lastPkts)
            p = 0
            if len(lastPkts) > 1 and SS_FIXED == 0: 
                AVG = int(sum(lastPkts)/len(lastPkts))
                AVG_RED = 0
                count = 1
                total = len(lastPkts)
                summaCount = 0
                for item in lastPkts:
                    AVG_RED += abs(min(0,(listQual[qualityIndexInPlay] / item) - SS))
                    AVG_WEIGHT += (count*1.0) * item
                    count += 1
                    summaCount += count
                AVG_WEIGHT /= summaCount
                AVG_WEIGHT = THRFromDataset
                AVG_RED = AVG_RED / len(lastPkts)
                print("AVG = " + str(AVG))
                print("AVG_RED = " + str(AVG_RED))

#                 print(lastPkts)
                count = 1
                for j in range(total):
                    VAR += abs(int(lastPkts[j]) - AVG)
                    VAR_RED += abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED))
                    VAR_WEIGHT += pow(((count * 1.0)/total)*(abs(int(lastPkts[j]) - AVG_WEIGHT)), alpha)
                    #VAR_WEIGHT += pow(((count * 1.0)/total)*abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED)),5)
                    #VAR_WEIGHT += pow((count * 1.0)/total,2)*abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED))
                    count += 1
                VAR_WEIGHT = math.sqrt(VAR_WEIGHT)
                print("VAR_WEIGHT is " + str(VAR_WEIGHT))
                VAR_RED = math.sqrt(VAR_RED)
                VAR = math.sqrt(VAR)
                isVarOk = False
                if (pType == 1 or pType == 5) and VAR_RED > 0:
                    isVarOk = True
                if (pType == 2 or pType == 3 or pType == 4 or pType == 7 or pType == 12) and VAR_WEIGHT > 0:
                    isVarOk = True
                if (pType == 6) and VAR > 0:
                    isVarOk = True
                if isVarOk > 0:
                    #VAR = math.sqrt(VAR)                         #       BUF      QUAL       SS     QUAL_I  PAUSE
                    beta = 1
                    if pType == 1:
                        p = beta * (AVG/(VAR_RED + AVG))           #SS0  17.9464 2.44187e+06 5.99726 13.3602 73.8056
                    elif pType == 2:
                        p = beta * (AVG/(VAR_WEIGHT + AVG))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 3:
                        p = beta * (AVG_RED/(VAR_WEIGHT + AVG_RED))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 4:
                        p = beta * (AVG_WEIGHT/(VAR_WEIGHT + AVG_WEIGHT))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 5:
                        p = beta * (AVG_RED/(VAR_RED + AVG_RED))   #SS0  20.9993 2.12835e+06 1.78363 11.173 17.1389
                    elif pType == 6:
                        p = beta * (AVG_RED / (VAR + AVG_RED))
                    elif pType == 7:
                        p = beta * (AVG_RED / (VAR_WEIGHT + AVG_RED))
                    elif pType == 12:
                        p = beta * (AVG / (VAR_WEIGHT + AVG))
                        if getBuffer() < 10:
                            p = 0
                            SS = 1
                        elif getBuffer() < 15:
                            SS = 2
                        elif getBuffer() < 20:
                            SS = 4


                else:
                    print("VAR_RED == 0")
                    p = 1
#                 print("VAR     = " + str(VAR))
                print("VAR_RED = " + str(VAR_RED))
             #   p = min(p * pow(((getBuffer() * 1.0) / 30),3), 1)
                print("DEBUG - p  = " + str(p))

                if pType == 13:
                    delta = max(lastPkts) - min(lastPkts)
                    avg = (sum(lastPkts) * 1.0) / len(lastPkts)
                    p = 1
                    if delta > 0:
                        p = (avg * 1.0) / delta
                
                # Try something different.
                # We count the number of times in which we failed to support the quality
                NUMTIMES = 0                        # LAMBDA=29 LAMBDA/3 LAMBDA/3  SS0  25.2748 2.23818e+06 1.91963 11.5398 0.666667
                                                    # LAMBDA=20 LAMBDA/2 LAMBDA/3  SS0  25.0096 2.25937e+06 2.32035 11.7669 0.722222
                                                    # LAMBDA=30 LAMBDA/2 LAMBDA/3  SS0  24.8463 2.25369e+06 2.54872 11.7771 0.833333
                                                    # LAMBDA=4  LAMBDA/2 LAMBDA/3  SS0  25.6655 2.19898e+06 1.93033 11.4699 0.472222
                SSalredyDefined = False            # With NUMTIMES    SS0  18.7531 2.36711e+06 9.37561 12.8515 93.7222
                SSold = SS

                if pType == 8 and SS_FIXED == 0:
                    for item in lastPkts:
                        if (listQual[qualityIndexInPlay] / item) - SS < 0:
                            NUMTIMES += 1
                    if NUMTIMES < (LAMBDA / 2):
                        SS = SS_A[min(len(SS_A) - 1,SS_A.index(SS) + 1)] 
                    elif NUMTIMES > (LAMBDA / 3):
                        SS = SS_A[max(0,SS_A.index(SS) - 1)]
                    SSalredyDefined = True            # With NUMTIMES    SS0  18.7531 2.36711e+06 9.37561 12.8515 93.7222
                # SS0  19.7643 2.24244e+06 2.5025 11.7449 42.4167
                #if SS_FIXED == 0:
                #    fvar = open('myvar.csv','a+')
                #    fvar.write(str(i) + " " + str(VAR) + " " + str(VAR_RED) + " " + str(VAR_WEIGHT) + " " + str(p) + " " + str(AVG) + " " + str(AVG_VAR) + " " + str(AVG_WEIGHT) + "\n")
                #    fvar.close()
                if not firstTime:
                    if not SSalredyDefined:
                        SS_before = SS_A[max(0,SS_A.index(SS) - 1)]
                        SS_after = SS_A[min(len(SS_A) - 1,SS_A.index(SS) + 1)]
                        SSprime = (1-p) * SS_before + p * SS_after
                        print("Actual SS = " + str(SS))
                        print("SSprime = " + str(SSprime))
    #                     
    #                     # Now we pick the closes SS to SSprime
                        delta = max(SS_A)
                        choosen = -1
                        for item in SS_A:
                            tmp_delta = abs(item - SSprime)
                            print(str(item) + " " + str(tmp_delta))
                            print("Delta = " + str(delta))
                            print("Choosen = " + str(choosen))
                            if tmp_delta < delta:
                                choosen = item
                                delta = tmp_delta
                            if SS_FIXED == 0:
                                if choosen == -1:
                                    SS = 1
                                else:
                                    SS = choosen
                   #     if p > 10.5:
                   #         print(SS)
                   #         print(len(SS_A))
                   #         print(min(len(SS_A) - 1, SS_A.index(SS) + 1))
#
#                            SS = SS_A[min(len(SS_A) - 1, SS_A.index(SS) + 1)]
#                        else:
#                            SS = SS_A[max(0, SS_A.index(SS) - 1)]
                else:
                    if SS_FIXED == 0:
                        SS = 1
                    elif SS_FIXED > 0:
                        SS = SS_FIXED

                    firstTime = False       
                    #SS0  19.7643 2.24244e+06 2.5025 11.7449 42.4167
                
                # Let's try something different
                # p just determines where we should pick the next SS
                # so SS_A = 6, so every 1/6 we switch from one SS to another
                # how does p compare to 1/6 - 2/6 - 3/6 - 4/6 - 5/6 - 6/6
                # 0/6 -> 1/6 = 1
                # 1/6 -> 2/6 = 2
                # 2/6 -> 3/6 = 4
                # 3/6 -> 4/6 = 6
                # 4/6 -> 5/6 = 10
                # 5/6 -> 6/6 = 15
#                     beta = 2.1
#                     if p <= (1)/(6.0+beta):
#                         SS = 1
#                     elif p <= (2)/(6.0+beta):
#                         SS = 2
#                     elif p <= (4)/(6.0+beta):
#                         SS = 4
#                     elif p <= (6)/(6.0+beta):
#                         SS = 6
#                     elif p <= (10)/(6.0+beta):
#                         SS = 10
#                     else:
#                         SS = 15
                #SS0  20.9993 2.12835e+06 1.78363 11.173 17.1389
                
                
                # TODO Try 1: decide SS based on the BUF
                #SS0  19.3801 2.29665e+06 14.8478 12.8165 106.222
                print("WE ARE NOW SWITCHING SS")
                print("BUF = " + str(getBuffer()))
                if SS_FIXED == 0 and pType == 9:
                    if getBuffer() < 7:
                        SS = 1
                    elif getBuffer() < 12:
                        SS = SS_A[min(len(SS_A) - 2,SS_A.index(SS) + 2)]
                        SS = SS_A[len(SS_A) - 1]
                        SS = 2
                    elif getBuffer() < 18:
                        SS = SS_A[int(len(SS_A) / 2)]
                        SS = 4
                    elif getBuffer() < 22:
                        SS = 6
                    elif getBuffer() < 28:
                        SS = 10
                    else:
                        SS = 15
                    print("NEW SS = " + str(SS))
            else:
                if SS_FIXED > 0:
                    SS = SS_FIXED
                else:
                    SS = 1  # We start with a segment size of 1

            # TODO Select new SS
            # Another try. We select the SS just based on the buffer.
#             if SS_FIXED == 0:
#                 if getBuffer() >= 14 and p > 0.0001: # No Problems, maximum quality
#                     SS = 15
#                 elif getBuffer() > 10: # We can increase a bit
#                     SS = 10
#                 elif getBuffer() > 6: # We stay as we were
#                     SS = 6
#                 elif getBuffer() > 3: # We might have some problems, step down
#                     SS = 4
#                 else:   # Minimum quality, buffer extremely low
#                     SS = 2
                    
            # Another try. Maybe we have to select the segment size only based on the throughput.
            # Since longer segments are smaller in size, when the THR is low we might consider
            # to switch to longer segments.
            if SS_FIXED == 0 and pType == 10:
                if getBuffer() >= 22 and THRFromDataset < 2000000: # No Problems, maximum quality
                    SS = 15
                elif getBuffer() > 20 and THRFromDataset < 1000000: # We can increase a bit
                    SS = 10
                elif getBuffer() > 16 and THRFromDataset < 500000: # We stay as we were
                    SS = 6
                elif getBuffer() > 12: # We might have some problems, step down
                    SS = 4
                elif getBuffer() > 8:
                    SS = 2
                else:   # Minimum quality, buffer extremely low
                    SS = 1
            
            if SS_FIXED == 0 and  pType == 11:
                if getBuffer() >= 22 and qualityIndexInPlay >16:
                    SS = 15
                elif getBuffer() > 20 and qualityIndexInPlay > 14:
                    SS = 10
                elif getBuffer() > 16 and qualityIndexInPlay > 12:
                    SS = 6
                elif getBuffer() > 12 and qualityIndexInPlay > 10:
                    SS = 4
                elif getBuffer() > 8 and qualityIndexInPlay > 8:
                    SS = 2
                else:
                    SS = 1 

            if SS_FIXED == 0 and pType == 13:
                # Here we look at the previous throughputs and count the times in which we would have
                # succeed downloading the segment and the times we wouldn't. This gives us a ratio.
                timeOk = 0
                timeKo = 0
                for pkt in lastPkts:
                    # Let's pick SS = 1 as reference
                    # Quality is in actualQual
                    time = pkt / THRFromDataset
                    if time > getBuffer():
                        timeOk += 1
                    else:
                        timeKo += 1
                rr = (timeOk * 1.0) / (timeOk + timeKo)
                #if rr > 0.5:
                if rr > ratioForP:
                    SS = SS_A[min(SS_A.index(SS) + 1, len(SS_A) - 1)]
                else:
                    SS = SS_A[max(SS_A.index(SS) - 1, 0)]

                    
#            if SS_FIXED == 0 and timeToFinish - timeDownloadStarted > SS and fastBackoff:
#                SS = 1            
        
            # TODO Identify segment to be downloaded
            # TODO, for now, we math.floor(), so we pick an earlier segment and we might download again some data
            print("=)=)=)=)")
            print(actualTime)
            print(SS)
            print(getBuffer())
            print(VAR_RED)
            print(VAR)
            print(VAR_WEIGHT)
            print(switchMinimumQuality)
            if SS_FIXED == 0 and switchMinimumQuality and fastBackoff:
                SS = 1
#            elif VAR_WEIGHT == 0 and not firstTime:
#                if SS_FIXED == 0:
#                    print("Stable connection and low throughput. Maximum segment size.")
#                    SS = 15
                    
            if SS_FIXED == -1:
                # We go step by step to decide the best segment size
                print("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")
                print("THR: " + str(THRFromDataset))
                print("BUF: " + str(getBuffer()))
                print("actualTime: " + str(actualTime))
                SS = ""
                while SS == "":
                    SS = input("Insert new SS: ")
                SS = max(0,min(int(SS),SS_A[len(SS_A) - 1]))
                print(SS)
            
            if SS_FIXED > 0:
                SS = SS_FIXED
            print("ABBA - before everything the SS is " + str(SS))
            S_to_download = math.ceil((actualTime + getBuffer()) / SS)
            #if SS <= 1:
            #    S_to_download = math.floor((actualTime + getBuffer()) / SS)
            #else:
            #    S_to_download = math.floor((actualTime + getBuffer()) / SS) + 1
            print(S_to_download)

            print("CHECKTHIS - A - ACT - BUF - SS - S_to_DOWN")
            print("CHECKTHIS - A - " + str(actualTime) + " - " + str(getBuffer()) + " - " + str(SS) + " " + str(S_to_download))

            # Ok, now we got in SSold the old SS, and in SS the new SS. Let's see how many seconds 
            # already downloaded we have to lose
            amount = 0
            print("*** DEBUGOVERLAP ***")
            print("DEBUGOVERLAP - BUFFER IS " + str(BUFFER))
            print("DEBUGOVERLAP - SS is " + str(SS))
            for BB in range(S_to_download * SS, (S_to_download + 1) * SS):
                #print("CHECKTHIS - BUF at " + str(BB) + " = " + str(BUFFER[BB]))
                print("DEBUGOVERLAP - " + str(BUFFER[BB]))
                if BUFFER[BB] >= 0:
                    amount += 1
            print("CHECKTHIS - C - " + str(amount))
            mmm = (amount * 1.0) / SS
            print("DEBUGOVERLAP - " + str(mmm))
            if mmm >= maximumOverlap and SS_FIXED == 0:
                print("ABBA - too much overlap, switching to old SS")
                SS = SSold
                S_to_download = math.floor((actualTime + getBuffer()) / SS)
                print("CHECKTHIS - B - " + str(actualTime) + " - " + str(getBuffer()) + " - " + str(SS) + " " + str(S_to_download))
            
            # TODO Let's choose the quality depending on the buffer and on the 
            # for item in os.listdir() ....
#            qualityIndexInPLay = 0
            if SS_FIXED == 0 and riskOfBufferOutage:
                SS = 1
                S_to_download = math.floor((actualTime + getBuffer()) / SS)
            listQual = []
#             print(SS)
            for fileQ in os.listdir(BASEDIR + str(SS) + str("sec")):
                thisqual = int(fileQ.split("_")[1].split("bps")[0])
                listQual.append(thisqual)
            listQual.sort()
            print("DEBUG - The new SS will be " + str(SS))
            fileQuality = BASEDIR + str(SS) + "sec/bunny_" + str(int(listQual[qualityIndexInPlay])) + "bps"
            print("DEBUG - The file from which we get the data is " + str(fileQuality))
            print("Using quality: " + str(fileQuality))
            print("Current Quality = " + str(currentQual))
            print("Quality index in play = " + str(qualityIndexInPlay))
        
            # Download pkt
            bits = 0
            ff2 = open(fileQuality, 'r')
            print(fileQuality)
            print("ABBA - ************")
            print("ABBA - S_to_download = " + str(S_to_download))
            print("ABBA - actualTime = " + str(actualTime))
            print("ABBA - getBuffer() = " + str(getBuffer()))
            print("ABBA - SS = " + str(SS))
            for line2 in ff2.readlines():
                ll2 = line2.split()
                if int(S_to_download) == int(ll2[0]):
                    bits = int(ll2[1])
            if bits == 0:
                # We do not have further segments to download
                print("INSIDE")
                weHaveToBreak = True
                break
            else:
                switchMinimumQuality = False
                weHaveToBreak = False
        
            print("BITS = " + str(bits))
            pktSizeCurrentlyDownloading = bits
            originalPktSize = bits
            time_data = bits/THRFromDataset
            timeDownloadStarted = timeToFinish
            timeToFinish = timeToFinish + time_data
            expectedOriginalTime = time_data
            wasDownloading = True
            print("time_data = " + str(time_data))
#            print("THR = " + str(THRFromDataset))
#            print("actualTime = " + str(actualTime))
#            print("NEED, time = " + str(i))
            print("timeToFInish = " + str(timeToFinish))
    
    fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
    print("BUF = " + str(getBuffer()))   
    if getBuffer() > 0:
        print("HERE")
        BUF = max(0,BUF - 1)
        actualTime += 1
        #fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " # " + str(line).strip() + " " + str(VAR_WEIGHT) + " " + str(AVG) + "\n")
        fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + "\n")
    else:
        # We have a pause. Let's switch back to the lowest possible quality
        #fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " 0 " + str(SS) + " 0 " + str(0) + " # " + str(line).strip() + " " + str(VAR_WEIGHT) + " " + str(AVG) + "\n")
        fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " 0 " + str(SS) + " 0 " + str(0) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + "\n")
    if getBuffer() == 0:
        switchMinimumQuality = True
        
    fw.close()
