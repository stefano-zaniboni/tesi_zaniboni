#!/bin/bash
DATASET="colorado"
sigma=0
rm *.dat

for item in 0
do
	for p in 0 1 2 
	do
		for ss in 1 2 4 6 10 15
		do
			./extract_data_from_results.py values.SS${ss}.${DATASET}.A${p}.data ${DATASET}
		done
	done
done
