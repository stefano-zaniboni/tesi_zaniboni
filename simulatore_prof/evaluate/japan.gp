# TEST GNUPLOT AUTOMATICO
 
set terminal postscript enhanced color eps
set grid ytics lc rgb "#bbbbbb" lw 1 lt 0
set grid xtics lc rgb "#bbbbbb" lw 1 lt 0
set style line 1 lt 2 lw 2 pt 3 ps 0.5

# FreeMbit SS FIXED
set o 'SS-Fixed-FreeMbps-QualityIndex.eps'
set xlabel "SS-Fixed"
set ylabel "Quality Index"
plot \
'fmbps_0.avg' u 4:5  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'fmbps_1.avg' u 4:5  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'fmbps_2.avg' u 4:5  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'

set o 'SS-Fixed-FreeMbps-Buffer.eps'
set xlabel "SS-Fixed"
set ylabel "Buffer"
plot \
'fmbps_0.avg' u 4:6  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'fmbps_1.avg' u 4:6  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'fmbps_2.avg' u 4:6  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'

set o 'SS-Fixed-FreeMbps-Bitrate.eps'
set xlabel "SS-Fixed"
set ylabel "Bitrate"
plot \
'fmbps_0.avg' u 4:7  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'fmbps_1.avg' u 4:7  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'fmbps_2.avg' u 4:7  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'

##
# 3MBit SS FIXED
set o 'SS-Fixed-3mbps-QualityIndex.eps'
set xlabel "SS-Fixed"
set ylabel "Quality Index"
plot \
'3mbps_0.avg' u 4:5  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'3mbps_1.avg' u 4:5  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'3mbps_2.avg' u 4:5  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'

set o 'SS-Fixed-3mbps-Buffer.eps'
set xlabel "SS-Fixed"
set ylabel "Buffer"
plot \
'3mbps_0.avg' u 4:6  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'3mbps_1.avg' u 4:6  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'3mbps_2.avg' u 4:6  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'

set o 'SS-Fixed-3mbps-Bitrate.eps'
set xlabel "SS-Fixed"
set ylabel "Bitrate"
plot \
'3mbps_0.avg' u 4:7  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'3mbps_1.avg' u 4:7  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'3mbps_2.avg' u 4:7  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'


##
# 1 MBit SS FIXED
set o 'SS-Fixed-1mbps-QualityIndex.eps'
set xlabel "SS-Fixed"
set ylabel "Quality Index"
plot \
'1mbps_0.avg' u 4:5  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'1mbps_1.avg' u 4:5  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'1mbps_2.avg' u 4:5  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'

set o 'SS-Fixed-1mbps-Buffer.eps'
set xlabel "SS-Fixed"
set ylabel "Buffer"
plot \
'1mbps_0.avg' u 4:6  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'1mbps_1.avg' u 4:6  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'1mbps_2.avg' u 4:6  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'

set o 'SS-Fixed-1mbps-Bitrate.eps'
set xlabel "SS-Fixed"
set ylabel "Bitrate"
plot \
'1mbps_0.avg' u 4:7  w l  lt 4 lw 5 lc rgb "black" t 'Algoritmo Istantaneous',\
'1mbps_1.avg' u 4:7  w l  lt 4 lw 5 lc rgb "red" t 'Oracolo Greedy',\
'1mbps_2.avg' u 4:7  w l  lt 4 lw 5 lc rgb "blue" t 'Oracolo Smart'


# quality index
set o 'q-thr0.eps' # segmento dinamico 
plot \
'values.SS0.japan.A0.data.dat' u 1:2 w l t 'A 0',\
'values.SS0.japan.A1.data.dat' u 1:2 w l t 'A 1',\
'values.SS0.japan.A2.data.dat' u 1:2 w l t 'A 2'

set o 'q-thr1.eps'
plot \
'values.SS1.japan.A0.data.dat' u 1:2 w l t 'A 0',\
'values.SS1.japan.A1.data.dat' u 1:2 w l t 'A 1',\
'values.SS1.japan.A2.data.dat' u 1:2 w l t 'A 2'

set o 'q-thr15.eps'
plot \
'values.SS15.japan.A0.data.dat' u 1:2 w l t 'A 0',\
'values.SS15.japan.A1.data.dat' u 1:2 w l t 'A 1',\
'values.SS15.japan.A2.data.dat' u 1:2 w l t 'A 2'


# 'values.SS0.japan.A0.data.dat' u 1:2  w l  t 'A 0D',\
####################### buf_thr
# N.B devo trasformare values.SSx.japan.Ax.data in modo da togliere la colonna japan/100000.SS1 --> FATTO
# set boxwidth 
# set style fill solid
set o 'buf-thr0.eps'
set ylabel "Buffer"
set xlabel "Throughput"
plot \
'values.SS0.japan.A0.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "black" t 'A 0',\
'values.SS0.japan.A1.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "red" t 'A 1',\
'values.SS0.japan.A2.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "blue" t 'A 2'

set o 'buf-thr1.eps'
set ylabel "Buffer"
set xlabel "Throughput"
plot \
'values.SS1.japan.A0.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "black" t 'A 0',\
'values.SS1.japan.A1.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "red" t 'A 1',\
'values.SS1.japan.A2.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "blue" t 'A 2'

set o 'buf-thr15.eps'
set ylabel "Buffer"
set xlabel "Throughput"
plot \
'values.SS15.japan.A0.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "black" t 'A 0',\
'values.SS15.japan.A1.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "red" t 'A 1',\
'values.SS15.japan.A2.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "blue" t 'A 2'

set o 'pause-thr0.eps'
set ylabel "Numero Pause"
set xlabel "Throughput"
plot \
'values.SS0.japan.A0.data.dat' u 1:3 w l t 'A 0',\
'values.SS0.japan.A1.data.dat' u 1:3 w l t 'A 1',\
'values.SS0.japan.A2.data.dat' u 1:3 w l t 'A 2'
#'values.SS0.japan.A3.data.dat' u 1:3 w l t 'A 3'

set o 'pause-thr1.eps'
set ylabel "Numero Pause"
set xlabel "Throughput"
plot \
'values.SS1.japan.A0.data.dat' u 1:3 w l t 'A 0',\
'values.SS1.japan.A1.data.dat' u 1:3 w l t 'A 1',\
'values.SS1.japan.A2.data.dat' u 1:3 w l t 'A 2'
#'values.SS1.japan.A3.data.dat' u 1:3 w l t 'A 3'

set o 'pause-thr15.eps'
set ylabel "Numero Pause"
set xlabel "Throughput"
plot \
'values.SS15.japan.A0.data.dat' u 1:3 w l t 'A 0',\
'values.SS15.japan.A1.data.dat' u 1:3 w l t 'A 1',\
'values.SS15.japan.A2.data.dat' u 1:3 w l t 'A 2'

# serie di 3 grafici dove viene fissato l'algoritmo e in ognuno vi sono tante linee quante sono le lunghezze di segmento
set autoscale
set o 'confronto-Instantaneous-dynamic-vs-fixed.eps'
set xlabel "Throughput"
set ylabel "Quality Index"
plot \
'values.SS0.japan.A0.data.dat' u 1:2  w l  t 'SS Dyn',\
'values.SS1.japan.A0.data.dat' u 1:2  w l  t 'SS 1',\
'values.SS15.japan.A0.data.dat' u 1:2  w l  t 'SS 15'

set o 'confronto-greedy-dynamic-vs-fixed.eps'
set xlabel "Throughput"
set ylabel "Quality Index"
plot \
'values.SS0.japan.A1.data.dat' u 1:2  w l  t 'SS Dyn',\
'values.SS1.japan.A1.data.dat' u 1:2  w l  t 'SS 1',\
'values.SS15.japan.A1.data.dat' u 1:2  w l  t 'SS 15'

set o 'confronto-smart-dynamic-vs-fixed.eps'
set xlabel "Throughput"
set ylabel "Quality Index"
plot \
'values.SS0.japan.A2.data.dat' u 1:2  w l  t 'SS Dyn',\
'values.SS1.japan.A2.data.dat' u 1:2  w l  t 'SS 1',\
'values.SS15.japan.A2.data.dat' u 1:2  w l  t 'SS 15'

# Grafici Bitrate e Buffer 
# Dynamic Instantaneous SS0 + Instantaneous SS1 + Instantaneous SS15
set o 'FreeMbit-Bitrate-dynamic-instantaneous-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Bitrate"
plot \
'values.SS0.japan.A0.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "black"  t "A0+Dynamic",\
'values.SS1.japan.A0.data.dat' u 1:5   w l  lt 4 lw 5 lc rgb "red"  t "A0 SS1",\
'values.SS15.japan.A0.data.dat' u 1:5   w l  lt 4 lw 5 lc rgb "blue"  t "A0 SS15"

set o 'FreeMbit-Buffer-dynamic-instantaneous-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Buffer"
plot \
'values.SS0.japan.A0.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
'values.SS1.japan.A0.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
'values.SS15.japan.A0.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o 'FreeMbit-QI-dynamic-instantaneous-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Quality Index"
plot \
'values.SS0.japan.A0.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
'values.SS1.japan.A0.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
'values.SS15.japan.A0.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"


# Grafici Bitrate e Buffer
# Dynamic Oracolo Greedy SS0 + Oracolo Greedy SS1 + Oracolo Greedy SS15 
set o 'FreeMbit-Bitrate-dynamic-greedy-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Bitrate"
plot \
'values.SS0.japan.A1.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "black" t "A1+Dynamic",\
'values.SS1.japan.A1.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "red" t "A1 SS1",\
'values.SS15.japan.A1.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "blue" t "A1 SS15"

set o 'FreeMbit-Buffer-dynamic-greedy-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Buffer"
plot \
'values.SS0.japan.A1.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A1+Dynamic",\
'values.SS1.japan.A1.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A1 SS1",\
'values.SS15.japan.A1.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A1 SS15"

set o 'FreeMbit-QI-dynamic-greedy-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Quality Index"
plot \
'values.SS0.japan.A1.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A1+Dynamic",\
'values.SS1.japan.A1.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A1 SS1",\
'values.SS15.japan.A1.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A1 SS15"

# Grafici Bitrate e Buffer
# Dynamic Oracolo Smart + Oracolo Smart SS1 + Oracolo Smart SS15 
set o 'FreeMbit-Bitrate-dynamic-smart-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Bitrate"
plot \
'values.SS0.japan.A2.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "black" t "A2+Dynamic",\
'values.SS1.japan.A2.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "red" t "A2 SS1",\
'values.SS15.japan.A2.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "blue" t "A2 SS15"

set o 'FreeMbit-Buffer-dynamic-smart-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Buffer"
plot \
'values.SS0.japan.A2.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A2+Dynamic",\
'values.SS1.japan.A2.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A2 SS1",\
'values.SS15.japan.A2.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A2 SS15"

set o 'FreeMbit-QI-dynamic-smart-vs-1-15.eps'
set xlabel "Throughput"
set ylabel "Quality Index"
plot \
'values.SS0.japan.A2.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A2+Dynamic",\
'values.SS1.japan.A2.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A2 SS1",\
'values.SS15.japan.A2.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A2 SS15"

# Grafici Bitrate e Buffer 
# 3MBit Instantaneous SS0 + Instantaneous SS1 + Instantaneous SS15
set o "3mbit-Bitrate-dynamic-instantaneous-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Bitrate"
plot\
"D3mbps_0_0.avg" u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_0_1.avg" u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_0_15.avg" u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "3mbit-Buffer-dynamic-instantaneous-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Buffer"
plot\
"D3mbps_0_0.avg" u 1:3  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_0_1.avg" u 1:3  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_0_15.avg" u 1:3  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "3mbit-QI-dynamic-instantaneous-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Quality Index"
plot\
"D3mbps_0_0.avg" u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_0_1.avg" u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_0_15.avg" u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

# Grafici Bitrate e Buffer
# 3Mbit Oracolo Greedy SS0 + Oracolo Greedy SS1 + Oracolo Greedy SS15 
set o "3mbit-Bitrate-dynamic-greedy-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Bitrate"
plot\
"D3mbps_1_0.avg" u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_1_1.avg" u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_1_15.avg" u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "3mbit-Buffer-dynamic-greedy-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Buffer"
plot\
"D3mbps_1_0.avg" u 1:3  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_1_1.avg" u 1:3  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_1_15.avg" u 1:3  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "3mbit-QI-dynamic-greedy-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Quality Index"
plot\
"D3mbps_1_0.avg" u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_1_1.avg" u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_1_15.avg" u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

# Grafici Bitrate e Buffer
# 3Mbit Oracolo Smart SS0 + Oracolo Greedy SS1 + Oracolo Greedy SS15 
set o "3mbit-Bitrate-dynamic-smart-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Bitrate"
plot\
"D3mbps_2_0.avg" u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_2_1.avg" u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_2_15.avg" u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "3mbit-Buffer-dynamic-smart-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Buffer"
plot\
"D3mbps_2_0.avg" u 1:3  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_2_1.avg" u 1:3  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_2_15.avg" u 1:3  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "3mbit-QI-dynamic-smart-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Quality Index"
plot\
"D3mbps_2_0.avg" u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D3mbps_2_1.avg" u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D3mbps_2_15.avg" u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

# Grafici Bitrate e Buffer 
# 1MBit Instantaneous SS0 + Instantaneous SS1 + Instantaneous SS15
set o "1mbit-Bitrate-dynamic-instantaneous-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Bitrate"
plot\
"D1mbps_0_0.avg" u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_0_1.avg" u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_0_15.avg" u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "1mbit-Buffer-dynamic-instantaneous-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Buffer"
plot\
"D1mbps_0_0.avg" u 1:3  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_0_1.avg" u 1:3  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_0_15.avg" u 1:3  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "1mbit-QI-dynamic-instantaneous-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Quality Index"
plot\
"D1mbps_0_0.avg" u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_0_1.avg" u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_0_15.avg" u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

# Grafici Bitrate e Buffer
# 1Mbit Oracolo Greedy SS0 + Oracolo Greedy SS1 + Oracolo Greedy SS15 
set o "1mbit-Bitrate-dynamic-greedy-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Bitrate"
plot\
"D1mbps_1_0.avg" u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_1_1.avg" u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_1_15.avg" u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "1mbit-Buffer-dynamic-greedy-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Buffer"
plot\
"D1mbps_1_0.avg" u 1:3  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_1_1.avg" u 1:3  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_1_15.avg" u 1:3  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "1mbit-QI-dynamic-greedy-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Quality Index"
plot\
"D1mbps_1_0.avg" u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_1_1.avg" u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_1_15.avg" u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

# Grafici Bitrate e Buffer
# 1Mbit Oracolo Smart SS0 + Oracolo Greedy SS1 + Oracolo Greedy SS15 
set o "1mbit-Bitrate-dynamic-smart-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Bitrate"
plot\
"D1mbps_2_0.avg" u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_2_1.avg" u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_2_15.avg" u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "1mbit-Buffer-dynamic-smart-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Buffer"
plot\
"D1mbps_2_0.avg" u 1:3  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_2_1.avg" u 1:3  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_2_15.avg" u 1:3  w l  lt 4 lw 5 lc rgb "blue" t "A0 SS15"

set o "1mbit-QI-dynamic-smart-vs-1-15.eps"
set xlabel "Throughput"
set ylabel "Quality Index"
plot\
"D1mbps_2_0.avg" u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
"D1mbps_2_1.avg" u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A0 SS1",\
"D1mbps_2_15.avg" u 1:2  w l  lt 4 lw 5 lc rgb "blue "t "A0 SS15"

# Grafici di confronto dei 3 algoritmi con Dynamic a supporto * {Bitrate, Buffer, Quality Index}
set o "dynamicInstantaneous-vs-dynamicGreedy-vs-dynamicSmart-QI.eps"
set xlabel "Throughput"
set ylabel "Quality Index"
plot\
'values.SS0.japan.A0.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
'values.SS0.japan.A1.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "red" t "A1+Dynamic",\
'values.SS0.japan.A2.data.dat' u 1:2  w l  lt 4 lw 5 lc rgb "blue" t "A2+Dynamic"

set o "dynamicInstantaneous-vs-dynamicGreedy-vs-dynamicSmart-Buffer.eps"
set xlabel "Throughput"
set ylabel "Buffer"
plot\
'values.SS0.japan.A0.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
'values.SS0.japan.A1.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "red" t "A1+Dynamic",\
'values.SS0.japan.A2.data.dat' u 1:4  w l  lt 4 lw 5 lc rgb "blue" t "A2+Dynamic"

set o "dynamicInstantaneous-vs-dynamicGreedy-vs-dynamicSmart-Bitrate.eps"
set xlabel "Throughput"
set ylabel "Bitrate"
plot\
'values.SS0.japan.A0.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "black" t "A0+Dynamic",\
'values.SS0.japan.A1.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "red" t "A1+Dynamic",\
'values.SS0.japan.A2.data.dat' u 1:5  w l  lt 4 lw 5 lc rgb "blue" t "A2+Dynamic"

# serie di 3 grafici dove viene fissato l'algoritmo e in ognuno vi sono tante linee quante sono le lunghezze di segmento
set autoscale
set o 'time-Instantaneous-dynamic-vs-fixed.eps'
set xlabel "Throughput"
set ylabel "Time"
set xrange [50:400]
plot \
'values.SS0.japan.A0.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "black" t 'SS Dyn',\
'values.SS1.japan.A0.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "red" t 'SS 1',\
'values.SS15.japan.A0.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "blue" t 'SS 15'

set o 'time-greedy-dynamic-vs-fixed.eps'
set xlabel "Throughput"
set ylabel "Time"
set xrange [50:400]
plot \
'values.SS0.japan.A1.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "black" t 'SS Dyn',\
'values.SS1.japan.A1.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "red" t 'SS 1',\
'values.SS15.japan.A1.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "blue" t 'SS 15'

set o 'time-smart-dynamic-vs-fixed.eps'
set xlabel "Throughput"
set ylabel "Time"
set xrange [50:400]
plot \
'values.SS0.japan.A2.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "black" t 'SS Dyn',\
'values.SS1.japan.A2.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "red" t 'SS 1',\
'values.SS15.japan.A2.data.dat' u 1:6 smooth bezier w l lt 4 lw 5 lc rgb "blue" t 'SS 15'







