#!/usr/local/bin/python3
import os
from pprint import pprint
import sys

#TODO: passo i file dat per ogni lunghezza di segmento {1, 2. 4. 6. 10, 15} e ne calcolo la media del Quality Index fino a 10mbps(free), fino a 3mbps, fino a 1mbps 
if sys.argv[2] == "datasetFake" or sys.argv[2] == "colorado" or sys.argv[2] == "japan":
	file_avg_f = open('fmbps_' + sys.argv[3] + ".avg", 'a')
	file_avg_3 = open('3mbps_' + sys.argv[3] + ".avg", 'a')
	file_avg_1 = open('1mbps_' + sys.argv[3] + ".avg", 'a')
	file_handle = open(sys.argv[1], "r")

	sum_f = 0 # qi 
	count_f = 0 # contatore
	buff = 0 # buffer
	bitratef = 0 # bitrate
	
	sum_3 = 0
	count_3 = 0
	buf3 = 0
	bitrate3 = 0
	
	sum_1 = 0
	count_1 = 0
	buf1 = 0
	bitrate1 = 0

	for line in file_handle:
		l = line.split(' ')
		if float(l[0]) < 1000000:
			sum_1 += float(l[1]) # conto solo i file minori di 1megabit
			buf1 += float(l[2])
			bitrate1 += float(l[3])
			count_1 += 1
		if float(l[0]) < 3000000:
			sum_3 += float(l[1]) # conto solo i file minori di 3megabit
			buf3 += float(l[2])
			bitrate3 += float(l[3])
			count_3 += 1
		sum_f += float(l[1]) # conto tutti i file 
		buff += float(l[2])
		bitratef += float(l[3])
		count_f += 1
	# print(str(sys.argv[1]) + ' ' + str(sum) + ' ' + str(count) + ' = ' + str(sum/count))

	file_avg_f.write(sys.argv[1] + ' ' + str(sum_f) + ' ' + str(count_f) + ' ' + str(sys.argv[4]) + ' ' + str(sum_f/count_f) + ' ' + str(buff/count_f) + ' ' + str(bitratef*8/count_f) + '\n')
	file_avg_3.write(sys.argv[1] + ' ' + str(sum_3) + ' ' + str(count_3) + ' ' + str(sys.argv[4]) + ' ' + str(sum_3/count_3) + ' ' + str(buf3/count_3) + ' ' + str(bitrate3*8/count_3) + '\n')
	file_avg_1.write(sys.argv[1] + ' ' + str(sum_1) + ' ' + str(count_1) + ' ' + str(sys.argv[4]) + ' ' + str(sum_1/count_1) + ' ' + str(buf1/count_1) + ' ' + str(bitrate1*8/count_1) + '\n')
	
	file_avg_f.close()
	file_avg_3.close()
	file_avg_1.close()

if sys.argv[2] == "colorado" or sys.argv[2] == "japan":
	file_avg_f = open('fmbps_' + sys.argv[3] + ".avg", 'a')
	file_avg_3 = open('3mbps_' + sys.argv[3] + ".avg", 'a')
	file_avg_1 = open('1mbps_' + sys.argv[3] + ".avg", 'a')
	file_handle = open(sys.argv[1], "r")

	sum_f = 0 # qi 
	count_f = 0 # contatore
	buff = 0 # buffer
	bitratef = 0 # bitrate
	
	sum_3 = 0
	count_3 = 0
	buf3 = 0
	bitrate3 = 0
	
	sum_1 = 0
	count_1 = 0
	buf1 = 0
	bitrate1 = 0

	for line in file_handle:
		l = line.split(' ')
		if float(l[0]) < 1000:
			sum_1 += float(l[1]) # conto solo i file minori di 1megabit
			buf1 += float(l[3])
			bitrate1 += float(l[4])
			count_1 += 1
		if float(l[0]) < 3000:
			sum_3 += float(l[1]) # conto solo i file minori di 3megabit
			buf3 += float(l[3])
			bitrate3 += float(l[4])
			count_3 += 1
		sum_f += float(l[1]) # conto tutti i file 
		buff += float(l[3])
		bitratef += float(l[4])
		count_f += 1
	# print(str(sys.argv[1]) + ' ' + str(sum) + ' ' + str(count) + ' = ' + str(sum/count))

	file_avg_f.write(sys.argv[1] + ' ' + str(sum_f) + ' ' + str(count_f) + ' ' + str(sys.argv[4]) + ' ' + str(sum_f/count_f) + ' ' + str(buff/count_f) + ' ' + str(bitratef*8/count_f) + '\n')
	file_avg_3.write(sys.argv[1] + ' ' + str(sum_3) + ' ' + str(count_3) + ' ' + str(sys.argv[4]) + ' ' + str(sum_3/count_3) + ' ' + str(buf3/count_3) + ' ' + str(bitrate3*8/count_3) + '\n')
	file_avg_1.write(sys.argv[1] + ' ' + str(sum_1) + ' ' + str(count_1) + ' ' + str(sys.argv[4]) + ' ' + str(sum_1/count_1) + ' ' + str(buf1/count_1) + ' ' + str(bitrate1*8/count_1) + '\n')
	
	file_avg_f.close()
	file_avg_3.close()
	file_avg_1.close()


	
