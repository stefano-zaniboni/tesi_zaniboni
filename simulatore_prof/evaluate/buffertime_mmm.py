#!/usr/local/bin/python3
import sys
import math
import os
import random
import time
from pprint import pprint
save = 0
# 0: se si vuole l'originale 
# 1: oracolo greedy
# 2: oracolo 'smart'
oracle_selector = int(sys.argv[3])
vettore_pesi_pacchetto = []
num_pkts = 0

def getBuffer():
    count = 0
#     print("---------------------")
    #print(BUFFER)
    for i in range(actualTime, len(BUFFER)):
        if BUFFER[i] >= 0:
            count += 1
        else:
            return count
    return len(BUFFER) - actualTime

def esplorazione(SS, num_pkts, qualityIndexInPlay, S_to_download):
    print('#######')
    print('Siamo al pacchetto: ' + str(S_to_download))
    print('Guardiamo al futuro')

    listQual = []
    qip = qualityIndexInPlay # mi salvo l'attuale qualityIndexInPlay

    # Ipotizzo di andare avanti di 10 pacchetti rispetto all'ultimo che ho all'interno del mio buffer
    future = S_to_download + 10
    if future > num_pkts: #un controllo per non cercare pacchetti che non esistono
        return False, 0, qip

    print(str(num_pkts) + " buf: " + str(getBuffer()))
    print('Guardo in ' +str(BASEDIR + str(SS) + str('sec')))

    #per comodita' prendo sempre la qualita' piu' bassa
    qip = 9
    fq = BASEDIR + str(SS) + "sec/bunny_" + str(int(listaQualita[qip])) + "bps" # p.e in 1sec/bunny_270316bps 
    print(str(fq))
        
    fff = open(fq, 'r')
    for line2 in fff.readlines():
        ll2 = line2.split()
        if int(future) == int(ll2[0]):
            bits = int(ll2[1])
    #pktSizeCurrentlyDownloading = bits
    #originalPktSize = float(bits)
    #time_data = (float(bits)/THRFromDataset) + 3 * RTT
    #print("Il futuro pacchetto " + str(future) + " pktSize = " + str(pktSizeCurrentlyDownloading) + ", time = " + str(time_data))
    aux_bytes[future] = (originalPktSize / SS)
    aux_buf[future] = qip
    return True, bits, qip
    


BUF = 0
first = True
counter = 0
totaltime = 0

SS_A = [1,2,4,6,10,15]
LAMBDA = int(sys.argv[5])

maximumOverlap = float(sys.argv[6])

alpha = 2

ratioForP = float(sys.argv[7])

pType = 4
fastBackoff = sys.argv[4]
if fastBackoff == True:
    fastBackoff = True
else:
    fastBackoff = False

#print("fast = " + str(fastBackoff))

SS_FIXED = int(sys.argv[2])  # this is 1 2 4 6 10 15
#if SS_FIXED == 0:
#    open('myvar.csv','w+').close()

ff = open(sys.argv[1],'r')

#RTT = float(sys.argv[8]) # latenza per connessione
RTT = 0
#sigma = float(sys.argv[9]) #ma se buffertime e' chiamato da runFake e gli vengono passati 9 argomenti (0-8) come fa a prendere il 9?
sigma = 6 # a magic number  
BASEDIR = "../data/"
# Let's start with a segment size equal to 1
# TODO Make it parametric
if SS_FIXED > 0:
    SS = SS_FIXED
else:
    SS = 1

#============================= Prendo i pesi dato il SS fissato ====================================
pkts_weight = [] 

path = 'pesi/' + str(SS_FIXED) + 'sec/'
num_files = sum(os.path.isfile(os.path.join(path, f)) for f in os.listdir(path))

listaQualita = []
for file in os.listdir(path):
    thisqual = int(file.split('_')[1].split('bps')[0])
    listaQualita.append(thisqual)
listaQualita = sorted(listaQualita)

c = -1
#for file in os.listdir(path):
for item in listaQualita:
    pkts_weight.append([])
    thisqual = 'bunny_' + str(item) + 'bps'
    fw = open(path + thisqual, 'r')
    c += 1
    num_pkts = 0
    for line in fw:
        num_pkts += 1
        p = line.split()
        pkts_weight[c].append((p[1])) #optional float
#============================= END ==================================================================
aux_buf = []
aux_bytes = []

for i in range(num_pkts):
    aux_bytes.append(-1)
    aux_buf.append(-1)


#print("SIGMA = " + str(sigma))

lastPkts = []
actualTime = 1

# TIME of the video
needToDownload = True
timeToFinish = 0

timeFromDataset = -1
THRFromDataset = -1
actualTime = 1

numPauses = 0
wasPaused = False

open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'w+').close()

wasDownloading = False

currentQual = 0
timeDownloadStarted = 0
pktSizeCurrentlyDownloading = 0
originalPktSize = 0
weHaveToBreak = False
alreadyDownloadedTime = 0

VAR = 0
VAR_RED = 0
VAR_WEIGHT = 0
AVG_WEIGHT = 0
AVG_VAR = 0
AVG = 0

BUFFER = []
BYTES = []
for i in range(650):
    BUFFER.append(-1)
    BYTES.append(-1)

firstTime = True ###################### a che cosa serve?

S_to_download = -1

riskOfBufferOutage = False

qualityIndexInPlay = -1
canChange = 0
switchMinimumQuality = False
totalBits = 0
p = 0

expectedOriginalTime = -1

lastTHR = -1

#print(str(SS) + " " + str(sys.argv[1]))

for i in range(1,10000):
    #print(",".join([str(i),str(actualTime),str(getBuffer()),str(SS),str(THRFromDataset),str(qualityIndexInPlay),str(lastTHR),str(sigma),str(wasDownloading),str(timeToFinish)]))
    #print(THRFromDataset)
    if lastTHR > 0:
        variation = lastTHR - random.gauss(float(lastTHR), float(sigma)) 
        THRFromDataset = min(lastTHR,max(1,lastTHR - variation))
    if weHaveToBreak:
        # Let's empty the buffer
        fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
        cc = i
        while getBuffer() > 0:
            fw.write(str(cc) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + "\n")
            actualTime += 1
            cc += 1
        fw.close()    
        sys.exit()
    # At each second (or when we have finished downloading a segment)
    # we have to choose the segment size.
    line = ""
    if i > timeFromDataset:
        # Read a new line, which should represent the data rate and the timestamp
        line = ff.readline()
        lastTHR = THRFromDataset
        if line != "":
            timeFromDataset = float(line.split(" ")[0])
            THRFromDataset = max(1,float(line.split(" ")[1]))
        else:
            pass    # We finished values from the dataset
        # TODO Here we have to update the timetofinish according to the new datarate and to the content already downloaded
    if wasDownloading:
#         print("** - timetofinish = " + str(timeToFinish))
        timePassed = i - timeDownloadStarted
        
        contentDownloaded = lastTHR * timePassed
        pktSizeCurrentlyDownloading = originalPktSize - contentDownloaded
        if pktSizeCurrentlyDownloading > 0:
        # Now compute new timetofinish
            time_data = (pktSizeCurrentlyDownloading/THRFromDataset) + 3 * RTT
#             print("pktSize = " + str(pktSizeCurrentlyDownloading) + ", time = " + str(time_data))
            timeToFinish = i + time_data
            time_tmp = (originalPktSize * 1.0 / THRFromDataset) + 3 * RTT
            ratio = expectedOriginalTime / (time_tmp * 1.0)
            continue

    needToDownload = True
    while i >= timeToFinish and needToDownload:
        if THRFromDataset > -1:
            if len(lastPkts) >= LAMBDA:
                lastPkts.remove(lastPkts[0])
            lastPkts.append(THRFromDataset)
        
        # We finished to download the previous packet
        if wasDownloading:
            canChange -= 1
            # Let's fill correctly the buffer
            for s_down in range(SS * (S_to_download - 1) + 1, SS * (S_to_download) + 1):
                #BUFFER.insert((SS*S_to_download)+s_down,1)
                BUFFER[s_down] = qualityIndexInPlay
                BYTES[s_down] = (originalPktSize / SS)
                totalBits += originalPktSize
            fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
            file_media = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]) + '.values', 'a+')
            fw.write(str(timeToFinish) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + "# down packet " + str(S_to_download) + " " + str(numPauses) + "\n")
            file_media.write(str(THRFromDataset) + ' ' + str(BUFFER[actualTime]) + '\n')
            fw.close()

            #if oracle_selector == 3:
                #esplorazione(SS, num_pkts, qualityIndexInPlay, math.floor((actualTime + getBuffer()) / SS))
                       
            # TODO: check that we do not surpass LAMBDA
            # We have to update lastPkts
            #timeItTook = timeToFinish - timeDownloadStarted
            #lastPkts.append(pktSizeCurrentlyDownloading / timeItTook)
        if getBuffer() >= 30:
            needToDownload = False
            wasDownloading = False
            timeDownloadStarted = i
            timeToFinish = i
    
        if needToDownload:
            # Let's first decide the quality
            listQual = [] 
            for file in os.listdir(BASEDIR + str(SS) + str("sec")):
                thisqual = int(file.split("_")[1].split("bps")[0])
                listQual.append(thisqual)
            listQual = sorted(listQual) ######################################## le ordino p.e sorted([5, 2, 3, 1, 4]) --> [1, 2, 3, 4, 5]
            if SS_FIXED >= 0 and canChange <= 0:
                #print("*****")
                #print("Let's decide whether we have to change the quality")
                #print("We have a THR of " + str(THRFromDataset))
                #print("We are in the #down Packet" + str(S_to_download))

                canChange = 3 ############################################# cosa e perche' 3
                expectedTime = 0
                actualQual = listQual[0] #################### la mia qualita' attuale e' la prima alla posizione 0
                for item in listQual: ################### ciclando sulla lista delle qualita' se THR(presumo il throughput dal dataset) e' maggiore dell'elemento nella lista delle qualita' allora 211
                    #print("item is " + str(item))
                    if THRFromDataset > item: ########## in pratica mi fermo quando supero il throughput disponibile mi sono salvato l'ultimo compatibile
                        actualQual = item
                qualityIndexInPlay = listQual.index(actualQual)
                #print("new quality index is " + str(qualityIndexInPlay) + " SS: " + str(SS))

            if SS_FIXED > 0 and firstTime:
                qualityIndexInPlay = 10
                firstTime = False

            ######################## TODO: Decide which segment size we have to choose
            p = 0
            if len(lastPkts) > 1 and SS_FIXED == 0: #################### dal file runFake visto che SS_FIXED prende il secondo argomento $item dal ciclo 1,2,4,6,10,15 come fa ad essere 0?
                AVG = int(sum(lastPkts)/len(lastPkts))
                AVG_RED = 0 ################## che cosa fa?
                count = 1
                total = len(lastPkts) ################### numero totale di elementi che stanno dentro l'array
                summaCount = 0
                for item in lastPkts:
                    AVG_RED += abs(min(0,(listQual[qualityIndexInPlay] / item) - SS))
                    AVG_WEIGHT += (count*1.0) * item ################# cosa fa?
                    count += 1
                    summaCount += count
                AVG_WEIGHT /= summaCount
                AVG_WEIGHT = THRFromDataset
                AVG_RED = AVG_RED / len(lastPkts)

                count = 1
                for j in range(total): 
                    VAR += abs(int(lastPkts[j]) - AVG)
                    VAR_RED += abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED)) ################## cosa fa?
                    VAR_WEIGHT += pow(((count * 1.0)/total)*(abs(int(lastPkts[j]) - AVG_WEIGHT)), alpha) ################## cosa fa?
                    #VAR_WEIGHT += pow(((count * 1.0)/total)*abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED)),5)
                    #VAR_WEIGHT += pow((count * 1.0)/total,2)*abs(min(1,abs(math.floor(listQual[qualityIndexInPlay] / item) - SS))*(lastPkts[j] - AVG_RED))
                    count += 1
                VAR_WEIGHT = math.sqrt(VAR_WEIGHT)
                VAR_RED = math.sqrt(VAR_RED)
                VAR = math.sqrt(VAR)
                isVarOk = False
                if (pType == 1 or pType == 5) and VAR_RED > 0:
                    isVarOk = True
                if (pType == 2 or pType == 3 or pType == 4 or pType == 7 or pType == 12) and VAR_WEIGHT > 0:
                    isVarOk = True
                if (pType == 6) and VAR > 0:
                    isVarOk = True
                if isVarOk > 0:
                    #VAR = math.sqrt(VAR)                         #       BUF      QUAL       SS     QUAL_I  PAUSE
                    beta = 1
                    if pType == 1:
                        p = beta * (AVG/(VAR_RED + AVG))           #SS0  17.9464 2.44187e+06 5.99726 13.3602 73.8056
                    elif pType == 2:
                        p = beta * (AVG/(VAR_WEIGHT + AVG))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 3:
                        p = beta * (AVG_RED/(VAR_WEIGHT + AVG_RED))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 4:
                        p = beta * (AVG_WEIGHT/(VAR_WEIGHT + AVG_WEIGHT))                #SS0  17.9448 2.44468e+06 5.99805 13.3701 73.75
                    elif pType == 5:
                        p = beta * (AVG_RED/(VAR_RED + AVG_RED))   #SS0  20.9993 2.12835e+06 1.78363 11.173 17.1389
                    elif pType == 6:
                        p = beta * (AVG_RED / (VAR + AVG_RED))
                    elif pType == 7:
                        p = beta * (AVG_RED / (VAR_WEIGHT + AVG_RED))
                    elif pType == 12:
                        p = beta * (AVG / (VAR_WEIGHT + AVG))
                        if getBuffer() < 10:
                            p = 0
                            SS = 1
                        elif getBuffer() < 15:
                            SS = 2
                        elif getBuffer() < 20:
                            SS = 4


                else:
                    p = 1

                SSold = SS
                if not firstTime:
                    SS_before = SS_A[max(0,SS_A.index(SS) - 1)]
                    SS_after = SS_A[min(len(SS_A) - 1,SS_A.index(SS) + 1)]
                    SSprime = (1-p) * SS_before + p * SS_after
#                     print("*** SSprime ***")
#                     print("SSprime = " + str(SSprime))
#                     
#                     # Now we pick the closest SS to SSprime
                    delta = max(SS_A)
                    choosen = -1
                    for item in SS_A:
                        tmp_delta = abs(item - SSprime)
                        if tmp_delta < delta:
                            choosen = item
                            delta = tmp_delta
                        if SS_FIXED == 0:
                            if choosen == -1:
                                SS = 1
                            else:
                                SS = choosen

                else:
                    qualityIndexInPlay = 10
                    if SS_FIXED == 0:
                        SS = 1
                    elif SS_FIXED > 0:
                        SS = SS_FIXED

                    firstTime = False       
                    #SS0  19.7643 2.24244e+06 2.5025 11.7449 42.4167
            else:  ####################### questo e' l'else di if len(lastPkts) > 1 and SS_FIXED == 0:
                if SS_FIXED > 0:
                    SS = SS_FIXED
                else:
                    SS = 1  # We start with a segment size of 1

            # TODO Identify segment to be downloaded
            # TODO, for now, we math.floor(), so we pick an earlier segment and we might download again some data
            if SS_FIXED == 0 and switchMinimumQuality and fastBackoff:
#                 print("SSprime, here")
                SS = 1           
            
            if SS_FIXED > 0:
                SS = SS_FIXED
            S_to_download = math.ceil((actualTime + getBuffer()) / SS) ############################## parte intera superiore 
            
            # Ok, now we got in SSold the old SS, and in SS the new SS. Let's see how many seconds 
            # already downloaded we have to lose
            amount = 0
            for BB in range(S_to_download * SS, (S_to_download + 1) * SS):
                if BUFFER[BB] >= 0:
                    amount += 1
            mmm = (amount * 1.0) / SS ##### questo e' l'overlap non mi interessa (e' una cosa di dynamic dove ho pacchetti di ss=1 e scarico i primi 9 sec. Poi voglio passare ad un ss=10, overlap e' spreco di tempo )
            if mmm >= maximumOverlap and SS_FIXED == 0:
                SS = SSold
                S_to_download = math.floor((actualTime + getBuffer()) / SS)
            
            # TODO Let's choose the quality depending on the buffer and on the 
            # for item in os.listdir() ....
#            qualityIndexInPLay = 0
            ## Qua viene preso il file della qualita' della qualita' che ho scelto
            if SS_FIXED == 0 and riskOfBufferOutage:
                SS = 1
                S_to_download = math.floor((actualTime + getBuffer()) / SS)
            listQual = []
            for fileQ in os.listdir(BASEDIR + str(SS) + str("sec")):
                thisqual = int(fileQ.split("_")[1].split("bps")[0])
                listQual.append(thisqual)
            listQual.sort()
            fileQuality = BASEDIR + str(SS) + "sec/bunny_" + str(int(listQual[qualityIndexInPlay])) + "bps"
           
            # Download pkt QUA!
            #TODO: mi prendo dal vettore 2dimensioni di tutti i pesi di tutte le qualita' i pesi specifici del pacchetto con l'id che sto osservando
            vettore_pesi_pacchetto = []
            for item in range(len(pkts_weight)):
                #print(item)
                for indice_pacchetto in range(len(pkts_weight[item])):
                    #print(pkts_weight[item][peso])
                    if int(S_to_download) == int(indice_pacchetto):
                        really = str((int(pkts_weight[item][indice_pacchetto]) * 8))
                        vettore_pesi_pacchetto.append(float(really))

            ### Esperimento per switch rapido degli algoritmi: 
            ### 0 originale
            ### 1 greedy
            ### 2 smart
            bits = 0
            if oracle_selector == 0:
                # buffertime originale
                ff2 = open(fileQuality, 'r')
                for line2 in ff2.readlines():
                    ll2 = line2.split()
                    if int(S_to_download) == int(ll2[0]):
                        bits = int(ll2[1])
            else:
                if oracle_selector == 1:
                    # modifica oracolo greedy
                    if int(S_to_download) < 0 or not vettore_pesi_pacchetto:
                        qualityIndexInPlay = save
                    else:
                        qualityIndexInPlay = vettore_pesi_pacchetto[0]
                        for item in vettore_pesi_pacchetto:
                            time_data = (float(item) / THRFromDataset) + 3 * RTT
                            if int(THRFromDataset) > (float(item) / int(SS)) and time_data <= int(SS):
                                bits = float(item)
                        if bits == 0:
                            qualityIndexInPlay = 0
                            bits = vettore_pesi_pacchetto[0]
                        else:
                            qualityIndexInPlay = vettore_pesi_pacchetto.index(bits)
                        save = qualityIndexInPlay
                else:
                    # vecchio oracolo smart sostituito dal quello del prof.
                    # caso oracolo smart
                    if oracle_selector == 2:
                        if int(S_to_download) < 0 or not vettore_pesi_pacchetto:
                            qualityIndexInPlay = save
                        else:
                            qualityIndexInPlay = vettore_pesi_pacchetto[0]
                            for item in vettore_pesi_pacchetto:
                                time_data = (float(item) / THRFromDataset) + 3 * RTT
                                if getBuffer() == 0:
                                    qualityIndexInPlay = 0
                                    bits = vettore_pesi_pacchetto[0]
                                else:
                                    if time_data < getBuffer():
                                      # print(str(vettore_pesi_pacchetto.index(item)) + ": " + str(time_data) + " " + str(getBuffer()) + " " + str(bits))
                                        bits = float(item)
                          # print(vettore_pesi_pacchetto)
                            total = vettore_pesi_pacchetto.count(bits)
                            if bits == 0:
                                qualityIndexInPlay = 0
                                bits = vettore_pesi_pacchetto[0]
                            else:
                                qualityIndexInPlay = vettore_pesi_pacchetto.index(bits) + total - 1
                            save = qualityIndexInPlay
                    else:
                        #caso esplorazione
                        # mi chiedo se devo fare esplorazione:
                        # se si scarico il pacchetto in avanti e me lo faccio ritornare come nuovo bits con il suo qualityIndexInPlay
                        # e vado a metterlo in BYTES e BUFFER
                        ff2 = open(fileQuality, 'r')
                        print(str(fileQuality) + " " + str(S_to_download))
                        for line2 in ff2.readlines():
                            ll2 = line2.split()
                            if int(S_to_download) == int(ll2[0]):
                                item = int(ll2[1])
                        bits = item
                        if S_to_download == 41:
                            bits = 0                        
                        #print(str(bits) + " " + str(item))
                        # in questo if assumo di fare esplorazione solo a determinati valori per provare 20, 31, 47 ecc..
                        if getBuffer() > 28 and (S_to_download == 15 or S_to_download == 29 or S_to_download == 47 or S_to_download == 60 or S_to_download == 83 or S_to_download == 160 or S_to_download == 247 or S_to_download == 333 or S_to_download == 418 or S_to_download == 551):
                            [exploring, bits_f, quality_f] = esplorazione(SS, num_pkts, qualityIndexInPlay, S_to_download)
                            if exploring == False: #significa che l'esplorazione non e' andata a buon fine
                                #print('No esplorazione')
                                bits = item
                                #print(str(bits) + " " + str(item))
                            else:
                                #print('Si esplorato')
                                exploring == False
                                bits = bits_f
                                qualityIndexInPlay = quality_f
                                #print(str(bits) + " " + str(item))
                        else:
                            #print("No condizioni")
                            bits = item
                            # print(str(bits) + " " + str(item))
                            # time.sleep(1)
                            # se non faccio esplorazione, guardo se nel mio buffer ho informazioni future
                            # se si cerco di capire come posso/se devo cambiare la qualita'
                            # altrimenti procedo normalmente
                            # print(str(S_to_download))
                            if aux_buf[S_to_download] > -1: 
                                print("SI")
                                # if aux_bytes[S_to_download] < item:
                                # time_data = (float(item) / THRFromDataset) + 3 * RTT
                                # if int(THRFromDataset) > (float(item) / int(SS)) and time_data <= int(SS):
                                time_data = (float(aux_bytes[S_to_download]) / THRFromDataset) + 3 * RTT 
                                # if aux_bytes[S_to_download] < THRFromDataset:
                                if int(THRFromDataset) > (float(aux_bytes[S_to_download]) / int(SS)) and time_data <= int(SS):
                                    bits = aux_bytes[S_to_download]
                                    qualityIndexInPlay = aux_buf[S_to_download]
                                else:
                                    #print("NO")
                                    bits = item
            
            if bits == 0:
                # We do not have further segments to download
                weHaveToBreak = True ############################### siamo alla fine del video quindi non devo scaricare altro
                break
            else:
                switchMinimumQuality = False
                weHaveToBreak = False

            pktSizeCurrentlyDownloading = bits
            originalPktSize = float(bits)
            time_data = (float(bits)/THRFromDataset) + 3 * RTT
#             print("bits = " + str(bits) + ", pktSize = " + str(pktSizeCurrentlyDownloading) + ", time = " + str(time_data))
            timeDownloadStarted = timeToFinish
            timeToFinish = timeToFinish + time_data
#             print("timeToFinish = " + str(timeToFinish))
            expectedOriginalTime = time_data
            wasDownloading = True
    
    fw = open(str(sys.argv[1]) + '.SS' + str(sys.argv[2]),'a+')
    if getBuffer() > 0:
        wasPaused = False
        BUF = max(0,BUF - 1)
        actualTime += 1
        #                                                                                                                qualita' che mi sono eff scaricato
        #fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " # " + str(line).strip() + " " + str(VAR_WEIGHT) + " " + str(AVG) + "\n")
        fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " " + str(currentQual) + " " + str(SS) + " " + str(BUFFER[actualTime]) + " " + str(BYTES[actualTime]) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + " PLAY\n")
        
    else:
        # We have a pause. Let's switch back to the lowest possible quality
        #fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " 0 " + str(SS) + " 0 " + str(0) + " # " + str(line).strip() + " " + str(VAR_WEIGHT) + " " + str(AVG) + "\n")
        if not wasPaused:
            numPauses += 1
        wasPaused = True
        fw.write(str(i) + " " + str(actualTime) + " " + str(getBuffer()) + " 0 " + str(SS) + " 0 " + str(0) + " " + str(VAR_WEIGHT) + " " + str(AVG) + " " + str(p) + " " + str(numPauses) + " PAUSE\n")
    if getBuffer() == 0:
        switchMinimumQuality = True

    fw.close()
