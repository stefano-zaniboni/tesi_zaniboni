#!/bin/bash
PAUSE=`tail -1 $1 | awk '{print $1-$2}'`
COUNT=`cat $1 | awk 'BEGIN{NUMP=BITS=QUAL=SS=BUF=BPS=COUNT=VAR=AVG==0}{NUMP+=$11;VAR+=$8;AVG+=$9;BITS+=$7;QUAL+=$6;SS+=$5;BUF+=$3;BPS+=$4;COUNT+=1}END{print BUF/COUNT " " BPS/COUNT " " SS/COUNT " " QUAL/COUNT " " BITS/COUNT " " AVG/COUNT " " VAR/COUNT " " NUMP/COUNT}'`
echo -n $COUNT
echo -n " "
echo $PAUSE
