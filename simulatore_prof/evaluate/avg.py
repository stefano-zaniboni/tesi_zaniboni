#!/usr/bin/python
import os
import sys
from pprint import pprint

listFile = []
listS = [1, 2, 4, 6, 10, 15]
fw = open('output.gp', 'w')
fw.write('# TEST GNUPLOT AUTOMATICO\n')
fw.write(" \n")
fw.write("set terminal postscript enhanced color eps\n")
fw.write("set yrange [0:30]\n")

# Questo e' per i grafici con thr su asse x e media qualita' per thr su asse y
for item in listS:
	fw.write("set o 'thr" + str(item) + ".eps'\n")
	fw.write('plot\\' + '\n')
	for i in [0, 1]:
		fw.write("'values.SS" + str(item) + ".A" + str(i) + ".data' u 1:4 w p t '" + "A "+ str(i) + "',\\"+ "\n")
	fw.write("'values.SS" + str(item) + ".A" + str(2) + ".data' u 1:4 w p t '" + "A "+ str(2) + "'"+ "\n")
	fw.write('\n')
