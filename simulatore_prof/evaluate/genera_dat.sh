#!/bin/bash
# TODO: ordinamento dei file .data per la generazione di file con linee di gnuplot

# DATASET="datasetFake"
# DATASET="colorado"
# DATASET="japan"
rm ${1}/*.data.dat
rm ${1}/*.avg

for p in 0 1 2
do
	# for ss in 0 1 2 4 6 10 15 # per colorado e datasetFake
	for ss in 0 1 15 # per japan
	do
		# ./extract_data_from_results.py ${DATASET}/values.SS${ss}.${DATASET}.A${p}.data ${DATASET} ${p} ${ss}
		./extract_data_from_results.py ${1}/values.SS${ss}.${1}.A${p}.data ${1} ${p} ${ss}
	done
done

# TODO: generazione file avg per medie a parita' di segmento
for p in 0 1 2
do
	# for ss in 1 2 4 6 10 15 # per colorado e datasetFake
	for ss in 0 1 15 # per japan
	do
		./estrapolaMedie.py ${1}/values.SS${ss}.${1}.A${p}.data.dat ${1} ${p} ${ss}
	done
done

# TODO: generazione file avg per SS=0 e lunghezze di segmento 0 1 15
for p in 0 1 2 
do
	for ss in 0 1 15
	do
		./estrapolaMedieDynamic.py ${1}/values.SS${ss}.${1}.A${p}.data.dat ${1} ${p} ${ss}
	done
done
# rm *.data.dat