#!/usr/bin/python3
import sys
ff = open(sys.argv[1], 'r')
fw = open(sys.argv[1] + ".eachsec", "w+")
lastTime = -1
lastThr = -1
for line in ff.readlines():
    ll = line.split(" ")
    if lastThr == -1:
        lastTime = int(ll[0])
        lastThr = float(ll[1])
    else:
        numTimes = int(ll[0]) - lastTime
        delta = (float(ll[1]) - lastThr)/numTimes
        #print("-----")
        #print("LasThr = " + str(lastThr))
        #print(float(ll[1]))
        #print("Numtimes = " + str(numTimes))
        #print("Delta = " + str(delta))
        nextOne = float(ll[1])
        nextOne = lastThr
        #print(ll[0])
        for j in range(lastTime, int(ll[0])):
            print(str(j) + " " + str(nextOne) + " # " + str(delta) + " " + str(line.strip()))
            fw.write(str(j) + " " + str(nextOne) + " # " + str(delta) + " " + str(line.strip()) + "\n")
            nextOne += delta
        #print("\n")
        
        lastTime = int(ll[0])
        lastThr = float(ll[1])
ff.close()
fw.close()
