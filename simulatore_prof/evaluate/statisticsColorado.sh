#!/bin/bash
./compareColorado.sh > allLogsColorado
echo "NAME  BUF     BPS     SS  QUAL  BITS  PAUSE"
echo -n "SS0  "
cat allLogsColorado | grep SS0 | awk 'BEGIN{VAR=AVG=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{VAR+=$8;AVG+=$7;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C}'
echo -n "SS1  "
cat allLogsColorado | grep "SS1 " | awk 'BEGIN{VAR=AVG=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{VAR+=$8;AVG+=$7;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C}'
echo -n "SS2  "
cat allLogsColorado | grep SS2 | awk 'BEGIN{VAR=AVG=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{VAR+=$8;AVG+=$7;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C}'
echo -n "SS4  "
cat allLogsColorado | grep SS4 | awk 'BEGIN{VAR=AVG=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{VAR+=$8;AVG+=$7;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C}'
echo -n "SS6  "
cat allLogsColorado | grep SS6 | awk 'BEGIN{VAR=AVG=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{VAR+=$8;AVG+=$7;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C}'
echo -n "SS10 "
cat allLogsColorado | grep SS10 | awk 'BEGIN{VAR=AVG=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{VAR+=$8;AVG+=$7;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C}'
echo -n "SS15 "
cat allLogsColorado | grep SS15 | awk 'BEGIN{VAR=AVG=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{VAR+=$8;AVG+=$7;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C}'
