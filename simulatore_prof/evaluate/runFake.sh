#!/bin/bash
#for item in 0 1 2 4 6 10 15
#exit
#./createFakeCsv.sh

DATASET="datasetFake"
DATASET="colorado_reduced"
DATASET="japan"
DATASET="wiscape"
DATASET="colorado"

rm ${DATASET}/*.values
#rm values*.data
#rm values.SS*.datasetFake.A*.data.*

#rm values.SS*.colorado.A2.data*

for item in 0
do
#	for p in 0 1 2 3
	for p in 0 1 2 # 3 is still the same oracle, but with the 20171004 edit. 
#	for p in 1 2 3 4 5 6 7 8 9 10 11 12 13
#	for p in 2 3 4 7 12
	do
		#for lambda in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
		#for lambda in 2 3 4 5 6 7 8 9 10
		#for RTT in `seq 0.01 0.01 0.5`
		#do
			#RTT=`echo $RTT | sed 's/,/./g'`
			#for ss in 1 2 4 6 10 15 
			#for ss in 1 2 4 6 10 15
			for ss in 0
			do
	#			for file in `ls datasetFake/*.csv` # per prove normali
#				for file in `ls ${DATASET}/*.final.eachsec` #per dataset japan
#				for file in `ls wiscape/*.parsed` # per dataset wiscape
				for file in `ls ${DATASET}/*.parsed.ok` # per dataset colorado
				do
					echo $ss
					#./buffertime.py ${file} $ss ${p} False 5 5 1 ${RTT} # argv[2] equal to 0 means we dynamically adjust the segment size
				#	python3 buffertime.py ${file} $ss ${p} False 5 5 1 ${RTT} ${1} ${2} # argv[2] equal to 0 means we dynamically adjust the segment size
				done
			done
			for ss in 0 1 2 4 6 10 15
			do
				rm values.SS${ss}.${DATASET}.A${p}.data
				for file in `find ${DATASET}/*.SS${ss}`
				do 
					echo -n "$file ${DATASET} $ss ${p} " >> values.SS${ss}.${DATASET}.A${p}.data
					#cat $file | grep -v " -1 " | awk 'BEGIN{TIME=C=Q=B=THR=0;}{C+=1;B+=$7;Q+=$6;P+=$9;NUMP+=$10;TIME+=$1;THR+=$7;}END{print THR/C " " Q/C " " TIME-595 " " B/C " " NUMP}' >> values.SS${ss}.A${p}.data
					LASTTIME=`tail -1 ${file} | awk '{print $2}'`
					cat $file | grep -v " -1 " | awk -v var="$LASTTIME" 'BEGIN{TIME=C=Q=B=THR=0;}{C+=1;B+=$7;Q+=$6;P+=$9;NUMP=$11;TIME=$1;THR+=$7;}END{print THR/C " " Q/C " " TIME-var " " B/C " " NUMP}' | sed 's/#//g' >> values.SS${ss}.${DATASET}.A${p}.data
				done
			done
			rm ${DATASET}/*.values

#			for lambda in 5
#			do
#				for maxO in 0.2
#				do
#					for ratioP in 0
#					do
#						for file in `ls datasetFake/*.csv`
#						do
#							./buffertime.py ${file} $item $p False $lambda $maxO $ratioP $RTT	# argv[2] equal to 0 means we dynamically adjust the segment size
#						done
#						./statisticsFake.sh $RTT > statFake_p_${p}_False_${lambda}_${maxO}_${ratioP}_${RTT}
	#					for file in `ls datasetFake/*.csv`
	#					do
	#						./buffertime.py ${file} $item $p True $lambda $maxO	$ratioP $RTT # argv[2] equal to 0 means we dynamically adjust the segment size
	#					done
	#					./statisticsFake.sh $RTT > statFake_p_${p}_True_${lambda}_${maxO}_${ratioP}_${RTT}
#					done
#				done
#			done
		#done
	done
done
./startGenGnuplot.sh # per creare i file values.SS.datasetFake.A.data.csv e generare lo script di gnuplot
