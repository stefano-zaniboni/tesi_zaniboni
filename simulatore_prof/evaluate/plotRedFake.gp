set terminal postscript color eps
set out 'chart2.eps'

plot 'datasetFake/client_30000.csv.SS0' u 1:2 w l,'datasetFake/client_30000.csv.SS1' u 1:2 w l,'datasetFake/client_30000.csv.SS15' u 1:2 w l

set out 'chart3.eps'
plot 'datasetFake/client_30000.csv.SS0' u 1:3 w l,'datasetFake/client_30000.csv.SS1' u 1:3 w l,'datasetFake/client_30000.csv.SS15' u 1:3 w l

set out 'chart4.eps'
plot 'datasetFake/client_30000.csv.SS0' u 1:4 w l,'datasetFake/client_30000.csv.SS1' u 1:4 w l,'datasetFake/client_30000.csv.SS15' u 1:4 w l

set out 'chart5.eps'
plot 'datasetFake/client_30000.csv.SS0' u 1:5 w l,'datasetFake/client_30000.csv.SS1' u 1:5 w l,'datasetFake/client_30000.csv.SS15' u 1:5 w l

set out 'chart6.eps'
plot 'datasetFake/client_30000.csv.SS0' u 1:6 w l,'datasetFake/client_30000.csv.SS1' u 1:6 w l,'datasetFake/client_30000.csv.SS15' u 1:6 w l

set out 'chart_variance.eps'
plot 'datasetFake/client_30000.csv.SS0' u 1:8 w l

set out 'chart_avg.eps'
plot 'datasetFake/client_30000.csv.SS0' u 1:9 w l

set yrange [-0.1:1.1]
set out 'chart_p.eps'
plot 'datasetFake/client_30000.csv.SS0' u 1:10 w l
