#!/bin/bash
# Scripts that takes 3 parameters:
# - 1: minimum datarate
# - 2: maximum datarate
# - 3: increment

for i in `seq ${1} ${2} ${3}`
do
    #echo $i | awk '{ print sprintf("%.9f", $1); }'
    a=$(echo $i | awk '{ print sprintf("%.0f", $1); }')
    echo $a
    # echo "0 ${i}" > datasetFakeGen/client_${i}.csv
    # echo "10000 ${i}" >> datasetFakeGen/client_${i}.csv
    echo "0 ${i}" | awk '{ print $1, sprintf("%.9f", $2); }' > datasetFake/${a}.csv
    echo "10000 ${i}" | awk '{ print $1, sprintf("%.9f", $2); }'  >> datasetFake/${a}.csv
done
