set terminal postscript enhanced color eps
set o '10.eps'
plot 'bunny_1174238bps' w l,'bunny_127412bps' w l,'bunny_1431232bps' w l,'bunny_176780bps' w l,'bunny_2070985bps' w l,'bunny_216536bps' w l,'bunny_2384387bps' w l,'bunny_252988bps' w l,'bunny_2884382bps' w l,'bunny_317328bps' w l,'bunny_3245900bps' w l,'bunny_3493765bps' w l,'bunny_368912bps' w l,'bunny_3792491bps' w l,'bunny_45373bps' w l,'bunny_503270bps' w l,'bunny_568500bps' w l,'bunny_771359bps' w l,'bunny_88482bps' w l,'bunny_987061bps' w l,

set o '10perc.eps'
plot 'bunny_1174238bps' u 1:3 w l,'bunny_127412bps' u 1:3 w l,'bunny_1431232bps' u 1:3 w l,'bunny_176780bps' u 1:3 w l,'bunny_2070985bps' u 1:3 w l,'bunny_216536bps' u 1:3 w l,'bunny_2384387bps' u 1:3 w l,'bunny_252988bps' u 1:3 w l,'bunny_2884382bps' u 1:3 w l,'bunny_317328bps' u 1:3 w l,'bunny_3245900bps' u 1:3 w l,'bunny_3493765bps' u 1:3 w l,'bunny_368912bps' u 1:3 w l,'bunny_3792491bps' u 1:3 w l,'bunny_45373bps' u 1:3 w l,'bunny_503270bps' u 1:3 w l,'bunny_568500bps' u 1:3 w l,'bunny_771359bps' u 1:3 w l,'bunny_88482bps' u 1:3 w l,'bunny_987061bps' u 1:3 w l,

set o '10thr.eps'
plot 'bunny_1174238bps' u 1:5 w l,'bunny_127412bps' u 1:5 w l,'bunny_1431232bps' u 1:5 w l,'bunny_176780bps' u 1:5 w l,'bunny_2070985bps' u 1:5 w l,'bunny_216536bps' u 1:5 w l,'bunny_2384387bps' u 1:5 w l,'bunny_252988bps' u 1:5 w l,'bunny_2884382bps' u 1:5 w l,'bunny_317328bps' u 1:5 w l,'bunny_3245900bps' u 1:5 w l,'bunny_3493765bps' u 1:5 w l,'bunny_368912bps' u 1:5 w l,'bunny_3792491bps' u 1:5 w l,'bunny_45373bps' u 1:5 w l,'bunny_503270bps' u 1:5 w l,'bunny_568500bps' u 1:5 w l,'bunny_771359bps' u 1:5 w l,'bunny_88482bps' u 1:5 w l,'bunny_987061bps' u 1:5 w l,
