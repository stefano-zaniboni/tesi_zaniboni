#!/bin/bash
for file in `find ${1}`
do
	./percentages.py ${file} > `basename ${file}`
done

echo "set terminal postscript enhanced color eps" > plot.gp
echo "set o '10.eps'" >> plot.gp
echo -n "plot " >> plot.gp

for file in `ls bunny*`
do
	echo -n "'${file}' w l," >> plot.gp
done

echo "" >> plot.gp
echo "" >> plot.gp
echo "set o '10perc.eps'" >> plot.gp
echo -n "plot " >> plot.gp
for file in `ls bunny*`
do
	echo -n "'${file}' u 1:4 w l," >> plot.gp
done

echo "" >> plot.gp
echo "" >> plot.gp
echo "set o '10thr.eps'" >> plot.gp
echo -n "plot " >> plot.gp
for file in `ls bunny*`
do
	echo -n "'${file}' u 1:5 w l," >> plot.gp
done
