#!/usr/bin/python
import sys
# Get bps
BPS = float(sys.argv[1].split("_")[1].split("bps")[0])
BPS /= 8
SEC = float(sys.argv[1].split("sec")[0])

W = -1
for line in open(sys.argv[1],'r').readlines():
    ll = line.split()
    if W == -1:
        # First Time
        W = float(ll[1])/SEC
        continue
    dd = -1

    if W > BPS:
        dd = 1
    print(str(ll[0]) + " " + str(W/(float(ll[1])/SEC)) + " " + str(W/BPS) + " " + str(dd) + " " + str(ll[1]))
    W = float(ll[1])/SEC
