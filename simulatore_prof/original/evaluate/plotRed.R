library(ggplot2)
library(plyr)
library(reshape2)
library(grid)
library(extrafont)

dd <- read.table("dataOk", col.names = c("THR","SS","RTT","SIGMA","BUF","BPS","AVGSS","QUAL","BITS","AVG","VAR","PAUSE"))
#dd <- read.table("dataOk", col.names = c("THR","SS","BUF","BPS","AVGSS","QUAL","BITS","AVG","VAR","PAUSE"))

dd$SS[dd$SS == "0"] = "DYNAMIC"
dd$SS[dd$SS == "1"] = "SS = 1"
dd$SS[dd$SS == "2"] = "SS = 2"
dd$SS[dd$SS == "4"] = "SS = 4"
dd$SS[dd$SS == "6"] = "SS = 6"
dd$SS[dd$SS == "10"] = "SS = 10"
dd$SS[dd$SS == "15"] = "SS = 15"

dd$THR = dd$THR / 1e6

mySIGMA <- 0
myTHR <- 1.2
#myTHR <- 0.5
myRTT <- 0.01
#myRTT <- 0.01

#aggdata <-aggregate(dd, by=list(dd$THR,dd$SS,dd$RTT,dd$SIGMA), FUN=mean)
#aggdata <-aggregate(. + dd$THR + dd$SS + dd$RTT + dd$SIGMA ~ dd$QUAL + dd$PAUSE, data = dd, FUN=mean)
#aggdata
#colnames(aggdata) = c("THR","SS","RTT","SIGMA","BUF","BPS","AVGSS","QUAL","BITS","AVG","VAR","PAUSE")
#dd = aggdata
#dd
aggdata <- aggregate(x = list(
                       BUF=dd$BUF,
                       BPS=dd$BPS,
                       AVGSS=dd$AVGSS,
                       QUAL=dd$QUAL,
                       BITS=dd$BITS,
                       AVG=dd$AVG,
					   VAR=dd$VAR,
  					   PAUSE=dd$PAUSE
                       ),
                     by = list(THR=dd$THR, SS=dd$SS, RTT=dd$RTT, SIGMA=dd$SIGMA),
                     FUN = mean, na.rm = TRUE)
dd = aggdata
summary(dd)
#dd

dd1 = dd[dd$THR == myTHR & dd$SIGMA == mySIGMA,]
dd1

g <- ggplot(data = dd[dd$THR == myTHR & dd$SIGMA == mySIGMA,], aes(x = SS, y = PAUSE, group = SS, color = factor(SS), linetype = factor(SS))) + geom_bar(stat='identity')# + geom_line(stat = "smooth")
#g <- g + scale_x_continuous(expand = c(0,0), limits = c(0.01,0.27)) + scale_y_continuous(expand = c(0,0), limits = c(0,8))
g <- g + theme_bw()
g <- g + theme(axis.line = element_line(colour = "black"),
				 panel.grid.major = element_blank(),
				 panel.grid.minor = element_blank(),
				 panel.border = element_blank(),
				 panel.background = element_blank(),
				 legend.title = element_blank(),
				 legend.position = "bottom",
			         legend.key.size = unit(1.5, "lines"))
g <- g + scale_linetype(guide = "none")
g <- g + xlab("Latency (s)") + ylab("Total Pause Time (s)")
g <- g + scale_linetype_manual(values = c("solid","dashed","dotted","solid","longdash","twodash","dotdash"), breaks = c("DYNAMIC", "SS = 1", "SS = 2", "SS = 4", "SS = 6", "SS = 10", "SS = 15")) + scale_colour_manual(values = c("#000000","#FF0000","#0000AA","#AAAAAA","#0000FF","#555555","#AA0000"), breaks = c("DYNAMIC", "SS = 1", "SS = 2", "SS = 4", "SS = 6", "SS = 10", "SS = 15"))
g <- g + ggsave("rchart_pause_rtt.pdf", width=7, height=5)

embed_fonts("rchart_pause_rtt.pdf")
