#!/bin/bash
for n in `seq 1 44`
do
	if [ -f "japan/interference_${n}.final.eachsec.SS0" ]
	then
#        echo $n
		echo -n "japan/interference_${n}.final.eachsec.SS0 "
		./getDataSingle.sh japan/interference_${n}.final.eachsec.SS0
		echo -n "japan/interference_${n}.final.eachsec.SS1 "
		./getDataSingle.sh japan/interference_${n}.final.eachsec.SS1
		echo -n "japan/interference_${n}.final.eachsec.SS2 "
		./getDataSingle.sh japan/interference_${n}.final.eachsec.SS2
		echo -n "japan/interference_${n}.final.eachsec.SS4 "
		./getDataSingle.sh japan/interference_${n}.final.eachsec.SS4
		echo -n "japan/interference_${n}.final.eachsec.SS6 "
		./getDataSingle.sh japan/interference_${n}.final.eachsec.SS6
		echo -n "japan/interference_${n}.final.eachsec.SS10 "
		./getDataSingle.sh japan/interference_${n}.final.eachsec.SS10
		echo -n "japan/interference_${n}.final.eachsec.SS15 "
		./getDataSingle.sh japan/interference_${n}.final.eachsec.SS15
	fi
done
