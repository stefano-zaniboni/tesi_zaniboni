set terminal postscript color eps
set out 'chart2.eps'

plot 'japan/interference_16.final.eachsec.SS0' u 1:2 w l,'japan/interference_16.final.eachsec.SS1' u 1:2 w l,'japan/interference_16.final.eachsec.SS15' u 1:2 w l

set out 'chart3.eps'
plot 'japan/interference_16.final.eachsec.SS0' u 1:3 w l,'japan/interference_16.final.eachsec.SS1' u 1:3 w l,'japan/interference_16.final.eachsec.SS15' u 1:3 w l

set out 'chart4.eps'
plot 'japan/interference_16.final.eachsec.SS0' u 1:4 w l,'japan/interference_16.final.eachsec.SS1' u 1:4 w l,'japan/interference_16.final.eachsec.SS15' u 1:4 w l

set out 'chart5.eps'
plot 'japan/interference_16.final.eachsec.SS0' u 1:5 w l,'japan/interference_16.final.eachsec.SS1' u 1:5 w l,'japan/interference_16.final.eachsec.SS15' u 1:5 w l

set out 'chart6.eps'
plot 'japan/interference_16.final.eachsec.SS0' u 1:6 w l,'japan/interference_16.final.eachsec.SS1' u 1:6 w l,'japan/interference_16.final.eachsec.SS15' u 1:6 w l

set out 'chart_variance.eps'
plot 'japan/interference_16.final.eachsec.SS0' u 1:8 w l

set out 'chart_avg.eps'
plot 'japan/interference_16.final.eachsec.SS0' u 1:9 w l

set out 'chart_p.eps'
plot 'japan/interference_16.final.eachsec.SS0' u 1:10 w l
