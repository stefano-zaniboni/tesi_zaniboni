cat $1 | awk 'BEGIN{SS=BUF=BPS=COUNT=0}{SS+=$5;BUF+=$3;BPS+=$4;COUNT+=1}END{print BUF/COUNT " " BPS/COUNT " " SS/COUNT}'
SUM=0
for file in `ls log_interference*final`
do
	THIS=`tail -1 $file | awk '{print $1-$2}'`
	SUM=`expr $THIS + $SUM`
done
TOTAL=`ls log_interference*final | wc -l`
expr $SUM / $TOTAL
