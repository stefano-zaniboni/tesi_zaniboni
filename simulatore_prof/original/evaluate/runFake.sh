#!/bin/bash
#for item in 0 1 2 4 6 10 15
#exit
for item in 0
do
	for p in 4
#	for p in 1 2 3 4 5 6 7 8 9 10 11 12 13
#	for p in 2 3 4 7 12
	do
		#for lambda in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
		#for lambda in 2 3 4 5 6 7 8 9 10
		for RTT in `seq 0.01 0.01 0.5`
		do
			RTT=`echo $RTT | sed 's/,/./g'`
			for item in 1 2 4 6 10 15
			do
				for file in `ls datasetFake/*.csv`
				do
					./buffertime.py ${file} $item 1 False 5 5 1 ${RTT} 	# argv[2] equal to 0 means we dynamically adjust the segment size
				done
			done
			for lambda in 5
			do
				for maxO in 0.2
				do
					for ratioP in 0
					do
						for file in `ls datasetFake/*.csv`
						do
							./buffertime.py ${file} $item $p False $lambda $maxO $ratioP $RTT	# argv[2] equal to 0 means we dynamically adjust the segment size
						done
						./statisticsFake.sh $RTT > statFake_p_${p}_False_${lambda}_${maxO}_${ratioP}_${RTT}
	#					for file in `ls datasetFake/*.csv`
	#					do
	#						./buffertime.py ${file} $item $p True $lambda $maxO	$ratioP $RTT # argv[2] equal to 0 means we dynamically adjust the segment size
	#					done
	#					./statisticsFake.sh $RTT > statFake_p_${p}_True_${lambda}_${maxO}_${ratioP}_${RTT}
					done
				done
			done
		done
	done
done
