set term postscript enhanced color eps

set style fill solid 0.7

#set key outside below
set nokey

set boxwidth 0.8
set style line 1 lt 1 lc rgb "green"
set style line 2 lt 1 lc rgb "red"

set xlabel "SS Size"
set ylabel "Average Pause Time (s)"

set grid

set output 'pauseWiscape.eps'
plot 'statWiscape' u (column(0)):7:(0.5):($1=="0var"?1:$1=="1"?2:$1=="2"?3:$1=="4"?4:$1=="6"?5:$1=="10"?6:7):xtic(1) with boxes lc variable

set ylabel "Average Quality Index [0:19]"
set output 'qualityWiscape.eps'
plot 'statWiscape' u (column(0)):3:(0.5):($1=="0var"?1:$1=="1"?2:$1=="2"?3:$1=="4"?4:$1=="6"?5:$1=="10"?6:7):xtic(1) with boxes lc variable
