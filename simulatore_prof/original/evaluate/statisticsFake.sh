#!/bin/bash
./compareFake.sh ${1} ${2} > allLogsFake_${1}_${2}_${3}
echo "NAME  BUF     BPS     SS  QUAL  BITS  PAUSE"
echo -n "SS0  "
cat allLogsFake_${1} | grep SS0 | awk 'BEGIN{AVG=VAR=NUMP=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{AVG+=$7;VAR+=$8;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;NUMP+=$10;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C " " NUMP/C}'
echo -n "SS1  "
cat allLogsFake_${1} | grep "SS1 " | awk 'BEGIN{AVG=VAR=NUMP=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{AVG+=$7;VAR+=$8;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;NUMP+=$10;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C " " NUMP/C}'
echo -n "SS2  "
cat allLogsFake_${1} | grep SS2 | awk 'BEGIN{AVG=VAR=NUMP=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{AVG+=$7;VAR+=$8;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;NUMP+=$10;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C " " NUMP/C}'
echo -n "SS4  "
cat allLogsFake_${1} | grep SS4 | awk 'BEGIN{AVG=VAR=NUMP=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{AVG+=$7;VAR+=$8;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;NUMP+=$10;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C " " NUMP/C}'
echo -n "SS6  "
cat allLogsFake_${1} | grep SS6 | awk 'BEGIN{AVG=VAR=NUMP=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{AVG+=$7;VAR+=$8;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;NUMP+=$10;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C " " NUMP/C}'
echo -n "SS10 "
cat allLogsFake_${1} | grep SS10 | awk 'BEGIN{AVG=VAR=NUMP=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{AVG+=$7;VAR+=$8;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;NUMP+=$10;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C " " NUMP/C}'
echo -n "SS15 "
cat allLogsFake_${1} | grep SS15 | awk 'BEGIN{AVG=VAR=NUMP=PAUSE=BITS=QUAL=SS=BPS=BUF=C=0}{AVG+=$7;VAR+=$8;PAUSE+=$9;BITS+=$6;QUAL+=$5;SS+=$4;BPS+=$3;BUF+=$2;NUMP+=$10;C+=1}END{print BUF/C " " BPS/C " " SS/C " " QUAL/C " " BITS/C " " PAUSE/C " " AVG/C " " VAR/C " " NUMP/C}'
