#!/bin/bash
#for item in 0 1 2 4 6 10 15
#exit
#for RTT in 0.01 0.05 0.1 0.15 0.2 0.5 1
#for RTT in 0.02 0.03 0.04 0.06 0.07 0.08 0.09 0.11 0.12 0.13 0.14 0.16 0.17 0.18 0.19
for RTT in 0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1
do
	RTT=`echo $RTT | sed 's/,/./g'`
	for sigma in `seq 0 100000 1000000`
	#for sigma in 200000
	#for sigma in 0 100000
	do
		for run in `seq 1 5`
		do
			for item in 0 1 2 4 6 10 15
			do
				for file in `ls datasetFake/*.csv`
				do
					./buffertime.py ${file} $item 4 False 5 0.2 0 ${RTT} ${sigma}	# argv[2] equal to 0 means we dynamically adjust the segment size
				done
			done
			echo $run " " $sigma " " $item " " $RTT
			./statisticsFake.sh $RTT $sigma $run > statFake_p_${p}_False_5_0.2_0_${RTT}_${sigma}_${run}
			rm statFake_p_${p}_False_5_0.2_0_${RTT}_${sigma}_${run}
		done
	done
done
