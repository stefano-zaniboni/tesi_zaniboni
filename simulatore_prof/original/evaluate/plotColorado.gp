set terminal postscript color eps
set out 'chart2.eps'

plot 'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS0' u 1:2 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS1' u 1:2 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS2' u 1:2 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS4' u 1:2 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS6' u 1:2 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS10' u 1:2 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS15' u 1:2 w l

set out 'chart3.eps'
plot 'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS0' u 1:3 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS1' u 1:3 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS2' u 1:3 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS4' u 1:3 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS6' u 1:3 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS10' u 1:3 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS15' u 1:3 w l

set out 'chart4.eps'
plot 'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS0' u 1:4 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS1' u 1:4 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS2' u 1:4 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS4' u 1:4 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS6' u 1:4 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS10' u 1:4 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS15' u 1:4 w l

set out 'chart5.eps'
plot 'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS0' u 1:5 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS1' u 1:5 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS2' u 1:5 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS4' u 1:5 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS6' u 1:5 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS10' u 1:5 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS15' u 1:5 w l

set out 'chart6.eps'
plot 'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS0' u 1:6 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS1' u 1:6 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS2' u 1:6 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS4' u 1:6 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS6' u 1:6 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS10' u 1:6 w l,'colorado/8-10-2011_1-05-22_PM.csv.parsed.ok.SS15' u 1:6 w l
