#for item in 0 1 2 4 6 10 15
#for item in 1 2 4 6 10 15
#do
#	for file in `ls japan/*.eachsec`
#	do
#		./buffertime.py ${file} $item 1 False 5 5	# argv[2] equal to 0 means we dynamically adjust the segment size
#	done
#done
for item in 0
do
	for p in 1 2 3 4 5 6 7 8 9 10 11 12
	do
		for lambda in 5
		do
			for maxO in 0.5
			do
				for ratioP in 0
				do
					for file in `ls japan/*.final.eachsec`
					do
						./buffertime.py ${file} $item $p False $lambda $maxO $ratioP	# argv[2] equal to 0 means we dynamically adjust the segment size
					done
					./statisticsJapan.sh > statJapan_p_${p}_False_${lambda}_${maxO}_${ratioP}
					for file in `ls japan/*.final.eachsec`
					do
						./buffertime.py ${file} $item $p True $lambda $maxO $ratioP 	# argv[2] equal to 0 means we dynamically adjust the segment size
					done
					./statisticsJapan.sh > statJapan_p_${p}_True_${lambda}_${maxO}_${ratioP}
				done
			done
		done
	done
done
