#!/usr/bin/python3
import sys

ff = open(sys.argv[1],'r')
for line in ff.readlines():
    ll = line.split(" ")
    fw = open('interference_' + str(ll[1]),'a+')
    fw.write(ll[0] + " " + ll[2].strip() + "\n")
    fw.close()
ff.close()

import os
for file in os.listdir("."):
    counter = 0
    if file.startswith("interference_"):
        print(file)
        ff = open(file,'r')
        fw = open(file + ".final",'w+')
        for line in ff.readlines():
            ll = line.split(" ")
            if counter == 0:
                offset = int(ll[0])
                counter = 1
            if "DIV" in ll[1]:
                fw.write(str(int(ll[0]) - offset) + " " + "10000000" + "\n")
            else:
                fw.write(str(int(ll[0]) - offset) + " " + str(float(ll[1].strip()) * 8) + "\n")
        fw.close()
